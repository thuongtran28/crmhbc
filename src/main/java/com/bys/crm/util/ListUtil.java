package com.bys.crm.util;

import java.util.ArrayList;
import java.util.Set;

import com.bys.crm.domain.erp.constant.AAStatus;
import com.bys.crm.domain.erp.model.BRBranchs;

public class ListUtil {

	public static Integer[] convertToArrayId(Set<BRBranchs> branchs) {
		ArrayList<Integer> ids = new ArrayList<>();
		branchs.forEach(branch -> {
			if (branch.getId() != null && AAStatus.Alive.name().equals(branch.getStatus())) {
				ids.add(branch.getId());
			}
		});
		return ids.isEmpty() ? new Integer[] {1}  : ids.toArray(new Integer[0]);
	}
	
	public static Integer[] convertToArrayIdNull(Set<BRBranchs> branchs) {
		ArrayList<Integer> ids = new ArrayList<>();
		branchs.forEach(branch -> {
			if (branch.getId() != null && AAStatus.Alive.name().equals(branch.getStatus())) {
				ids.add(branch.getId());
			}
		});
		return ids.isEmpty()? new Integer[] {1} : ids.toArray(new Integer[0]);
	}
}
