package com.bys.crm.config.security.handler;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;

import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;

import com.bys.crm.app.dto.ResponseDto;
import com.bys.crm.app.dto.UserInfoDto;
import com.bys.crm.config.security.jwt.JWTService;
import com.bys.crm.config.security.model.DatabaseUserDetails;
import com.bys.crm.domain.erp.model.ADUserDevices;
import com.bys.crm.util.SecurityUtil;
import com.fasterxml.jackson.databind.ObjectMapper;

public class DeviceUsernamePasswordLoginSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {
	
	private JWTService jwtService;
	private ObjectMapper mapper = new ObjectMapper();
	
	
		
	public void setJwtService(JWTService jwtService) {
		this.jwtService = jwtService;
	}

//	@Autowired
//	private ADPrivilegeService adPrivilegeService;

	@Override
	@Transactional
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication auth)
			throws IOException, ServletException {
		response.setStatus(HttpStatus.OK.value());
		
		DatabaseUserDetails userDetails = (DatabaseUserDetails) auth.getPrincipal();
		
		ADUserDevices device = userDetails.getUser().getAuthenticatedDevice();
		String loginName = (device == null) ? userDetails.getUsername()
				: SecurityUtil.getLoginName(userDetails.getUsername(), device.getDeviceKey(), device.getDeviceType());
		
		// Get privilege detail list
//		List<PrivilegeDetailDto> privilegeDetails = adPrivilegeService.getPrivilegeDetailList(userDetails.getUser().getObjectID().intValue());
		
//		UserInfoDto dto = new UserInfoDto(jwtService.encode(loginName), userDetails.getUser(), privilegeDetails);
		UserInfoDto dto = new UserInfoDto(jwtService.encode(loginName), userDetails.getUser());
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(mapper.writeValueAsString(new ResponseDto(dto)));
		
		
		response.getWriter().flush();
	}
}
