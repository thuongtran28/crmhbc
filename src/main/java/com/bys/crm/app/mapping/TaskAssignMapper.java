package com.bys.crm.app.mapping;

import org.springframework.stereotype.Component;

import com.bys.crm.app.dto.TaskAssignDto;
import com.bys.crm.domain.erp.model.ARTaskAssigns;

@Component
public class TaskAssignMapper extends BaseMapper<TaskAssignDto, ARTaskAssigns> {

	@Override
	public TaskAssignDto buildDto(ARTaskAssigns entity) {
		TaskAssignDto dto = super.buildDto(entity);
		if (dto == null) {
			return null;
		}

		return dto;
	}

	@Override
	public ARTaskAssigns buildEntity(TaskAssignDto dto) {
		ARTaskAssigns entity = super.buildEntity(dto);

		return entity;
	}
}
