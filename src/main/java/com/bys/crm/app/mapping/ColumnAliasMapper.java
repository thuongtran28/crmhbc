package com.bys.crm.app.mapping;

import org.springframework.stereotype.Component;

import com.bys.crm.app.dto.ColumnAliasDto;
import com.bys.crm.domain.erp.model.AAColumnAlias;

@Component
public class ColumnAliasMapper extends BaseMapper<ColumnAliasDto, AAColumnAlias> {
	@Override
	public ColumnAliasDto buildDto(AAColumnAlias entity) {
		ColumnAliasDto dto = super.buildDto(entity);
		if (dto == null) {
			return null;
		}
		 
		return dto;
	}
}
