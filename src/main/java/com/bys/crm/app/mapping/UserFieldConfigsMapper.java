package com.bys.crm.app.mapping;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bys.crm.app.dto.ColumnAliasDto;
import com.bys.crm.app.dto.UserFieldConfigsDto;


import com.bys.crm.domain.erp.model.STUserFieldConfigs;
import com.bys.crm.domain.erp.repository.STUserFieldConfigsRepository;

@Component
public class UserFieldConfigsMapper extends BaseMapper<UserFieldConfigsDto, STUserFieldConfigs> {
	@Override
	public UserFieldConfigsDto buildDto(STUserFieldConfigs entity) {
		UserFieldConfigsDto dto = super.buildDto(entity);
		if (dto == null) {
			return null;
		}
		 
		return dto;
	}
	
	
//	@Override
//	public UserFieldConfigsDto buildDto(STUserFieldConfigs entity ) {
//		UserFieldConfigsDto dto = super.buildDto(entity);
//		if (dto == null) {
//			return null;
//		}
//		 
//		return dto;
//	}
	
	@Autowired
	private STUserFieldConfigsRepository userFieldConfigsRepository;
	
	
	@Override
	public STUserFieldConfigs buildEntity(UserFieldConfigsDto dto) {
		STUserFieldConfigs entity = super.buildEntity(dto);
		if (dto.getId() == null) {
			entity.setId(keyGenerationService.findMaxId(userFieldConfigsRepository).incrementAndGet());
			 
		}

		 
		return entity;
	}
}
