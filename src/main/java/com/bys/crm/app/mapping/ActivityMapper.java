package com.bys.crm.app.mapping;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bys.crm.app.dto.ActivityDto;
import com.bys.crm.app.dto.TaskAssignDto;
import com.bys.crm.domain.erp.model.ARActivitys;
import com.bys.crm.domain.erp.model.ARTaskAssigns;
import com.bys.crm.domain.erp.repository.ARActivitysRepository;
import com.bys.crm.domain.erp.repository.ARTaskAssignsRepository;

@Component
public class ActivityMapper extends BaseMapper<ActivityDto, ARActivitys> {
	@Autowired
	private ARActivitysRepository activitysRepository;

	@Autowired
	private ARTaskAssignsRepository taskAssignsRepository;

	@Autowired
	private TaskAssignMapper taskAssignMapper;

	@Override
	public ActivityDto buildDto(ARActivitys entity) {
		ActivityDto dto = super.buildDto(entity);
		if (dto == null) {
			return null;
		}

//		List<TaskAssignDto> taskAssignDtos = new ArrayList<>();
//		if (entity.getTaskAssigns() != null && !entity.getTaskAssigns().isEmpty()) {
//			taskAssignDtos = taskAssignMapper.buildDtos(entity.getTaskAssigns());
//		}
//		dto.setTaskAssigns(taskAssignDtos);

		return dto;
	}

	@Override
	public ARActivitys buildEntity(ActivityDto dto) {
		ARActivitys entity = super.buildEntity(dto);
		if (dto.getId() == null) {
			entity.setId(keyGenerationService.findMaxId(activitysRepository).incrementAndGet());
		}

//		if (dto.getEmployee() != null && dto.getEmployee().getId() == null) {
//			entity.setEmployee(null);
//		}

		if (dto.getBranch() != null && dto.getBranch().getId() == null) {
			entity.setBranch(null);
		}

//		if (dto.getEmployeeGroup() != null && dto.getEmployeeGroup().getId() == null) {
//			entity.setEmployeeGroup(null);
//		}

		List<ARTaskAssigns> taskAssigns = new ArrayList<>();
		ARTaskAssigns taskAssign = null;
		Long newId = null;
		AtomicLong maxId = keyGenerationService.findMaxId(taskAssignsRepository);

		if (dto.getTaskAssigns() != null && !dto.getTaskAssigns().isEmpty()) {
			for (TaskAssignDto taskAssignDto : dto.getTaskAssigns()) {
				taskAssign = taskAssignMapper.buildEntity(taskAssignDto);
				newId = maxId.incrementAndGet();
				maxId = new AtomicLong(newId);

				taskAssign.setId(newId);
				taskAssign.setActivity(entity);
				taskAssigns.add(taskAssign);
			}
		}
		entity.setTaskAssigns(taskAssigns);

		return entity;
	}
}
