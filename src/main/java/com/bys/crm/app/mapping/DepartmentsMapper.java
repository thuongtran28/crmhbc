package com.bys.crm.app.mapping;

import org.springframework.stereotype.Component;

import com.bys.crm.app.dto.DepartmentsDto;
import com.bys.crm.domain.erp.model.HRDepartments;
@Component
public class DepartmentsMapper extends BaseMapper<DepartmentsDto, HRDepartments> {
	@Override
	public DepartmentsDto buildDto(HRDepartments entity) {
		DepartmentsDto dto = super.buildDto(entity);
		if (dto == null) {
			return null;
		}
		 
		return dto;
	}
}
