package com.bys.crm.app.mapping;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bys.crm.app.dto.EmployeeGroupDto;
import com.bys.crm.domain.erp.constant.AAStatus;
import com.bys.crm.domain.erp.model.HREmployeeGroups;
import com.bys.crm.domain.erp.repository.HREmployeeGroupReporitory;

@Component
public class EmployeeGroupMapper extends BaseMapper<EmployeeGroupDto, HREmployeeGroups> {

	@Autowired
	private HREmployeeGroupReporitory repository;
	
	@Override
	public EmployeeGroupDto buildDto(HREmployeeGroups entity) {
		EmployeeGroupDto dto = super.buildDto(entity);
		if (dto == null) {
			return null;
		}

		return dto;
	}

	@Override
	public HREmployeeGroups buildEntity(EmployeeGroupDto dto) {
		HREmployeeGroups entity = super.buildEntity(dto);

		entity.setId(keyGenerationService.findMaxId(repository).incrementAndGet());
		entity.setStatus(AAStatus.Alive.name());
		
		return entity;
	}
}

