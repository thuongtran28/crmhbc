package com.bys.crm.app.mapping;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bys.crm.app.dto.ProspectCustomerDto;
import com.bys.crm.domain.erp.model.ARProspectCustomers;
import com.bys.crm.domain.erp.repository.ARProspectCustomersRepository;

@Component
public class ProspectCustomerMapper extends BaseMapper<ProspectCustomerDto, ARProspectCustomers> {
	@Autowired
	private ARProspectCustomersRepository prospectCustomersRepository;

	@Override
	public ProspectCustomerDto buildDto(ARProspectCustomers entity) {
		ProspectCustomerDto dto = super.buildDto(entity);
		if (dto == null) {
			return null;
		}

		return dto;
	}

	@Override
	public ARProspectCustomers buildEntity(ProspectCustomerDto dto) {
		ARProspectCustomers entity = super.buildEntity(dto);
		if (dto.getId() == null) {
			entity.setId(keyGenerationService.findMaxId(prospectCustomersRepository).incrementAndGet());
		}

		if (dto.getEmployee() != null && dto.getEmployee().getId() == null) {
			entity.setEmployee(null);
		}

		if (dto.getBranch() != null && dto.getBranch().getId() == null) {
			entity.setBranch(null);
		}

		if (dto.getEmployeeGroup() != null && dto.getEmployeeGroup().getId() == null) {
			entity.setEmployeeGroup(null);
		}

		return entity;
	}
	
}
