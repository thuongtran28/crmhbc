package com.bys.crm.app.mapping;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bys.crm.app.dto.UserPermissionDto;
import com.bys.crm.domain.erp.constant.AAStatus;
import com.bys.crm.domain.erp.model.ADPermissions;
import com.bys.crm.domain.erp.model.ADUserPermissions;
import com.bys.crm.domain.erp.model.ADUsers;
import com.bys.crm.domain.erp.repository.ADUserPermissionRepository;

@Component
public class UserPermissionMapper extends BaseMapper<UserPermissionDto, ADUserPermissions> {

	@Autowired
	private ADUserPermissionRepository userPermissionRepository;
		
	@Override
	public UserPermissionDto buildDto(ADUserPermissions entity) {
		UserPermissionDto dto = super.buildDto(entity);
		if (dto == null) {
			return null;
		}
		return dto;
	}

	@Override
	public ADUserPermissions buildEntity(UserPermissionDto dto) {
		ADUserPermissions entity = super.buildEntity(dto);

		entity.setId(keyGenerationService.findMaxId(userPermissionRepository).incrementAndGet());
		entity.setStatus(AAStatus.Alive.name());
		entity.setCreatedDate(DateTime.now());
		
		ADUsers user = new ADUsers();
		user.setId(dto.getId());
		entity.setUser(user);
		
		ADPermissions permission = new ADPermissions();
		permission.setId(dto.getId());
		entity.setPermissions(permission);
		
		return entity;
	}
}
