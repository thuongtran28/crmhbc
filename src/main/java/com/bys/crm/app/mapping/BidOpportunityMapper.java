package com.bys.crm.app.mapping;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bys.crm.app.dto.BidOpportunitysDto;
import com.bys.crm.domain.erp.model.ADConfigValues;
import com.bys.crm.domain.erp.model.ARBidOpportunitys; 
import com.bys.crm.domain.erp.repository.ARBidOpportunitysRepository;
 
 

@Component
public class BidOpportunityMapper extends BaseMapper<BidOpportunitysDto , ARBidOpportunitys > {
	@Override
	public BidOpportunitysDto buildDto(ARBidOpportunitys entity) {
		BidOpportunitysDto dto = super.buildDto(entity);
		if (dto == null) {
			return null;
		}

		return dto;
	}
	
	public BidOpportunitysDto buildDto(ARBidOpportunitys entity, List<ADConfigValues> opportunityStages, List<ADConfigValues> hsmtStatus) {
		BidOpportunitysDto dto = super.buildDto(entity);
		if (dto == null) {
			return null;
		}
		ADConfigValues eval = opportunityStages.stream()
				  .filter(evaluation -> dto.getOpportunityStage().equals(evaluation.getValue()))
				  .findFirst().orElse(null);
		
		if(eval != null ) {
			dto.setOpportunityStage(eval.getText());
		}
		
		ADConfigValues hsmt = hsmtStatus.stream()
				  .filter(hsmtSt -> dto.getOpportunityStageStatus().equals(hsmtSt.getValue()))
				  .findFirst().orElse(null);
		
		if(hsmt != null ) {
			dto.setOpportunityStageStatus(hsmt.getText());
		}
		return dto;
	}

	@Autowired
	private ARBidOpportunitysRepository bidOpportunitysRepository;
	
	@Override
	public ARBidOpportunitys  buildEntity(BidOpportunitysDto dto) {
		ARBidOpportunitys entity = super.buildEntity(dto);
		if (dto.getId() == null) {
			entity.setId(keyGenerationService.findMaxId(bidOpportunitysRepository).incrementAndGet());
		}
		return entity;
	}
}
