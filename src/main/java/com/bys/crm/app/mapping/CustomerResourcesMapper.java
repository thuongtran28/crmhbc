package com.bys.crm.app.mapping;

import org.springframework.stereotype.Component;

import com.bys.crm.app.dto.CustomerResourcesDto;
 
import com.bys.crm.domain.erp.model.ARCustomerResources;
 
 
@Component
public class CustomerResourcesMapper extends BaseMapper<CustomerResourcesDto, ARCustomerResources> {
	@Override
	public CustomerResourcesDto buildDto(ARCustomerResources entity) {
		CustomerResourcesDto dto = super.buildDto(entity);
		if (dto == null) {
			return null;
		}
		 
		return dto;
	}
}
