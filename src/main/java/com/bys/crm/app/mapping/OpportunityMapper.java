package com.bys.crm.app.mapping;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bys.crm.app.dto.OpportunityDto;
import com.bys.crm.app.dto.TaskAssignDto;
import com.bys.crm.domain.erp.model.AROpportunitys;
import com.bys.crm.domain.erp.model.ARTaskAssigns;
import com.bys.crm.domain.erp.repository.ARBidOpportunitysRepository;
import com.bys.crm.domain.erp.repository.AROpportunitysRepository;
import com.bys.crm.domain.erp.repository.ARTaskAssignsRepository;

@Component
public class OpportunityMapper extends BaseMapper<OpportunityDto, AROpportunitys> {
	@Autowired
	private AROpportunitysRepository opportunitysRepository;
	
	@Autowired
	private ARBidOpportunitysRepository bidOpportunitysRepository;

	@Autowired
	private ARTaskAssignsRepository taskAssignsRepository;

	@Autowired
	private TaskAssignMapper taskAssignMapper;
 

	@Override
	public OpportunityDto buildDto(AROpportunitys entity) {
		OpportunityDto dto = super.buildDto(entity);
		if (dto == null) {
			return null;
		}

		return dto;
	}

	@Override
	public AROpportunitys buildEntity(OpportunityDto dto) {
		AROpportunitys entity = super.buildEntity(dto);
		if (dto.getId() == null) {
			entity.setId(keyGenerationService.findMaxId(opportunitysRepository).incrementAndGet());
		//	entity.getBidOpportunity().setOpportunity(entity);
		}
		
		if (dto.getCustomer() != null && dto.getCustomer().getId() == null) {
			entity.setCustomer(null);
		}

//		if (dto.getCustomerContact() != null && dto.getCustomerContact().getId() == null) {
//			entity.setCustomerContact(null);
//		}

//		if (dto.getEmployee() != null && dto.getEmployee().getId() == null) {
//			entity.setEmployee(null);
//		}

		if (dto.getBranch() != null && dto.getBranch().getId() == null) {
			entity.setBranch(null);
		}

		if (dto.getCampaign() != null && dto.getCampaign().getId() == null) {
			entity.setCampaign(null);
		}

//		if (dto.getEmployeeGroup() != null && dto.getEmployeeGroup().getId() == null) {
//			entity.setEmployeeGroup(null);
//		}

//		if (dto.getBidOpportunity() != null ) {
//			entity.setBidOpportunity(opportunityMapper.buildEntity(dto.getBidOpportunity())); 
//			entity.getBidOpportunity().setOpportunity(entity);
//		}
		
		List<ARTaskAssigns> taskAssigns = new ArrayList<>();
		ARTaskAssigns taskAssign = null;
		Long newId = null;
		AtomicLong maxId = keyGenerationService.findMaxId(taskAssignsRepository);

		if (dto.getTaskAssigns() != null && !dto.getTaskAssigns().isEmpty()) {
			for (TaskAssignDto taskAssignDto : dto.getTaskAssigns()) {
				taskAssign = taskAssignMapper.buildEntity(taskAssignDto);
				newId = maxId.incrementAndGet();
				maxId = new AtomicLong(newId);

				taskAssign.setId(newId);
				taskAssign.setOpportunity(entity);
				taskAssigns.add(taskAssign);
			}
		}
		entity.setTaskAssigns(taskAssigns);

		return entity;
	}
}
