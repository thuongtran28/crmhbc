package com.bys.crm.app.mapping;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import com.bys.crm.app.dto.CustomerDto;
import com.bys.crm.app.dto.LocationDto;
import com.bys.crm.domain.erp.constant.ADObjectType;
import com.bys.crm.domain.erp.constant.CustomerType;
import com.bys.crm.domain.erp.model.ARCustomers;
import com.bys.crm.domain.erp.repository.ARCustomersRepository;

@Component
public class CustomerMapper extends BaseMapper<CustomerDto, ARCustomers> {
	@Resource
	PasswordEncoder passwordEncoder;

	@Autowired
	private GenericMapper mapper;

	@Autowired
	private ARCustomersRepository customersRepository;

	@Override
	public CustomerDto buildDto(ARCustomers entity) {
		CustomerDto dto = super.buildDto(entity);
		if (dto == null) {
			return null;
		}
		if (entity.getLocation() != null) {
			dto.setLocation(this.mapper.buildObject(entity.getLocation(), LocationDto.class));
			if (entity.getLocation().getParent() != null) {
				dto.getLocation()
						.setParent(this.mapper.buildObject(entity.getLocation().getParent(), LocationDto.class));
			}
		}

		return dto;
	}

	@Override
	public ARCustomers buildEntity(CustomerDto dto) {
		ARCustomers entity = super.buildEntity(dto);
		if (dto.getId() == null) {
			entity.setId(keyGenerationService.findMaxId(customersRepository).incrementAndGet());
			entity.setCustomerNo(keyGenerationService.generateCustomerNumber(ADObjectType.Customer.name()));
		}

		if (dto.getEmployee() != null && dto.getEmployee().getId() == null) {
			entity.setEmployee(null);
		}

		if (dto.getBranch() != null && dto.getBranch().getId() == null) {
			entity.setBranch(null);
		}

		if (dto.getEmployeeGroup() != null && dto.getEmployeeGroup().getId() == null) {
			entity.setEmployeeGroup(null);
		}
		if(dto.getCustomerResources()!= null && dto.getCustomerResources().getId() == null) {
			entity.setCustomerResources(null);
		}

		if(dto.getDepartments()!= null && dto.getDepartments().getId() == null) {
			entity.setDepartments(null);
		}
		
		if(dto.getActivity()!= null && dto.getActivity().getId() == null) {
			entity.setActivity(null);
		}
		if (CustomerType.Company.name().equals(dto.getCustomerType())) {
			entity.setDob(null);
		} else {
			entity.setCompanyEstablishmentDay(null);
		}

		return entity;
	}
}
