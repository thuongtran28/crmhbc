package com.bys.crm.app.mapping;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bys.crm.app.dto.CampaignDto;
import com.bys.crm.app.dto.TaskAssignDto;
import com.bys.crm.domain.erp.model.ARCampaigns;
import com.bys.crm.domain.erp.model.ARTaskAssigns;
import com.bys.crm.domain.erp.repository.ARCampaignsRepository;
import com.bys.crm.domain.erp.repository.ARTaskAssignsRepository;

@Component
public class CampaignMapper extends BaseMapper<CampaignDto, ARCampaigns> {
	@Autowired
	private ARCampaignsRepository campaignsRepository;

	@Autowired
	private ARTaskAssignsRepository taskAssignsRepository;

	@Autowired
	private TaskAssignMapper taskAssignMapper;

	@Override
	public CampaignDto buildDto(ARCampaigns entity) {
		CampaignDto dto = super.buildDto(entity);
		if (dto == null) {
			return null;
		}

		return dto;
	}

	@Override
	public ARCampaigns buildEntity(CampaignDto dto) {
		ARCampaigns entity = super.buildEntity(dto);
		if (dto.getId() == null) {
			entity.setId(keyGenerationService.findMaxId(campaignsRepository).incrementAndGet());
		}

//		if (dto.getEmployee() != null && dto.getEmployee().getId() == null) {
//			entity.setEmployee(null);
//		}

		if (dto.getBranch() != null && dto.getBranch().getId() == null) {
			entity.setBranch(null);
		}

//		if (dto.getEmployeeGroup() != null && dto.getEmployeeGroup().getId() == null) {
//			entity.setEmployeeGroup(null);
//		}

		List<ARTaskAssigns> taskAssigns = new ArrayList<>();
		ARTaskAssigns taskAssign = null;
		Long newId = null;
		AtomicLong maxId = keyGenerationService.findMaxId(taskAssignsRepository);

		if (dto.getTaskAssigns() != null && !dto.getTaskAssigns().isEmpty()) {
			for (TaskAssignDto taskAssignDto : dto.getTaskAssigns()) {
				taskAssign = taskAssignMapper.buildEntity(taskAssignDto);
				newId = maxId.incrementAndGet();
				maxId = new AtomicLong(newId);

				taskAssign.setId(newId);
				taskAssign.setCampaign(entity);
				taskAssigns.add(taskAssign);
			}
		}
		entity.setTaskAssigns(taskAssigns);

		return entity;
	}
}
