package com.bys.crm.app.dto;

import java.math.BigDecimal;
import java.util.Set;

 
import javax.validation.Valid;

import org.joda.time.DateTime;

 



public class CustomerDto {

	private Long id;

	private String name;

	private DateTime dob;

	private String address;

	private DateTime createdDate;

	private DateTime updatedDate;

	private String updatedUser;

	private String createdUser;

	@Valid
	private LocationDto location;

	private String email1;

	private String customerType;

	private String taxNumber;
	private Integer customerTypeAccountConfigID;

	public Integer getCustomerTypeAccountConfigID() {
		return customerTypeAccountConfigID;
	}

	public void setCustomerTypeAccountConfigID(Integer customerTypeAccountConfigID) {
		this.customerTypeAccountConfigID = customerTypeAccountConfigID;
	}

	@Valid
	private BranchSummaryDto branch;

	private String business;

	private String classify;

	private String email2;

	private String fax;

	private BigDecimal revenueDueYear;

	private String stockCode;

	private String tel1;

	private String tel2;

	private String website;
 
	
	private String customerNo;
	

	
	//New 
	
	private String customerNewOldType;
	
 
	public String getCustomerNewOldType() {
		return customerNewOldType;
	}

	public void setCustomerNewOldType(String customerNewOldType) {
		this.customerNewOldType = customerNewOldType;
	}

	private String customerCapacityProvideProdQuality;
	
 
	private String customerCapacitySupplyProgress;
	
	 
	private String customerCapacityFinancialFinishContract;
	
 
	private String customerCapacityConstructionQuality;
	
 
	private String customerCapacityConstructionManagemen;
	
	 
	private String customerCapacityEnsureLaborSafety;
	
	
	
	public String getCustomerCapacityProvideProdQuality() {
		return customerCapacityProvideProdQuality;
	}

	public void setCustomerCapacityProvideProdQuality(String customerCapacityProvideProdQuality) {
		this.customerCapacityProvideProdQuality = customerCapacityProvideProdQuality;
	}

	public String getCustomerCapacitySupplyProgress() {
		return customerCapacitySupplyProgress;
	}

	public void setCustomerCapacitySupplyProgress(String customerCapacitySupplyProgress) {
		this.customerCapacitySupplyProgress = customerCapacitySupplyProgress;
	}

	public String getCustomerCapacityFinancialFinishContract() {
		return customerCapacityFinancialFinishContract;
	}

	public void setCustomerCapacityFinancialFinishContract(String customerCapacityFinancialFinishContract) {
		this.customerCapacityFinancialFinishContract = customerCapacityFinancialFinishContract;
	}

	public String getCustomerCapacityConstructionQuality() {
		return customerCapacityConstructionQuality;
	}

	public void setCustomerCapacityConstructionQuality(String customerCapacityConstructionQuality) {
		this.customerCapacityConstructionQuality = customerCapacityConstructionQuality;
	}

	public String getCustomerCapacityConstructionManagemen() {
		return customerCapacityConstructionManagemen;
	}

	public void setCustomerCapacityConstructionManagemen(String customerCapacityConstructionManagemen) {
		this.customerCapacityConstructionManagemen = customerCapacityConstructionManagemen;
	}

	public String getCustomerCapacityEnsureLaborSafety() {
		return customerCapacityEnsureLaborSafety;
	}

	public void setCustomerCapacityEnsureLaborSafety(String customerCapacityEnsureLaborSafety) {
		this.customerCapacityEnsureLaborSafety = customerCapacityEnsureLaborSafety;
	}

	private String customerOfficeAddress;
	
	public String getCustomerOfficeAddress() {
		return customerOfficeAddress;
	}

	public void setCustomerOfficeAddress(String customerOfficeAddress) {
		this.customerOfficeAddress = customerOfficeAddress;
	}

	private Boolean customerIsEventInvitation;
	
	 
 
	
	 
	private String customerRepresentativePosition;
	
	 
	private Double customerDebtSales;
	
	
 
	public Boolean getCustomerIsEventInvitation() {
		return customerIsEventInvitation == null ||  customerIsEventInvitation == false ? false : true;
	}

	public void setCustomerIsEventInvitation(Boolean customerIsEventInvitation) {
		this.customerIsEventInvitation = customerIsEventInvitation;
	}

	 
	public String getCustomerRepresentativePosition() {
		return customerRepresentativePosition;
	}

	public void setCustomerRepresentativePosition(String customerRepresentativePosition) {
		this.customerRepresentativePosition = customerRepresentativePosition;
	}

	public Double getCustomerDebtSales() {
		return customerDebtSales;
	}

	public void setCustomerDebtSales(Double customerDebtSales) {
		this.customerDebtSales = customerDebtSales;
	}

	private String customerCategories;
	
 
	private Boolean customerGift;
	
 
	private String customerGiftType;
	
  
	private String customerLawRepresentative;
	
 
	private String customerParentCompany;

 
	private String customerPostCode;
	
	
 
	private Boolean customerIsIPO;
	
	 
	private DateTime customerWorkWithHBCFromDate;
	
	@Valid
	private DepartmentsDto departments;
	
	@Valid
	private CustomerResourcesDto customerResources;
	
	@Valid
	private ActivityDto activity;

	public ActivityDto getActivity() {
		return activity;
	}

	public void setActivity(ActivityDto activity) {
		this.activity = activity;
	}

	public CustomerResourcesDto getCustomerResources() {
		return customerResources;
	}

	public void setCustomerResources(CustomerResourcesDto customerResources) {
		this.customerResources = customerResources;
	}

	private Integer customerTotalContracts;
	
 
	private String customerPaymentStatus;
	
 
	private Integer customerSalesLastYear;
	
	
 
	private Integer customerAbilityToPay;
	
	
 
	private Integer customerAbilityToPayQuickly;
	
	 
	private Integer customerInventoryTurnaround;
	
 
	private Integer customerReceivableTurnaround;
	
 
	private Double customerDebtTotalRatio; // Tỉ số nợ phải thu/ tổng tài sản 
	
 
	private Double customerROE;
	
 
	private Double customerSalesGrowthRate;
	
 
	private Double customerProfitGrowthRate;
	
	
	
	//============
	
	private String customerDescription;
	
	public String getCustomerDescription() {
		return customerDescription;
	}

	public void setCustomerDescription(String customerDescription) {
		this.customerDescription = customerDescription;
	}

	public String getCustomerCategories() {
		return customerCategories;
	}

	public void setCustomerCategories(String customerCategories) {
		this.customerCategories = customerCategories;
	}

	public Boolean getCustomerGift() {
		return customerGift == null ||  customerGift == false ? false : true;
	}

	public void setCustomerGift(Boolean customerGift) {
		this.customerGift = customerGift;
	}

	public String getCustomerGiftType() {
		return customerGiftType;
	}

	public void setCustomerGiftType(String customerGiftType) {
		this.customerGiftType = customerGiftType;
	}

	 

	public String getCustomerLawRepresentative() {
		return customerLawRepresentative;
	}

	public void setCustomerLawRepresentative(String customerLawRepresentative) {
		this.customerLawRepresentative = customerLawRepresentative;
	}

	public String getCustomerParentCompany() {
		return customerParentCompany;
	}

	public void setCustomerParentCompany(String customerParentCompany) {
		this.customerParentCompany = customerParentCompany;
	}

	public String getCustomerPostCode() {
		return customerPostCode;
	}

	public void setCustomerPostCode(String customerPostCode) {
		this.customerPostCode = customerPostCode;
	}

	public Boolean getCustomerIsIPO() {
		
		return customerIsIPO == null ||  customerIsIPO == false ? false : true;
	}

	public void setCustomerIsIPO(Boolean customerIsIPO) {
		this.customerIsIPO = customerIsIPO;
	}

	public DateTime getCustomerWorkWithHBCFromDate() {
		return customerWorkWithHBCFromDate;
	}

	public void setCustomerWorkWithHBCFromDate(DateTime customerWorkWithHBCFromDate) {
		this.customerWorkWithHBCFromDate = customerWorkWithHBCFromDate;
	}

	public DepartmentsDto getDepartments() {
		return departments;
	}

	public void setDepartments(DepartmentsDto departments) {
		this.departments = departments;
	}

	public Integer getCustomerTotalContracts() {
		return customerTotalContracts;
	}

	public void setCustomerTotalContracts(Integer customerTotalContracts) {
		this.customerTotalContracts = customerTotalContracts;
	}

	public String getCustomerPaymentStatus() {
		return customerPaymentStatus;
	}

	public void setCustomerPaymentStatus(String customerPaymentStatus) {
		this.customerPaymentStatus = customerPaymentStatus;
	}

	public Integer getCustomerSalesLastYear() {
		return customerSalesLastYear;
	}

	public void setCustomerSalesLastYear(Integer customerSalesLastYear) {
		this.customerSalesLastYear = customerSalesLastYear;
	}

	public Integer getCustomerAbilityToPay() {
		return customerAbilityToPay;
	}

	public void setCustomerAbilityToPay(Integer customerAbilityToPay) {
		this.customerAbilityToPay = customerAbilityToPay;
	}

	public Integer getCustomerAbilityToPayQuickly() {
		return customerAbilityToPayQuickly;
	}

	public void setCustomerAbilityToPayQuickly(Integer customerAbilityToPayQuickly) {
		this.customerAbilityToPayQuickly = customerAbilityToPayQuickly;
	}

	public Integer getCustomerInventoryTurnaround() {
		return customerInventoryTurnaround;
	}

	public void setCustomerInventoryTurnaround(Integer customerInventoryTurnaround) {
		this.customerInventoryTurnaround = customerInventoryTurnaround;
	}

	public Integer getCustomerReceivableTurnaround() {
		return customerReceivableTurnaround;
	}

	public void setCustomerReceivableTurnaround(Integer customerReceivableTurnaround) {
		this.customerReceivableTurnaround = customerReceivableTurnaround;
	}

	public Double getCustomerDebtTotalRatio() {
		return customerDebtTotalRatio;
	}

	public void setCustomerDebtTotalRatio(Double customerDebtTotalRatio) {
		this.customerDebtTotalRatio = customerDebtTotalRatio;
	}

	public Double getCustomerROE() {
		return customerROE;
	}

	public void setCustomerROE(Double customerROE) {
		this.customerROE = customerROE;
	}

	public Double getCustomerSalesGrowthRate() {
		return customerSalesGrowthRate;
	}

	public void setCustomerSalesGrowthRate(Double customerSalesGrowthRate) {
		this.customerSalesGrowthRate = customerSalesGrowthRate;
	}

	public Double getCustomerProfitGrowthRate() {
		return customerProfitGrowthRate;
	}

	public void setCustomerProfitGrowthRate(Double customerProfitGrowthRate) {
		this.customerProfitGrowthRate = customerProfitGrowthRate;
	}

	 

	public String getCustomerNo() {
		return customerNo;
	}

	public void setCustomerNo(String customerno) {
		this.customerNo = customerno;
	}

	@Valid
	private EmployeeSummaryDto employee;

	@Valid
	private GroupSummaryDto employeeGroup;

	private DateTime companyEstablishmentDay;

	@Valid
	private Set<CustomerContactDto> contacts;

	@Valid
	private Set<CustomerContactGroupDto> customerContactGroups;

	private Integer createdUserId;

	private Integer updatedUserId;

//	private String customerGroup;

	private String group;

	private String employeeNo;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public DateTime getDob() {
		return dob;
	}

	public void setDob(DateTime dob) {
		this.dob = dob;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public DateTime getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(DateTime createdDate) {
		this.createdDate = createdDate;
	}

	public DateTime getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(DateTime updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getUpdatedUser() {
		return updatedUser;
	}

	public void setUpdatedUser(String updatedUser) {
		this.updatedUser = updatedUser;
	}

	public String getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(String createdUser) {
		this.createdUser = createdUser;
	}

	public LocationDto getLocation() {
		return location;
	}

	public void setLocation(LocationDto location) {
		this.location = location;
	}

	public String getEmail1() {
		return email1;
	}

	public void setEmail1(String email1) {
		this.email1 = email1;
	}

	public String getCustomerType() {
		return customerType;
	}

	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}

	public String getTaxNumber() {
		return taxNumber;
	}

	public void setTaxNumber(String taxNumber) {
		this.taxNumber = taxNumber;
	}

	public BranchSummaryDto getBranch() {
		return branch;
	}

	public void setBranch(BranchSummaryDto branch) {
		this.branch = branch;
	}

	public String getBusiness() {
		return business;
	}

	public void setBusiness(String business) {
		this.business = business;
	}

	public String getClassify() {
		return classify;
	}

	public void setClassify(String classify) {
		this.classify = classify;
	}

	public String getEmail2() {
		return email2;
	}

	public void setEmail2(String email2) {
		this.email2 = email2;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public BigDecimal getRevenueDueYear() {
		return revenueDueYear;
	}

	public void setRevenueDueYear(BigDecimal revenueDueYear) {
		this.revenueDueYear = revenueDueYear;
	}

	public String getStockCode() {
		return stockCode;
	}

	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}

	public String getTel1() {
		return tel1;
	}

	public void setTel1(String tel1) {
		this.tel1 = tel1;
	}

	public String getTel2() {
		return tel2;
	}

	public void setTel2(String tel2) {
		this.tel2 = tel2;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public EmployeeSummaryDto getEmployee() {
		return employee;
	}

	public void setEmployee(EmployeeSummaryDto employee) {
		this.employee = employee;
	}

	public GroupSummaryDto getEmployeeGroup() {
		return employeeGroup;
	}

	public void setEmployeeGroup(GroupSummaryDto employeeGroup) {
		this.employeeGroup = employeeGroup;
	}

	public DateTime getCompanyEstablishmentDay() {
		return companyEstablishmentDay;
	}

	public void setCompanyEstablishmentDay(DateTime companyEstablishmentDay) {
		this.companyEstablishmentDay = companyEstablishmentDay;
	}

	public Set<CustomerContactGroupDto> getCustomerContactGroups() {
		return customerContactGroups;
	}

	public void setCustomerContactGroups(Set<CustomerContactGroupDto> customerContactGroups) {
		this.customerContactGroups = customerContactGroups;
	}

	public Integer getCreatedUserId() {
		return createdUserId;
	}

	public void setCreatedUserId(Integer createdUserId) {
		this.createdUserId = createdUserId;
	}

	public Integer getUpdatedUserId() {
		return updatedUserId;
	}

	public void setUpdatedUserId(Integer updatedUserId) {
		this.updatedUserId = updatedUserId;
	}

	public Set<CustomerContactDto> getContacts() {
		return contacts;
	}

	public void setContacts(Set<CustomerContactDto> contacts) {
		this.contacts = contacts;
	}

	public String getEmployeeNo() {
		return employeeNo;
	}

	public void setEmployeeNo(String employeeNo) {
		this.employeeNo = employeeNo;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

}