package com.bys.crm.app.dto;

import java.util.ArrayList;
import java.util.List;

public class ProvinceDto {

	private Long id;
	
	private String name;
	
	private List<DistrictDto> districts = new ArrayList<>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<DistrictDto> getDistricts() {
		return districts;
	}

	public void setDistricts(List<DistrictDto> districts) {
		this.districts = districts;
	}

}
