package com.bys.crm.app.dto;

 
 
public class ColumnAliasDto {
	 
	private Long Id;
	 
  
	 
	private String FieldName;
	
	private String FieldCaption;
	
 
	
	private boolean Hidden;

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		this.Id = id;
	}
 
	public String getFieldName() {
		return FieldName;
	}

	public void setFieldName(String columnAliasName) {
		this.FieldName = columnAliasName;
	}

	public String getFieldCaption() {
		return FieldCaption;
	}

	public void setFieldCaption(String columnAliasCaption) {
		this.FieldCaption = columnAliasCaption;
	}

 
	public boolean getHidden() {
		return Hidden;
	}

	public void setHidden(boolean isHidden) {
		this.Hidden = isHidden;
	}
	
}
