package com.bys.crm.app.dto;

 
import java.util.List;

import javax.validation.Valid;

import org.hibernate.annotations.Type;
 
import org.joda.time.DateTime;

 

 
public class UserFieldConfigsDto {
	 
	private Long id;
	
	private String status;
	
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	private DateTime createdDate;
	
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	private DateTime updatedDate;
	
	private String createdUser;
	
	private String updatedUser;
	
	private List<ColumnAliasDto> columnAliasDto;
	
 
 	public List<ColumnAliasDto> getColumnAliasDto() {
		return columnAliasDto;
	}

	public void setColumnAliasDto(List<ColumnAliasDto> columnAliasDto) {
		this.columnAliasDto = columnAliasDto;
	}

    private List<ColumnAliasDto> columnAlias ;
	
	public List<ColumnAliasDto> getColumnAlias() {
		return columnAlias;
	}

	public void setColumnAlias(List<ColumnAliasDto> columnAlias) {
		this.columnAlias = columnAlias;
	}

	@Valid
	private UserDto user;
	
	private String userFieldConfigValue;
	
	private String userFieldConfigType;

	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public DateTime getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(DateTime createdDate) {
		this.createdDate = createdDate;
	}

	public DateTime getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(DateTime modifiedDate) {
		this.updatedDate = modifiedDate;
	}

	public String getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(String aACreatedUser) {
		createdUser = aACreatedUser;
	}

	public String getUpdatedUser() {
		return updatedUser;
	}

	public void setUpdatedUser(String aAUpdatedUser) {
		updatedUser = aAUpdatedUser;
	}

	 

	 

	public UserDto getUser() {
		return user;
	}

	public void setUser(UserDto user) {
		this.user = user;
	}

	public String getUserFieldConfigValue() {
		return userFieldConfigValue;
	}

	public void setUserFieldConfigValue(String userFieldConfigValue) {
		this.userFieldConfigValue = userFieldConfigValue;
	}

	public String getUserFieldConfigType() {
		return userFieldConfigType;
	}

	public void setUserFieldConfigType(String userFieldConfigType) {
		this.userFieldConfigType = userFieldConfigType;
	}
	
	
	
}
