package com.bys.crm.app.dto;

public class LocationDto {

	private Long id;

	private String name;

	private String type;

	private LocationDto parent;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LocationDto getParent() {
		return parent;
	}

	public void setParent(LocationDto parent) {
		this.parent = parent;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}