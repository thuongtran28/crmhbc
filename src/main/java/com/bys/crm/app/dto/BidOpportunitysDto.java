package com.bys.crm.app.dto;

import java.math.BigDecimal;

 
import org.joda.time.DateTime;
 

public class BidOpportunitysDto {


	private Long id;


	private String status;


	private String createdUser;

	
	private DateTime createdDate;


	private String updatedUser;


	private DateTime updatedDate;
	

	private String projectName;
	

	private String name;
	

	private String opportunityNo;
	

	private String job;
	

	private String place;
	

	private String quarter;
	

	private String customerType;
	

	private String consultantUnit;
	

	private String consultantAddress;
	
	private String consultantPhone;
	

	private BigDecimal floorArea;
	

	private DateTime startTrackingDate;
	

	private DateTime bidSubmissionDate;
	

	private DateTime estimatedResultDate;
	

	private String magnitude;
	

	private String opportunityHBCRole;
	

	private String documentLink;
	

	private Integer progress;
	

	private String acceptanceReason;
	

	private String unacceptanceReason;
	

	private String cancelReason;
	

	private String evaluation;
	

	private DateTime estimatedProjectStartDate;
	 

	private DateTime estimatedProjectEndDate;
	
	private String opportunityStageStatus;
	
	
 
	public String getOpportunityStageStatus() {
		return opportunityStageStatus;
	}




	public void setOpportunityStageStatus(String opportunityStageStatus) {
		this.opportunityStageStatus = opportunityStageStatus;
	}




	private String totalTime;
	
	private String  opportunityStage;
	
	private String  opportunityHSMTStatus;
	
	
 
	public String getOpportunityStage() {
		return opportunityStage;
	}




	public void setOpportunityStage(String opportunityStage) {
		this.opportunityStage = opportunityStage;
	}




	public String getOpportunityHSMTStatus() {
		return opportunityHSMTStatus;
	}




	public void setOpportunityHSMTStatus(String opportunityHSMTStatus) {
		this.opportunityHSMTStatus = opportunityHSMTStatus;
	}




	private  CustomerSummaryDto customer;
	
 
	private  CustomerContactSummaryDto customerContact;
	
 
	private BidConstructionTypesDto constructionType;
	
 
	private BidConstructionCategoriesDto constructionCategorie;
	
	
	private EmployeeSummaryDto chairEmployee;
 
	private OpportunityDto opportunityInfo;
	 
	private BidLocationsDto bidLocation;

	private EmployeeSummaryDto employee;

	

	
	private  CustomerSummaryDto consultantUnitCustomer;

	
	public CustomerSummaryDto getConsultantUnitCustomer() {
		return consultantUnitCustomer;
	}




	public void setConsultantUnitCustomer(CustomerSummaryDto consultantUnitCustomer) {
		this.consultantUnitCustomer = consultantUnitCustomer;
	}




	public BidLocationsDto getBidLocation() {
		return bidLocation;
	}




	public void setBidLocation(BidLocationsDto bidLocation) {
		this.bidLocation = bidLocation;
	}



 



	public Long getId() {
		return id;
	}




	public void setId(Long id) {
		this.id = id;
	}




	public String getStatus() {
		return status;
	}




	public void setStatus(String status) {
		this.status = status;
	}




	public String getCreatedUser() {
		return createdUser;
	}




	public void setCreatedUser(String createdUser) {
		this.createdUser = createdUser;
	}




	public DateTime getCreatedDate() {
		return createdDate;
	}




	public void setCreatedDate(DateTime createdDate) {
		this.createdDate = createdDate;
	}




	public String getUpdatedUser() {
		return updatedUser;
	}




	public void setUpdatedUser(String updatedUser) {
		this.updatedUser = updatedUser;
	}




	public DateTime getUpdatedDate() {
		return updatedDate;
	}




	public void setUpdatedDate(DateTime updatedDate) {
		this.updatedDate = updatedDate;
	}




	public String getProjectName() {
		return projectName;
	}




	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}




	public String getName() {
		return name;
	}




	public void setName(String name) {
		this.name = name;
	}




	public String getOpportunityNo() {
		return opportunityNo;
	}




	public void setOpportunityNo(String opportunityNo) {
		this.opportunityNo = opportunityNo;
	}




	public String getJob() {
		return job;
	}




	public void setJob(String job) {
		this.job = job;
	}




	public String getPlace() {
		return place;
	}




	public void setPlace(String place) {
		this.place = place;
	}




	public String getQuarter() {
		return quarter;
	}




	public void setQuarter(String quarter) {
		this.quarter = quarter;
	}




	public String getCustomerType() {
		return customerType;
	}




	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}




	public String getConsultantUnit() {
		return consultantUnit;
	}




	public void setConsultantUnit(String consultantUnit) {
		this.consultantUnit = consultantUnit;
	}




	public String getConsultantAddress() {
		return consultantAddress;
	}




	public void setConsultantAddress(String consultantAddress) {
		this.consultantAddress = consultantAddress;
	}




	public String getConsultantPhone() {
		return consultantPhone;
	}




	public void setConsultantPhone(String consultantPhone) {
		this.consultantPhone = consultantPhone;
	}




	public BigDecimal getFloorArea() {
		return floorArea;
	}




	public void setFloorArea(BigDecimal floorArea) {
		this.floorArea = floorArea;
	}




	public DateTime getStartTrackingDate() {
		return startTrackingDate;
	}




	public void setStartTrackingDate(DateTime startTrackingDate) {
		this.startTrackingDate = startTrackingDate;
	}




	public DateTime getBidSubmissionDate() {
		return bidSubmissionDate;
	}




	public void setBidSubmissionDate(DateTime bidSubmissionDate) {
		this.bidSubmissionDate = bidSubmissionDate;
	}




	public DateTime getEstimatedResultDate() {
		return estimatedResultDate;
	}




	public void setEstimatedResultDate(DateTime estimatedResultDate) {
		this.estimatedResultDate = estimatedResultDate;
	}




	public String getMagnitude() {
		return magnitude;
	}




	public void setMagnitude(String magnitude) {
		this.magnitude = magnitude;
	}




	public String getOpportunityHBCRole() {
		return opportunityHBCRole;
	}




	public void setOpportunityHBCRole(String opportunityHBCRole) {
		this.opportunityHBCRole = opportunityHBCRole;
	}




	public String getDocumentLink() {
		return documentLink;
	}




	public void setDocumentLink(String documentLink) {
		this.documentLink = documentLink;
	}




	public Integer getProgress() {
		return progress;
	}




	public void setProgress(Integer progress) {
		this.progress = progress;
	}




	public String getAcceptanceReason() {
		return acceptanceReason;
	}




	public void setAcceptanceReason(String acceptanceReason) {
		this.acceptanceReason = acceptanceReason;
	}




	public String getUnacceptanceReason() {
		return unacceptanceReason;
	}




	public void setUnacceptanceReason(String unacceptanceReason) {
		this.unacceptanceReason = unacceptanceReason;
	}




	public String getCancelReason() {
		return cancelReason;
	}




	public void setCancelReason(String cancelReason) {
		this.cancelReason = cancelReason;
	}




	public String getEvaluation() {
		return evaluation;
	}




	public void setEvaluation(String evaluation) {
		this.evaluation = evaluation;
	}




	public DateTime getEstimatedProjectStartDate() {
		return estimatedProjectStartDate;
	}




	public void setEstimatedProjectStartDate(DateTime estimatedProjectStartDate) {
		this.estimatedProjectStartDate = estimatedProjectStartDate;
	}




	public DateTime getEstimatedProjectEndDate() {
		return estimatedProjectEndDate;
	}




	public void setEstimatedProjectEndDate(DateTime estimatedProjectEndDate) {
		this.estimatedProjectEndDate = estimatedProjectEndDate;
	}




	public String getTotalTime() {
		return totalTime;
	}




	public void setTotalTime(String totalTime) {
		this.totalTime = totalTime;
	}




	public CustomerSummaryDto getCustomer() {
		return customer;
	}




	public void setCustomer(CustomerSummaryDto customer) {
		this.customer = customer;
	}




	public  CustomerContactSummaryDto getCustomerContact() {
		return customerContact;
	}




	public void setCustomerContact(CustomerContactSummaryDto customerContact) {
		this.customerContact = customerContact;
	}




	public BidConstructionTypesDto getConstructionType() {
		return constructionType;
	}




	public void setConstructionType(BidConstructionTypesDto constructionType) {
		this.constructionType = constructionType;
	}




	public BidConstructionCategoriesDto getConstructionCategorie() {
		return constructionCategorie;
	}




	public void setConstructionCategorie(BidConstructionCategoriesDto constructionCategorie) {
		this.constructionCategorie = constructionCategorie;
	}




	public BidLocationsDto getBidlocation() {
		return bidLocation;
	}




	public void setBidlocation(BidLocationsDto bidLocation) {
		this.bidLocation = bidLocation;
	}




	public  EmployeeSummaryDto getChairEmployee() {
		return chairEmployee;
	}




	public void setChairEmployee(EmployeeSummaryDto chairEmployee) {
		this.chairEmployee = chairEmployee;
	}




	public OpportunityDto getOpportunityInfo() {
		return opportunityInfo;
	}




	public void setOpportunityInfo(OpportunityDto opportunityInfo) {
		this.opportunityInfo = opportunityInfo;
	}




	public EmployeeSummaryDto getEmployee() {
		return employee;
	}




	public void setEmployee(EmployeeSummaryDto employee) {
		this.employee = employee;
	}
	
	
	
	
	
}
