package com.bys.crm.app.dto;

public class UserPermissionDto {

	private Long id;
	
	private UserDto user;
	
	private PermissionDto permissions;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public UserDto getUser() {
		return user;
	}

	public void setUser(UserDto user) {
		this.user = user;
	}

	public PermissionDto getPermissions() {
		return permissions;
	}

	public void setPermissions(PermissionDto permissions) {
		this.permissions = permissions;
	}
	
}
