package com.bys.crm.app.facade;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bys.crm.app.dto.CustomerResourcesDto;
import com.bys.crm.app.dto.EmployeeDto;
import com.bys.crm.app.dto.constant.ErrorCodeEnum;
import com.bys.crm.app.exception.InvalidException;
import com.bys.crm.app.mapping.CustomerResourcesMapper;
 
import com.bys.crm.domain.erp.constant.AAStatus;
import com.bys.crm.domain.erp.model.ARCustomerResources;
import com.bys.crm.domain.erp.model.HREmployees;
import com.bys.crm.domain.erp.repository.ARCustomersResourcesRepository;
import com.bys.crm.domain.erp.repository.HREmployeesRepository;
 

@Service
public class CustomerResourcesFacade {
	
	@Autowired
	private CustomerResourcesMapper mapper;
	@Autowired
	private HREmployeesRepository employeesRepository;
	
	@Autowired
	private ARCustomersResourcesRepository customersResourcesRepository ;
	
	
	public List<CustomerResourcesDto> getCustomersResourcesList(Integer employeeId) {
		// Validate employee id
		HREmployees employee = employeesRepository.findByStatusAndId(AAStatus.Alive.name(), employeeId);
		if (employee == null) {
			throw new InvalidException("Employee id is not exist.", ErrorCodeEnum.INVALID_REQUEST);
		}

		List<ARCustomerResources> resourcesList = customersResourcesRepository.findByStatusOrderByNameAsc(AAStatus.Alive.name());

		return resourcesList.stream().map(entity -> mapper.buildDto(entity)).collect(Collectors.toList());
	}
}
