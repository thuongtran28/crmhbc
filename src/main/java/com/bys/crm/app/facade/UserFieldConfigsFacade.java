package com.bys.crm.app.facade;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
 

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

 
import com.bys.crm.app.dto.ColumnAliasDto;
import com.bys.crm.app.dto.UserFieldConfigsDto;
import com.bys.crm.app.dto.constant.ErrorCodeEnum;
import com.bys.crm.app.exception.InvalidException;
import com.bys.crm.app.mapping.UserFieldConfigsMapper;
 
import com.bys.crm.app.validation.UserFieldConfigValidator;
import com.bys.crm.domain.erp.constant.AAStatus;
import com.bys.crm.domain.erp.model.AAColumnAlias;
import com.bys.crm.domain.erp.model.ADUsers;
 
import com.bys.crm.domain.erp.model.HREmployees;
import com.bys.crm.domain.erp.model.STUserFieldConfigs;
import com.bys.crm.domain.erp.repository.AAColumnAliasRepository;
import com.bys.crm.domain.erp.repository.ADUsersRepository;
import com.bys.crm.domain.erp.repository.HREmployeesRepository;
import com.bys.crm.domain.erp.repository.STUserFieldConfigsRepository;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class UserFieldConfigsFacade {
	@Autowired
	private UserFieldConfigsMapper mapper;
	@Autowired
	private STUserFieldConfigsRepository userFieldConfigsRepository;
	
	@Autowired
	private ADUsersRepository usersRepository;
	
	@Autowired
	private HREmployeesRepository employeesRepository;
	
	@Autowired
	private AAColumnAliasRepository columnAliasRepository ;
	@Autowired
	private UserFieldConfigValidator validator;
	
	public UserFieldConfigsDto ApplySettingFieldOfFunction (UserFieldConfigsDto dto, Integer employeeId) {
		HREmployees employee = employeesRepository.findByStatusAndId(AAStatus.Alive.name(), employeeId);
		if (employee == null) {
			throw new InvalidException("Employee id is not exist.", ErrorCodeEnum.INVALID_REQUEST);
		}
		ADUsers user =  usersRepository.findByEmployeeIdAndStatus(employee.getId(),AAStatus.Alive.name());
		if (user == null) {
			throw new InvalidException("User id is not exist.", ErrorCodeEnum.INVALID_REQUEST);
		}
		validator.validate(dto);
		 
		STUserFieldConfigs userField = userFieldConfigsRepository.findByStatusAndUserFieldConfigTypeAndUserId(AAStatus.Alive.name(),
				dto.getUserFieldConfigType(), user.getId() );
		
		if (dto.getColumnAlias()== null) {
			//Get value Default 
			List<AAColumnAlias> fieldConfigValue =  columnAliasRepository.findByTableNameAndStatus(dto.getUserFieldConfigType(),AAStatus.Alive.name());
			List<ColumnAliasDto> itemRs = new ArrayList<ColumnAliasDto>();
			if (fieldConfigValue != null) {
				fieldConfigValue.forEach(item->{
					ColumnAliasDto column = new ColumnAliasDto();
					column.setId(item.getId());
					column.setFieldName(item.getColumnAliasName());
					column.setFieldCaption(item.getColumnAliasCaption());
					column.setHidden(item.getHidden());
					itemRs.add(column);
				});
			}
	 
			ObjectMapper mapperjson = new ObjectMapper();
			String json = null;
			if (itemRs != null) {
				try {
				 dto.setColumnAlias(itemRs);
	   			 json = mapperjson.writeValueAsString(itemRs);
	           } catch (JsonProcessingException e) {
	               e.printStackTrace();
	           }
			}
			if (json == null) {
				throw new InvalidException("Field default is not exist.", ErrorCodeEnum.INVALID_REQUEST);
			}
			 
			dto.setUserFieldConfigValue(json);
		}else {
			ObjectMapper mapperjson = new ObjectMapper();
			String json = null;
			try {
	   			 json = mapperjson.writeValueAsString(dto.getColumnAlias());
	           } catch (JsonProcessingException e) {
	               e.printStackTrace();
	           }
			if (json == null) {
				throw new InvalidException("Field default is not exist.", ErrorCodeEnum.INVALID_REQUEST);
			}
			 
			dto.setUserFieldConfigValue(json);
		}
		
		if (userField == null ) {
			//create 
			STUserFieldConfigs entity  = mapper.buildEntity(dto);
			entity.setStatus(AAStatus.Alive.name());
			entity.setCreatedDate(DateTime.now());
			entity.setCreatedUser(employee.getName());
			entity.setUpdatedDate(DateTime.now());
			entity.setUpdatedUser(employee.getName());
			entity.setUser(user); 
			userFieldConfigsRepository.save(entity);
			entity.setUserFieldConfigValue(null);
			entity.setColumnAlias(dto.getColumnAlias());
			return mapper.buildDto(entity);
		}else {
			// Gupdate
			STUserFieldConfigs fieldConfig = userFieldConfigsRepository.findOne(userField.getId());
			fieldConfig.setUpdatedDate(DateTime.now());
			fieldConfig.setUpdatedUser(employee.getName());
			fieldConfig.setUserFieldConfigValue(dto.getUserFieldConfigValue());
			userFieldConfigsRepository.save(fieldConfig);
			fieldConfig.setColumnAlias(dto.getColumnAlias());
			fieldConfig.setUserFieldConfigValue(null);
			return mapper.buildDto(fieldConfig);
		}
	}
	
	
	public UserFieldConfigsDto ResetDefaultSettingFieldOfFunction (UserFieldConfigsDto dto, Integer employeeId) {
		HREmployees employee = employeesRepository.findByStatusAndId(AAStatus.Alive.name(), employeeId);
		if (employee == null) {
			throw new InvalidException("Employee id is not exist.", ErrorCodeEnum.INVALID_REQUEST);
		}
		ADUsers user =  usersRepository.findByEmployeeIdAndStatus(employee.getId(),AAStatus.Alive.name());
		if (user == null) {
			throw new InvalidException("User id is not exist.", ErrorCodeEnum.INVALID_REQUEST);
		}
		
		STUserFieldConfigs userField = userFieldConfigsRepository.findByStatusAndUserFieldConfigTypeAndUserId(AAStatus.Alive.name(),
				dto.getUserFieldConfigType(), user.getId() );
		
		if (dto.getUserFieldConfigType() == null) {
			throw new InvalidException("UserFieldConfigType default is not exist.", ErrorCodeEnum.INVALID_REQUEST);
		}
		//Get value Default 
		List<AAColumnAlias> fieldConfigValue =  columnAliasRepository.findByTableNameAndStatus(dto.getUserFieldConfigType(),AAStatus.Alive.name());
		List<ColumnAliasDto> itemRs = new ArrayList<ColumnAliasDto>();
		if (fieldConfigValue != null) {
			fieldConfigValue.forEach(item->{
				ColumnAliasDto column = new ColumnAliasDto();
				column.setId(item.getId());
				column.setFieldName(item.getColumnAliasName());
				column.setFieldCaption(item.getColumnAliasCaption());
				column.setHidden(item.getHidden());
				itemRs.add(column);
			});
		}
 
		
		
		ObjectMapper mapperjson = new ObjectMapper();
		String json = null;
		if (itemRs != null) {
			try {
   			 json = mapperjson.writeValueAsString(itemRs);
           } catch (JsonProcessingException e) {
               e.printStackTrace();
           }
		}
		if (json == null) {
			throw new InvalidException("Field default is not exist.", ErrorCodeEnum.INVALID_REQUEST);
		}
		//
		if (userField == null ) {
			//create 
			STUserFieldConfigs entity  = mapper.buildEntity(dto);
			entity.setStatus(AAStatus.Alive.name());
			entity.setCreatedDate(DateTime.now());
			entity.setCreatedUser(employee.getName());
			entity.setUpdatedDate(DateTime.now());
			entity.setUpdatedUser(employee.getName());
			entity.setUser(user); 
			entity.setUserFieldConfigValue(json);
			userFieldConfigsRepository.save(entity);
			return mapper.buildDto(entity);
		}else {
			// Get customer
			STUserFieldConfigs fieldConfig = userFieldConfigsRepository.findOne(userField.getId());
			fieldConfig.setStatus(AAStatus.Alive.name());
			fieldConfig.setUpdatedDate(DateTime.now());
			fieldConfig.setUpdatedUser(employee.getName());
			fieldConfig.setUserFieldConfigValue(json);
			userFieldConfigsRepository.save(fieldConfig);
			
			return mapper.buildDto(fieldConfig);
		}
	}
	
	public UserFieldConfigsDto getUserFieldByIdOfFuncType(Integer employeeId, String funcType) {
		// Validate employee id
		//employeeValidator.validateEmployeeId(employeeId);
		HREmployees employee = employeesRepository.findByStatusAndId(AAStatus.Alive.name(), employeeId);
		if (employee == null) {
			throw new InvalidException("Employee id is not exist.", ErrorCodeEnum.INVALID_REQUEST);
		}	
		ADUsers user =  usersRepository.findByEmployeeIdAndStatus(employee.getId(),AAStatus.Alive.name());
		if (user == null) {
			throw new InvalidException("User id is not exist.", ErrorCodeEnum.INVALID_REQUEST);
		}
		
		validator.validate(funcType);
		
		STUserFieldConfigs userField = userFieldConfigsRepository.findByStatusAndUserFieldConfigTypeAndUserId(AAStatus.Alive.name(),funcType, user.getId() );

		if (userField == null) {
			List<AAColumnAlias> fieldConfigValue =  columnAliasRepository.findByTableNameAndStatus(funcType,AAStatus.Alive.name());
			List<ColumnAliasDto> columnAlias = new ArrayList<ColumnAliasDto>();
			if (fieldConfigValue != null) {
				fieldConfigValue.forEach(item->{
					ColumnAliasDto column = new ColumnAliasDto();
					column.setId(item.getId());
					column.setFieldName(item.getColumnAliasName());
					column.setFieldCaption(item.getColumnAliasCaption());
					column.setHidden(item.getHidden());
					columnAlias.add(column);
				});
			}
//			String configValueString = fieldConfigValue.stream ().map (i -> i.toString ()).collect (Collectors.joining (","));
    		STUserFieldConfigs  field = new STUserFieldConfigs();
 
    		//field.setColumnAliasDto(itemRs);
    		field.setColumnAlias(columnAlias); 
			field.setUserFieldConfigValue(null);
			field.setUserFieldConfigType(funcType);
			return mapper.buildDto(field);
		}else {
			
			String json = userField.getUserFieldConfigValue();
			ObjectMapper mapperjson = new ObjectMapper();
    		if (json != null) {
    			try {
    		 		
    				List<ColumnAliasDto> columnAlias = mapperjson.readValue(json, mapperjson.getTypeFactory().constructCollectionType(List.class, ColumnAliasDto.class));
    				userField.setColumnAlias(columnAlias); 
               } catch (IOException  e) {
                   e.printStackTrace();
               }
    		}
    		userField.setUserFieldConfigValue(null);
			return mapper.buildDto(userField);
		}
		  
		
	}
}
