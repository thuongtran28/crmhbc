package com.bys.crm.app.facade;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bys.crm.app.dto.PermissionDto;
import com.bys.crm.app.mapping.GenericMapper;
import com.bys.crm.domain.erp.constant.AAStatus;
import com.bys.crm.domain.erp.model.ADPermissions;
import com.bys.crm.domain.erp.repository.ADPermissionRepository;

@Service
public class PermissionFacade {

	@Autowired
	private ADPermissionRepository repository;
	
	@Autowired
	private GenericMapper genericMapper;
	
	public List<PermissionDto> getPermissions(){		
		List<ADPermissions> entities = this.repository.findByStatusAndType(AAStatus.ALive.name(), "CRM");
		return genericMapper.buildObjects(entities, PermissionDto.class);
	}
}
