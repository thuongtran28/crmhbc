package com.bys.crm.app.facade;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bys.crm.app.dto.EmployeeGroupDto;
import com.bys.crm.app.dto.StatusMessengerEnum;
import com.bys.crm.app.dto.constant.ErrorCodeEnum;
import com.bys.crm.app.exception.InvalidException;
import com.bys.crm.app.mapping.EmployeeGroupMapper;
import com.bys.crm.app.mapping.GenericMapper;
import com.bys.crm.domain.erp.constant.AAStatus;
import com.bys.crm.domain.erp.model.HREmployeeGroups;
import com.bys.crm.domain.erp.model.HREmployees;
import com.bys.crm.domain.erp.repository.HREmployeeGroupReporitory;
import com.bys.crm.domain.erp.repository.HREmployeesRepository;
@Service
public class EmployeeGroupFacade {

	@Autowired
	private GenericMapper mapper;
	@Autowired
	private HREmployeeGroupReporitory repository;
	@Autowired
	private EmployeeGroupMapper employeeGroupMapper;
	@Autowired
	private HREmployeesRepository employeesRepository;

	public List<EmployeeGroupDto> getEmployeeGroups() {
		List<HREmployeeGroups> entities = this.repository.findByStatus(AAStatus.ALive.name());

		return mapper.buildObjects(entities, EmployeeGroupDto.class);
	}

	@Transactional
	public String createEmployeeGroup(Integer employeeId, List<EmployeeGroupDto> employeeGroups) {
		// Validate employee id
		HREmployees employee = employeesRepository.findByStatusAndId(AAStatus.Alive.name(), employeeId);
		if (employee == null) {
			throw new InvalidException("Employee is not exist.", ErrorCodeEnum.DATA_NOT_EXIST);
		}

		List<Integer> employeeIds = new ArrayList<>();

		List<HREmployeeGroups> newEntities = new ArrayList<>();

		employeeGroups.stream().forEach(employeeGroupDto -> {
			employeeIds.add(employeeGroupDto.getEmployeeId());
			newEntities.add(employeeGroupMapper.buildEntity(employeeGroupDto));
		});

		List<HREmployeeGroups> entities = this.repository.findByStatusAndEmployeeIdIn(AAStatus.Alive.name(),
				employeeIds);

		entities.stream().forEach(entity -> {
			entity.setStatus(AAStatus.Delete.name());
		});

		if (entities != null && entities.size() > 0) {
			this.repository.save(entities);
		}

		if (newEntities.size() > 0) {
			this.repository.save(newEntities);
		}

		return StatusMessengerEnum.Successful.name();
	}
}
