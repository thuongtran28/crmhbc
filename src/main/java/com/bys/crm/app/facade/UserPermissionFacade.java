package com.bys.crm.app.facade;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bys.crm.app.dto.StatusMessengerEnum;
import com.bys.crm.app.dto.UserPermissionDto;
import com.bys.crm.app.dto.constant.ErrorCodeEnum;
import com.bys.crm.app.exception.InvalidException;
import com.bys.crm.app.mapping.GenericMapper;
import com.bys.crm.app.mapping.UserPermissionMapper;
import com.bys.crm.domain.erp.constant.AAStatus;
import com.bys.crm.domain.erp.model.ADUserPermissions;
import com.bys.crm.domain.erp.model.HREmployees;
import com.bys.crm.domain.erp.repository.ADUserPermissionRepository;
import com.bys.crm.domain.erp.repository.HREmployeesRepository;

@Service
public class UserPermissionFacade {

	@Autowired
	private GenericMapper genericMapper;
	
	@Autowired
	private UserPermissionMapper userPermissionMapper;

	@Autowired
	private HREmployeesRepository employeesRepository;

	@Autowired
	private ADUserPermissionRepository repository;
	
	public List<UserPermissionDto> getUserPermissions(){
		List<ADUserPermissions> entities = this.repository.findByStatus(AAStatus.Alive.name());

		return genericMapper.buildObjects(entities, UserPermissionDto.class);
	}

	@Transactional
	public List<UserPermissionDto> getUserPermissions(Long userId) {
		List<ADUserPermissions> entities = this.repository.findByStatusAndUserId(AAStatus.Alive.name(), userId);

		return genericMapper.buildObjects(entities, UserPermissionDto.class);
	}

	@Transactional
	public String createUserPermission(Integer employeeId, List<UserPermissionDto> userPermissions) {

		// Validate employee id
		HREmployees employee = employeesRepository.findByStatusAndId(AAStatus.Alive.name(), employeeId);
		if (employee == null) {
			throw new InvalidException("Employee is not exist.", ErrorCodeEnum.DATA_NOT_EXIST);
		}

		List<Long> userIds = new ArrayList<>();
		
		List<ADUserPermissions> newEntities = new ArrayList<>();
		
		userPermissions.stream().forEach(userDto ->{		
			userIds.add(userDto.getUser().getId());
			newEntities.add(userPermissionMapper.buildEntity(userDto));
		});
		 
		List<ADUserPermissions> entities = this.repository.findByStatusAndUserIdIn(AAStatus.Alive.name(), userIds);
		
		entities.stream().forEach(entity ->{
			entity.setStatus(AAStatus.Delete.name());
			entity.setUpdatedDate(DateTime.now());
			entity.setUpdatedUser(employee.getName());
		});
		
		if(entities != null && entities.size() > 0){
			this.repository.save(entities);
		}
		
		if(newEntities.size() > 0){
			this.repository.save(newEntities);
		}
				
		return StatusMessengerEnum.Successful.name();
	}
}
