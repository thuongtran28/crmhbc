package com.bys.crm.app.facade;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bys.crm.app.dto.BidOpportunitysDto;
import com.bys.crm.app.dto.ChartDto;
import com.bys.crm.app.dto.CustomerDto;
import com.bys.crm.app.dto.OpportunityDto;
import com.bys.crm.app.dto.StatusMessengerEnum;
import com.bys.crm.app.dto.TaskAssignDto;
import com.bys.crm.app.dto.constant.ErrorCodeEnum;
import com.bys.crm.app.exception.InvalidException;
import com.bys.crm.app.exception.ResourceNotFoundException;
import com.bys.crm.app.mapping.BidOpportunityMapper;
import com.bys.crm.app.mapping.OpportunityMapper;
import com.bys.crm.app.mapping.TaskAssignMapper;
import com.bys.crm.app.validation.BidOpportunityValidator;
import com.bys.crm.app.validation.BranchValidator;
import com.bys.crm.app.validation.EmployeeValidator;
import com.bys.crm.app.validation.OpportunityValidator;
import com.bys.crm.domain.PageableResult;
import com.bys.crm.domain.erp.constant.AAStatus;
import com.bys.crm.domain.erp.constant.ADConfigKeyGroup;
import com.bys.crm.domain.erp.constant.ChartType;
import com.bys.crm.domain.erp.constant.OpportunityStatus;
import com.bys.crm.domain.erp.model.ADConfigValues;
import com.bys.crm.domain.erp.model.ARBidConstructionCategories;
import com.bys.crm.domain.erp.model.ARBidConstructionTypes;
import com.bys.crm.domain.erp.model.ARBidLocations;
import com.bys.crm.domain.erp.model.ARBidOpportunitys;
import com.bys.crm.domain.erp.model.ARCampaigns;
import com.bys.crm.domain.erp.model.ARCustomerContacts;
import com.bys.crm.domain.erp.model.ARCustomers;
import com.bys.crm.domain.erp.model.AROpportunityContactGroups;
import com.bys.crm.domain.erp.model.AROpportunitys;
import com.bys.crm.domain.erp.model.ARTaskAssigns;
import com.bys.crm.domain.erp.model.BRBranchs;
import com.bys.crm.domain.erp.model.HREmployees;
import com.bys.crm.domain.erp.repository.ADConfigValuesRepository;
import com.bys.crm.domain.erp.repository.ARBidOpportunitysRepository;
import com.bys.crm.domain.erp.repository.ARCustomerContactRepository;
import com.bys.crm.domain.erp.repository.AROpportunityContactGroupsRepository;
import com.bys.crm.domain.erp.repository.AROpportunitysRepository;
import com.bys.crm.domain.erp.repository.ARTaskAssignsRepository;
import com.bys.crm.domain.erp.service.KeyGenerationService;
import com.bys.crm.util.ChartUtil;
import com.bys.crm.util.DateTimeUtil;
import com.bys.crm.util.ListUtil;
import com.bys.crm.util.StringUtil;

@Service
public class OpportunityFacade {
	@Autowired
	private OpportunityMapper opportunityMapper;

	@Autowired
	private BidOpportunityMapper bidOpportunityMapper;
	
	@Autowired
	private AROpportunitysRepository opportunitysRepository;

	@Autowired
	private ARBidOpportunitysRepository bidOpportunitysRepository;
	
	
	@Autowired
	private OpportunityValidator validator;

	@Autowired
	private BidOpportunityValidator bidValidator;
	
	@Autowired
	private ARCustomerContactRepository contactRepository;

	@Autowired
	private AROpportunityContactGroupsRepository groupRepository;

	@Autowired
	private KeyGenerationService keyGenerationService;

	@Autowired
	private EmployeeValidator employeeValidator;

	@Autowired
	private BranchValidator branchValidator;

	@Autowired
	private ADConfigValuesRepository configValuesRepository;

	private static final Logger LOGGER = LoggerFactory.getLogger(OpportunityFacade.class);

	// Create opportunity
	@Transactional
	public BidOpportunitysDto createOpportunity(BidOpportunitysDto dto, Integer employeeId) {
		// Validate employee id
		HREmployees employee = employeeValidator.validateEmployeeId(employeeId);

		// Validation input data
				bidValidator.validate(dto);
				validator.validate(dto.getOpportunityInfo());
		// Validate branch id
		branchValidator.validateBranchId(dto.getOpportunityInfo().getBranch() == null ? null : dto.getOpportunityInfo().getBranch().getId());

		
	
		// Convert data from dto to entity
		
		
		AROpportunitys entity = opportunityMapper.buildEntity(dto.getOpportunityInfo());
 
		//================================
	
		
		entity.setEmployee(employee);
		
		entity.setCreatedDate(DateTime.now());
		entity.setUpdatedDate(DateTime.now());
		entity.setCreatedUser(employee.getName());
		entity.setUpdatedUser(employee.getName());
		entity.setCreatedUserId(employee.getId());
		entity.setUpdatedUserId(employee.getId());
	 
	 
		ARBidOpportunitys entityBid = bidOpportunityMapper.buildEntity(dto);
				
		entity.setCustomer(entityBid.getCustomer());  
		entity.setName(entityBid.getName());
		opportunitysRepository.save(entity);
		entityBid.setStatus(AAStatus.Alive.name());
		entityBid.setCreatedDate(DateTime.now());
		entityBid.setUpdatedDate(DateTime.now());
		entityBid.setCreatedUser(employee.getName());
		entityBid.setUpdatedUser(employee.getName());
		entityBid.setOpportunityInfo(entity);
		entityBid.setEmployee(employee);
		entityBid.setOpportunityStageStatus(OpportunityStatus.CanLapDeNghiDuThau.name());
		entityBid.setOpportunityStage(OpportunityStatus.HSDT.name());
		entityBid.setOpportunityHSMTStatus(OpportunityStatus.CanBoSungHSMT.name());
		bidOpportunitysRepository.save(entityBid);
		return bidOpportunityMapper.buildDto(entityBid);
	}

	// Get opportunity by id
	public BidOpportunitysDto getOpportunityById(Integer employeeId, Long opportunityId) {
		// Validate employee id
		employeeValidator.validateEmployeeId(employeeId);

		ARBidOpportunitys opportunity = bidOpportunitysRepository.findByIdAndStatus(opportunityId, AAStatus.Alive.name());

		if (opportunity == null) {
			throw new InvalidException("Opportunity is not exist.", ErrorCodeEnum.INVALID_REQUEST);
		}

		//HSDT
		List<ADConfigValues> opportunityStages = configValuesRepository.findByStatusAndGroupAndActive(AAStatus.Alive.name(), "BidOpportunityStage", true);
		List<ADConfigValues> hsmtStatus = configValuesRepository.findByStatusAndGroupAndActive(AAStatus.Alive.name(), "BidOpportunityStageHSDTStatus" , true);
		return bidOpportunityMapper.buildDto(opportunity, opportunityStages, hsmtStatus);
	}

	// Edit opportunity
	@Transactional
	public BidOpportunitysDto editOpportunity(BidOpportunitysDto dto, Integer employeeId) {
		// Validate employee id
		HREmployees employee = employeeValidator.validateEmployeeId(employeeId);

		// Validate branch id
		bidValidator.validate(dto);
		validator.validate(dto.getOpportunityInfo());
		BRBranchs branch = branchValidator.validateBranchId(dto.getOpportunityInfo().getBranch() == null ? null : dto.getOpportunityInfo().getBranch().getId());

		// Validation Input data
		

		// Validation id
		if (dto.getId() == null) {
			throw new InvalidException("Id is required.", ErrorCodeEnum.INVALID_REQUEST);
		}

		// Get opportunity
		ARBidOpportunitys bidopportunity = bidOpportunitysRepository.findByIdAndStatus(dto.getId(), AAStatus.Alive.name());
	
		// Check Opportunity is exist or not
		
		if (bidopportunity == null  ) {
			throw new InvalidException("Opportunity is not exist", ErrorCodeEnum.INVALID_REQUEST);
		}
		 
		AROpportunitys opportunity = bidopportunity.getOpportunityInfo();

		if (  opportunity == null ) {
			throw new InvalidException("Opportunity is not exist", ErrorCodeEnum.INVALID_REQUEST);
		}
	    //Edit Bid 
		bidopportunity.setProjectName(dto.getProjectName());
		bidopportunity.setOpportunityNo(dto.getOpportunityNo());
		bidopportunity.setName(dto.getName());
		bidopportunity.setJob(dto.getJob());
		bidopportunity.setPlace(dto.getPlace());
		bidopportunity.setQuarter(dto.getQuarter());
		bidopportunity.setCustomerType(dto.getCustomerType());
		bidopportunity.setConsultantPhone(dto.getConsultantPhone());
		bidopportunity.setFloorArea(dto.getFloorArea());
		bidopportunity.setStartTrackingDate(dto.getStartTrackingDate());
		bidopportunity.setBidSubmissionDate(dto.getBidSubmissionDate());
		bidopportunity.setEstimatedResultDate(dto.getEstimatedResultDate());
		bidopportunity.setMagnitude(dto.getMagnitude());
		bidopportunity.setOpportunityHBCRole(dto.getOpportunityHBCRole());
		bidopportunity.setDocumentLink(dto.getDocumentLink());
		
		bidopportunity.setProgress(dto.getProgress());
		bidopportunity.setAcceptanceReason(dto.getAcceptanceReason());
		bidopportunity.setUnacceptanceReason(dto.getUnacceptanceReason());
		bidopportunity.setCancelReason(dto.getCancelReason());
		bidopportunity.setEvaluation(dto.getEvaluation());
		bidopportunity.setEstimatedProjectStartDate(dto.getEstimatedProjectStartDate());
		bidopportunity.setEstimatedProjectEndDate(dto.getEstimatedProjectEndDate());
		bidopportunity.setTotalTime(dto.getTotalTime());
		
		if (dto.getCustomer() != null && dto.getCustomer().getId() != null) {
			ARCustomers customer = new ARCustomers();
			customer.setId(dto.getCustomer().getId());
			bidopportunity.setCustomer(customer);
		} else {
			bidopportunity.setCustomer(null);
		}
		
		if (dto.getCustomerContact() != null && dto.getCustomerContact().getId() != null) {
			ARCustomerContacts customerContact = new ARCustomerContacts();
			customerContact.setId(dto.getCustomerContact().getId());
			bidopportunity.setCustomerContact(customerContact);
		} else {
			bidopportunity.setCustomerContact(null);
		}
		

		if (dto.getBidLocation() != null && dto.getBidLocation().getId() != null) {
			ARBidLocations bidlocation = new ARBidLocations();
			bidlocation.setId(dto.getBidLocation().getId());
			bidopportunity.setBidlocation(bidlocation);
		} else {
			bidopportunity.setBidlocation(null);
		}
		
		if (dto.getConsultantUnitCustomer() != null && dto.getConsultantUnitCustomer().getId() != null) {
			ARCustomers consultantUnitCustomer = new ARCustomers();
			consultantUnitCustomer.setId(dto.getConsultantUnitCustomer().getId());
			bidopportunity.setConsultantUnitCustomer(consultantUnitCustomer);
		} else {
			bidopportunity.setConsultantUnitCustomer(null);
		}
		if (dto.getConsultantUnitCustomer() != null && dto.getConsultantUnitCustomer().getId() != null) {
			ARCustomers consultantUnitCustomer = new ARCustomers();
			consultantUnitCustomer.setId(dto.getConsultantUnitCustomer().getId());
			bidopportunity.setConsultantUnitCustomer(consultantUnitCustomer);
		} else {
			bidopportunity.setConsultantUnitCustomer(null);
		}
		if (dto.getConstructionType() != null && dto.getConstructionType().getId() != null) {
			ARBidConstructionTypes constructionTypes = new ARBidConstructionTypes();
			constructionTypes.setId(dto.getConstructionType().getId());
			bidopportunity.setConstructionType(constructionTypes);
		} else {
			bidopportunity.setConstructionType(null);
		}
		if (dto.getConstructionCategorie() != null && dto.getConstructionCategorie().getId() != null) {
			ARBidConstructionCategories constructionCategorie = new ARBidConstructionCategories();
			constructionCategorie.setId(dto.getConstructionCategorie().getId());
			bidopportunity.setConstructionCategorie(constructionCategorie);
		} else {
			bidopportunity.setConstructionCategorie(null);
		}
 
		
		if (dto.getChairEmployee() != null && dto.getChairEmployee().getId() != null) {
			HREmployees chairEmployee = new HREmployees();
			chairEmployee.setId(dto.getChairEmployee().getId());
			bidopportunity.setChairEmployee(chairEmployee);
		} else {
			bidopportunity.setChairEmployee(null);
		}
		
		bidopportunity.setUpdatedDate(DateTime.now());
		bidopportunity.setUpdatedUser(employee.getName());
		bidopportunity.setEmployee(employee);
		//Edit root
		opportunity.setName(bidopportunity.getName());
		opportunity.setAmount(dto.getOpportunityInfo().getAmount());
		opportunity.setDescription(dto.getOpportunityInfo().getDescription());
		opportunity.setBranch(branch);
		opportunity.setCustomer(bidopportunity.getCustomer());
		opportunity.setUpdatedDate(DateTime.now());
		opportunity.setUpdatedUser(employee.getName());
		opportunity.setUpdatedUserId(employee.getId());
		opportunity.setEmployee(employee);
		bidOpportunitysRepository.save(bidopportunity);
		opportunitysRepository.save(opportunity);
		
	//	ARBidOpportunitys bidopportunityrs = bidOpportunitysRepository.findByIdAndStatus(dto.getId(), AAStatus.Alive.name());
		
		return bidOpportunityMapper.buildDto(bidopportunity);
	}

	// Delete opportunity
	@Transactional
	public String deleteOpportunity(Long opportunityId, Integer employeeId) {
		// Validate employee id
		HREmployees employee = employeeValidator.validateEmployeeId(employeeId);

		// Check opportunity is exist or not
		ARBidOpportunitys bidopportunity = bidOpportunitysRepository.findByIdAndStatus(opportunityId, AAStatus.Alive.name());
		
		// Check Opportunity is exist or not
		
		if (bidopportunity == null  ) {
			throw new InvalidException("Opportunity is not exist", ErrorCodeEnum.INVALID_REQUEST);
		}
		AROpportunitys opportunity = bidopportunity.getOpportunityInfo();
		if ( opportunity == null ) {
			throw new InvalidException("Opportunity is not exist", ErrorCodeEnum.INVALID_REQUEST);
		}
		bidopportunity.setStatus(AAStatus.Delete.name());
		bidopportunity.setUpdatedDate(DateTime.now());
		bidopportunity.setUpdatedUser(employee.getName());
		bidopportunity.setEmployee(employee); 
		bidOpportunitysRepository.save(bidopportunity);
		// Update status to delete
		opportunity.setStatus(AAStatus.Delete.name());
		opportunity.setUpdatedDate(DateTime.now());
		opportunity.setUpdatedUser(employee.getName());
		opportunity.setUpdatedUserId(employee.getId());
		opportunity.setEmployee(employee); 
		// Save data into DB
		opportunitysRepository.save(opportunity);
		return StatusMessengerEnum.Successful.name();
	}

	// Delete opportunity list
	@Transactional
	public String deleteOpportunityList(List<Long> idList, Integer employeeId) {
		// Validate employee id
		HREmployees employee = employeeValidator.validateEmployeeId(employeeId);

		// Validation input data
		if (idList == null || idList.isEmpty()) {
			throw new InvalidException("Id is required.", ErrorCodeEnum.INVALID_REQUEST);
		}

		AROpportunitys opportunityEntity = null;
		ARBidOpportunitys bidOpportunityEntity = null;

		// Loop id list
		for (Long id : idList) {
			// Get opportunity
			bidOpportunityEntity = bidOpportunitysRepository.findByIdAndStatus(id, AAStatus.Alive.name());

			// Check opportunity is exist or not
			if (bidOpportunityEntity == null) {
				throw new InvalidException("Opportunity is not exist", ErrorCodeEnum.INVALID_REQUEST);
			}

			opportunityEntity  = bidOpportunityEntity.getOpportunityInfo();
					 
			 if (opportunityEntity == null) {
					throw new InvalidException("Opportunity is not exist", ErrorCodeEnum.INVALID_REQUEST);
				}

			// Update status to delete
			 bidOpportunityEntity.setStatus(AAStatus.Delete.name());
			 bidOpportunityEntity.setUpdatedDate(DateTime.now());
			 bidOpportunityEntity.setUpdatedUser(employee.getName());
			 bidOpportunityEntity.setEmployee(employee); 
				bidOpportunitysRepository.save(bidOpportunityEntity);
				
				opportunityEntity.setStatus(AAStatus.Delete.name());
				opportunityEntity.setUpdatedDate(DateTime.now());
				opportunityEntity.setUpdatedUser(employee.getName());
				opportunityEntity.setUpdatedUserId(employee.getId());
				opportunityEntity.setEmployee(employee); 
				// Save data into DB
				opportunitysRepository.save(opportunityEntity);
		}

		// return success message
		return StatusMessengerEnum.Successful.name();
	}

	// Search opportunity
	public PageableResult<BidOpportunitysDto> searchOpportunity(Integer employeeId, String searchKey, Integer pageNumber,
			Integer pageSize, String sortBy, String direct) {
		// Validate employee id
		HREmployees employee = employeeValidator.validateEmployeeId(employeeId);

		if (StringUtils.isNotBlank(searchKey)) {
			try {
				searchKey = "%".concat(StringUtils.trim(URLDecoder.decode(searchKey, "UTF-8"))).concat("%");
			} catch (UnsupportedEncodingException e) {
				LOGGER.error("Decoder keySearch fail" + e);
			}
		} else {
			searchKey = "%";
		}

//		// Get page request list with page number and page size.
//		Pageable opportunityPageRequest = new PageRequest(pageNumber, pageSize,
//				StringUtil.isEmpty(sortBy) ? new Sort(Direction.ASC, "name")
//						: new Sort(Direction.fromStringOrNull(direct), sortBy));
//		String order = StringUtil.isEmpty(sortBy) ? "name ASC"
//				: sortBy + " " + Direction.fromStringOrNull(direct);
		
		// Search opportunity by name, phone, email, website with searchKey
		List<ARBidOpportunitys> opportunityList = bidOpportunitysRepository
				.findByNameLikeOrCustomerNameLikeOrCustomerContactNameLike(searchKey, searchKey, searchKey, searchKey,
						AAStatus.Alive.name());

//		// Convert the list of entity to the list of dto
//		List<OpportunityDto> dtos = null;
//		if (opportunityList != null && opportunityList.getContent() != null
//				&& !opportunityList.getContent().isEmpty()) {
//			dtos = opportunityList.getContent().stream().map(opportunity -> opportunityMapper.buildDto(opportunity))
//					.collect(Collectors.toList());
//		}
//
//		// Return PageableResult
//		return new PageableResult<>(pageNumber, opportunityList.getTotalPages(), opportunityList.getTotalElements(),
//				dtos);

		// Get size of opportunity list
		int listSize = opportunityList.size();

		// Get total page
		int totalPage = listSize / pageSize.intValue();
		if (listSize % pageSize.intValue() > 0) {
			totalPage = totalPage + 1;
		}

		// Get record number for displaying
		int recordNumber = pageSize;
		if (totalPage == (pageNumber + 1) && (listSize % pageSize.intValue()) > 0) {
			recordNumber = listSize % pageSize.intValue();
		}

		// Get result list
		List<BidOpportunitysDto> resultList = new ArrayList<>();
		if (pageNumber < totalPage) {
			for (int i = (pageNumber * pageSize); i < ((pageNumber * pageSize) + recordNumber); i++) {
				resultList.add(bidOpportunityMapper.buildDto(opportunityList.get(i)));
			}
		}

		// Return PageableResult
		return new PageableResult<>(pageNumber, totalPage, Long.valueOf(listSize), resultList);
	}

	// Search opportunity
	public List<OpportunityDto> searchOpportunityByName(Integer employeeId, String opportunityName) {
		// Validate employee id
		HREmployees employee = employeeValidator.validateEmployeeId(employeeId);

		if (StringUtils.isNotBlank(opportunityName)) {
			try {
				opportunityName = "%".concat(StringUtils.trim(URLDecoder.decode(opportunityName, "UTF-8"))).concat("%");
			} catch (UnsupportedEncodingException e) {
				LOGGER.error("Decoder opportunityName fail" + e);
			}
		} else {
			opportunityName = "%";
		}

		// Search opportunity by name
		List<AROpportunitys> opportunityList = opportunitysRepository
				.findByNameLikeAndStatusAndBranchIdInOrderByNameAsc(opportunityName, AAStatus.Alive.name(), ListUtil.convertToArrayId(employee.getBranchs()));

		return opportunityMapper.buildDtos(opportunityList);
	}

	// Filter opportunity
	public PageableResult<BidOpportunitysDto> opportunityFilter(Integer employeeId, String searchKey, String customerType,
			String opportunityStage , Integer chairEmployeeId,  BigDecimal fromAmount ,BigDecimal toAmount, Integer pageNumber
			, Integer pageSize, String sortBy, String direct, Long fromDate,
			Long toDate) {
		// Validate employee id
		HREmployees employee = employeeValidator.validateEmployeeId(employeeId);

		// Validate filter value
		//validator.validateFilter(classify, step);

		if (StringUtils.isNotBlank(searchKey)) {
			try {
				searchKey = "%".concat(StringUtils.trim(URLDecoder.decode(searchKey, "UTF-8"))).concat("%");
			} catch (UnsupportedEncodingException e) {
				LOGGER.error("Decoder keySearch fail" + e);
			}
		} else {
			searchKey = "%";
		}

		// Get page request list with page number and page size.
//		Pageable pageRequest = new PageRequest(pageNumber, pageSize,
//				StringUtil.isEmpty(sortBy) ? new Sort(Direction.ASC, "name")
//						: new Sort(Direction.fromStringOrNull(direct), sortBy));
//		String order = StringUtil.isEmpty(sortBy) ? "name ASC" : sortBy + " " + Direction.fromStringOrNull(direct);

		// Get page request list with page number and page size.
		Pageable pageRequest = new PageRequest(pageNumber, pageSize,
				StringUtil.isEmpty(sortBy) ? new Sort(Direction.DESC, "createdDate")
						: new Sort(Direction.fromStringOrNull(direct), sortBy));
		// Search opportunity with filter
//		List<ARBidOpportunitys> opportunityList =
//				bidOpportunitysRepository.findByClassifyLikeAndStepLikeAndStatus(searchKey,
//				StringUtil.convertSearchKey(customerType), StringUtil.convertSearchKey(opportunityStage),
//				StringUtil.convertSearchKey(chairEmployeeId),  
//						fromAmount == null ? new BigDecimal(0.0) : fromAmount , toAmount == null ? new BigDecimal(9990000000000.0) :toAmount,
//						AAStatus.Alive.name() );
		
		
		Page<ARBidOpportunitys> opportunityList =
				bidOpportunitysRepository.findByClassifyLikeAndStepLikeAndStatus(searchKey,
				StringUtil.convertSearchKey(customerType), StringUtil.convertSearchKey(opportunityStage),
				StringUtil.convertSearchKey(chairEmployeeId),  
						fromAmount == null ? new BigDecimal(0.0) : fromAmount , toAmount == null ? new BigDecimal(9990000000000.0) :toAmount,
						AAStatus.Alive.name() ,  pageRequest);

//		// Convert the list of entity to the list of dto
//		List<OpportunityDto> dtos = null;
//		if (opportunityList != null && opportunityList.getContent() != null
//				&& !opportunityList.getContent().isEmpty()) {
//			dtos = opportunityList.getContent().stream().map(opportunity -> opportunityMapper.buildDto(opportunity))
//					.collect(Collectors.toList());
//		}
//
//		// Return PageableResult
//		return new PageableResult<>(pageNumber, opportunityList.getTotalPages(), opportunityList.getTotalElements(),
//				dtos);
	
		//HSDT
		List<ADConfigValues> opportunityStages = configValuesRepository.findByStatusAndGroupAndActive(AAStatus.Alive.name(), "BidOpportunityStage", true);
		List<ADConfigValues> hsmtStatus = configValuesRepository.findByStatusAndGroupAndActive(AAStatus.Alive.name(), "BidOpportunityStageHSDTStatus" , true);
		//
		// Get size of opportunity list
		List<BidOpportunitysDto> dtos = null;
		if (opportunityList != null && opportunityList.getContent() != null && !opportunityList.getContent().isEmpty()) {
			dtos = opportunityList.getContent().stream().map(opportunity -> bidOpportunityMapper.buildDto(opportunity, opportunityStages, hsmtStatus))
					.collect(Collectors.toList());
		}

		// Return PageableResult
		return new PageableResult<>(pageNumber, opportunityList.getTotalPages(), opportunityList.getTotalElements(), dtos);

		// Return PageableResult
//		return new PageableResult<>(pageNumber, totalPage, Long.valueOf(listSize), resultList);
	}

	@Transactional
	public List<ChartDto> getOpportunityChart(Integer employeeId, Long from, Long to, String type) {
		// Validate employee id
		HREmployees employee = employeeValidator.validateEmployeeId(employeeId);

		List<ChartDto> dtos = null;

		DateTime startDate = DateTimeUtil.toDateTimeAtStartOfDay(from);
		DateTime endDate = DateTimeUtil.toDateTimeAtEndOfDay(to);
		if (startDate.isAfter(endDate)) {
			throw new ResourceNotFoundException("End Date must be greater than Start Date.",
					ErrorCodeEnum.INVALID_DATE);
		}

		List<AROpportunitys> entities = this.opportunitysRepository
				.findByStatusAndBranchIdInAndCreatedDateBetweenOrderByCreatedDate(AAStatus.Alive.name(), ListUtil.convertToArrayId(employee.getBranchs()), startDate, endDate);

		Map<DateTime, Long> counting = null;

		if (entities != null && !entities.isEmpty()) {
			if (ChartType.DAY.value().equals(type)) {
				counting = entities.stream().collect(
						Collectors.groupingBy(AROpportunitys::getCreatedDateChartKeyByDay, Collectors.counting()));
				dtos = counting.entrySet().stream().sorted(Comparator.comparing(e -> e.getKey()))
						.map(e -> new ChartDto(e.getKey(), e.getValue().floatValue())).collect(Collectors.toList());
			} else if (ChartType.MONTH.value().equals(type)) {
				counting = entities.stream().collect(
						Collectors.groupingBy(AROpportunitys::getCreatedDateChartKeyByMonth, Collectors.counting()));
				dtos = ChartUtil.buildChartByMonth(startDate, endDate, counting);
			} else {
				counting = entities.stream().collect(
						Collectors.groupingBy(AROpportunitys::getCreatedDateChartKeyByYear, Collectors.counting()));
				dtos = ChartUtil.buildChartByYear(startDate, endDate, counting);
			}
		}

		return dtos;
	}

	// Get opportunities by customer id
	public PageableResult<OpportunityDto> getOpportunitiesByCustomerId(Integer employeeId, Long customerId,
			Integer pageNumber, Integer pageSize, String sortBy, String direct) {
		// Validate employee id
		HREmployees employee = employeeValidator.validateEmployeeId(employeeId);

		// Get page request list with page number and page size.
		Pageable pageRequest = new PageRequest(pageNumber, pageSize,
				StringUtil.isEmpty(sortBy) ? new Sort(Direction.ASC, "name")
						: new Sort(Direction.fromStringOrNull(direct), sortBy));

		// Get opportunities by customer id
		Page<AROpportunitys> opportunityList = opportunitysRepository.findByCustomerIdAndStatusAndBranchIdIn(customerId,
				AAStatus.Alive.name(), ListUtil.convertToArrayId(employee.getBranchs()), pageRequest);

		// Convert the list of entity to the list of dto
		List<OpportunityDto> dtos = null;
		if (opportunityList != null && opportunityList.getContent() != null
				&& !opportunityList.getContent().isEmpty()) {
			dtos = opportunityList.getContent().stream().map(opportunity -> opportunityMapper.buildDto(opportunity))
					.collect(Collectors.toList());
		}

		// Return PageableResult
		return new PageableResult<>(pageNumber, opportunityList.getTotalPages(), opportunityList.getTotalElements(),
				dtos);
	}

	// Get opportunities by contact id
	public PageableResult<OpportunityDto> getOpportunitiesByContacId(Integer employeeId, Long contactId,
			Integer pageNumber, Integer pageSize, String sortBy, String direct) {
		// Validate employee id
		employeeValidator.validateEmployeeId(employeeId);

		// // Get page request list with page number and page size.
		// Pageable pageRequest = new PageRequest(pageNumber, pageSize,
		// StringUtil.isEmpty(sortBy) ? new Sort(Direction.ASC, "name")
		// : new Sort(Direction.fromStringOrNull(direct), sortBy));

		// Get opportunities by contact id
		// Page<AROpportunitys> opportunityList =
		// opportunitysRepository.findByCustomerContactIdAndStatus(contactId,
		// AAStatus.Alive.name(), pageRequest);
		ARCustomerContacts contact = new ARCustomerContacts();
		contact.setId(contactId);
		List<AROpportunityContactGroups> opportunityContactGroups = groupRepository.findByContactAndContactStatusAndStatus(contact,
				AAStatus.Alive.name(), AAStatus.Alive.name());

		// Convert the list of entity to the list of dto
		List<OpportunityDto> dtos = null;
		if (opportunityContactGroups != null && !opportunityContactGroups.isEmpty()) {
			dtos = opportunityContactGroups.stream().map(group -> opportunityMapper.buildDto(group.getOpportunity()))
					.collect(Collectors.toList());
		}

		int listSize = dtos == null ? 0 : dtos.size();
		// Get total page
		int totalPage = listSize / pageSize.intValue();
		if (listSize % pageSize.intValue() > 0) {
			totalPage = totalPage + 1;
		}

		// Get record number for displaying
		int recordNumber = pageSize;
		if (totalPage == (pageNumber + 1) && (listSize % pageSize.intValue()) > 0) {
			recordNumber = listSize % pageSize.intValue();
		}

		// Get result list
		List<OpportunityDto> resultList = new ArrayList<>();
		if (pageNumber < totalPage) {
			for (int i = (pageNumber * pageSize); i < ((pageNumber * pageSize) + recordNumber); i++) {
				resultList.add(dtos.get(i));
			}
		}

		// Return PageableResult
		return new PageableResult<>(pageNumber, totalPage, Long.valueOf(listSize), resultList);
	}

	// Get contacts and insert to AROpportunityContactGroups
	public List<AROpportunityContactGroups> getContactsAndInsert(OpportunityDto dto, AROpportunitys opportunity, HREmployees employee) {
		List<AROpportunityContactGroups> opportunityContactGroups = new ArrayList<>();

		// Get contact id list
		ArrayList<Long> contactIdList = new ArrayList<>();
		dto.getContacts().forEach(item -> {
			contactIdList.add(item.getId());
		});

		if (!contactIdList.isEmpty()) {
			// Get contactList from DB
			List<ARCustomerContacts> contactList = contactRepository
					.findByIds(contactIdList.toArray(new Long[0]));

			// Convert contacts from ArrayList to Set
			Set<ARCustomerContacts> contacts = new HashSet<ARCustomerContacts>(contactList);

			// Insert data to AROpportunityContactGroups table
			AROpportunityContactGroups opportunityContactGroup;
			AtomicLong maxId = keyGenerationService.findMaxId(groupRepository);
			for (ARCustomerContacts contact : contacts) {
				opportunityContactGroup = new AROpportunityContactGroups();
				opportunityContactGroup.setOpportunity(opportunity);
				opportunityContactGroup.setStatus(AAStatus.Alive.name());
				opportunityContactGroup.setContact(contact);
				opportunityContactGroup.setId(maxId.incrementAndGet());
				opportunityContactGroups.add(opportunityContactGroup);
			}
			;

			groupRepository.save(opportunityContactGroups);
		}
		return opportunityContactGroups;
	}
}
