package com.bys.crm.app.facade;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bys.crm.app.dto.BidConstructionCategoriesDto;
import com.bys.crm.app.dto.BidConstructionTypesDto;
import com.bys.crm.app.dto.BidLocationsDto;
import com.bys.crm.app.dto.ConfigValueDto;
import com.bys.crm.app.dto.ObjectDto;
import com.bys.crm.app.dto.PaymentTermDto;
import com.bys.crm.app.mapping.GenericMapper;
import com.bys.crm.app.validation.BranchValidator;
import com.bys.crm.app.validation.EmployeeValidator;
import com.bys.crm.domain.erp.constant.AAStatus;
import com.bys.crm.domain.erp.constant.ADConfigKeyGroup;
import com.bys.crm.domain.erp.constant.ADObjectType;
import com.bys.crm.domain.erp.model.ADConfigValues;
import com.bys.crm.domain.erp.model.ARBidConstructionCategories;
import com.bys.crm.domain.erp.model.ARBidConstructionTypes;
import com.bys.crm.domain.erp.model.ARBidLocations;
import com.bys.crm.domain.erp.model.ARCustomerContacts;
import com.bys.crm.domain.erp.model.ARCustomers;
import com.bys.crm.domain.erp.model.ARProspectCustomers;
import com.bys.crm.domain.erp.model.GEPaymentTerms;
import com.bys.crm.domain.erp.model.HREmployees;
import com.bys.crm.domain.erp.repository.ADConfigValuesRepository;
import com.bys.crm.domain.erp.repository.ARBidConstructionCategoriesRepository;
import com.bys.crm.domain.erp.repository.ARBidConstructionTypesRepository;
import com.bys.crm.domain.erp.repository.ARBidLocationsRepository;
import com.bys.crm.domain.erp.repository.ARBidOpportunitysRepository;
import com.bys.crm.domain.erp.repository.ARCustomerContactRepository;
import com.bys.crm.domain.erp.repository.ARCustomersRepository;
import com.bys.crm.domain.erp.repository.ARProspectCustomersRepository;
import com.bys.crm.domain.erp.repository.GEPaymentTermsRepository;

@Service
public class CommonFacade {

	@Autowired
	private ARCustomersRepository customersRepository;

	@Autowired
	private ARCustomerContactRepository contactRepository;

	@Autowired
	private ARProspectCustomersRepository prospectRepository;


	@Autowired
	private EmployeeValidator employeeValidator;

	@Autowired
	private BranchValidator branchValidator;
	
	@Autowired
	private GenericMapper genericMapper;

	@Autowired
	private GEPaymentTermsRepository paymentTermsRepository;
	

	@Autowired
	private ARBidLocationsRepository bidLocationsRepository;
	
	@Autowired
	private ARBidConstructionTypesRepository bidConstructionTypesRepository;
	
	@Autowired
	private ARBidConstructionCategoriesRepository bidConstructionCategoriesRepository;
	
	
	// Search customer
	public List<ObjectDto> searchPhoneNumber(Integer employeeId, String phone) {
		// Validate employee id
		HREmployees employee = employeeValidator.validateEmployeeId(employeeId);

		List<ARProspectCustomers> prospectList = prospectRepository.findByPhone(phone, AAStatus.Alive.name(), employee.getBranch().getId());

		List<ARCustomers> customerList = customersRepository.findByPhone(phone, AAStatus.Alive.name(), employee.getBranch().getId());

		List<ARCustomerContacts> contactList = contactRepository.findByPhone(phone, AAStatus.Alive.name(), employee.getBranch().getId());

		List<ObjectDto> objectList = new ArrayList<>();

		if (!prospectList.isEmpty()) {
			prospectList.forEach(prospect -> {
				objectList.add(new ObjectDto(prospect.getLastName() + " " + prospect.getFirstName(),
						prospect.getAddress(), ADObjectType.Prospect.name(), prospect.getId(), prospect.getEmail(), prospect.getCompany(), null,new BigDecimal(0)));
			});
		}

		if (!customerList.isEmpty()) {
			customerList.forEach(customer -> {
				objectList.add(new ObjectDto(customer.getName(), customer.getAddress(), ADObjectType.Customer.name(),
						customer.getId(), customer.getEmail1(), null, customer.getWebsite(), null));
			});
		}

		if (!contactList.isEmpty()) {
			contactList.forEach(contact -> {
				objectList.add(new ObjectDto(contact.getLastName() + " " + contact.getFirstName(), contact.getAddress(),
						ADObjectType.Contact.name(), contact.getId(), contact.getEmail()));
			});
		}

		return objectList;
	}

	// Check exist phone number
	public Boolean isExistPhoneNumber(Integer employeeId, Integer branchId, String phone) {
		// Validate employee id
		employeeValidator.validateEmployeeId(employeeId);

		// Validate branch id
		branchValidator.validateBranchId(branchId);

		List<ARProspectCustomers> prospectList = prospectRepository.findByPhone(phone, AAStatus.Alive.name(), branchId);

		List<ARCustomers> customerList = customersRepository.findByPhone(phone, AAStatus.Alive.name(), branchId);

		List<ARCustomerContacts> contactList = contactRepository.findByPhone(phone, AAStatus.Alive.name(), branchId);

		return !(prospectList.isEmpty() && customerList.isEmpty() && contactList.isEmpty());
	}
	
	
	public List<PaymentTermDto> getPaymentTerms(Integer employeeId) {
		// Validate employee id
		employeeValidator.validateEmployeeId(employeeId);

		List<GEPaymentTerms> paymentTerms = paymentTermsRepository.findByStatus(AAStatus.Alive.name());

		return genericMapper.buildObjects(paymentTerms, PaymentTermDto.class);
	}
	public List<BidLocationsDto> getBidLocations(Integer employeeId) {
		// Validate employee id
		employeeValidator.validateEmployeeId(employeeId);

		List<ARBidLocations> locations = bidLocationsRepository.findByStatus(AAStatus.Alive.name());

		return genericMapper.buildObjects(locations, BidLocationsDto.class);
	}
	public List<BidConstructionTypesDto> getBidConstructionTypes(Integer employeeId) {
		// Validate employee id
		employeeValidator.validateEmployeeId(employeeId);

		List<ARBidConstructionTypes> locations = bidConstructionTypesRepository.findByStatus(AAStatus.Alive.name());

		return genericMapper.buildObjects(locations, BidConstructionTypesDto.class);
	}
	
	public List<BidConstructionCategoriesDto> getBidConstructionCategories(Integer employeeId) {
		// Validate employee id
		employeeValidator.validateEmployeeId(employeeId);

		List<ARBidConstructionCategories> locations = bidConstructionCategoriesRepository.findByStatus(AAStatus.Alive.name());

		return genericMapper.buildObjects(locations, BidConstructionCategoriesDto.class);
	}

}
