package com.bys.crm.app.facade;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bys.crm.app.dto.BranchsDto;
import com.bys.crm.app.dto.constant.ErrorCodeEnum;
import com.bys.crm.app.exception.InvalidException;
import com.bys.crm.app.mapping.GenericMapper;
import com.bys.crm.app.validation.EmployeeValidator;
import com.bys.crm.domain.erp.constant.AAStatus;
import com.bys.crm.domain.erp.model.BRBranchs;
import com.bys.crm.domain.erp.model.HREmployees;

@Service
public class BranchFacade {
	@Autowired
	private GenericMapper mapper;

//	@Autowired
//	private BRBranchsRepository branchsRepository;

	@Autowired
	private EmployeeValidator employeeValidator;

	// Get branch list
	public List<BranchsDto> getBranchsList(Integer employeeId){
		// Validate employee id
		HREmployees employee = employeeValidator.validateEmployeeId(employeeId);

//		List<BRBranchs> branchsList = branchsRepository.findByStatusAndType(AAStatus.Alive.name(), "Central");
		List<BRBranchs> branchsList = employee.getBranchs().stream().filter(branch -> (branch.getId() != null
				&& AAStatus.Alive.name().equals(branch.getStatus())))
				.collect(Collectors.toList());

		if (branchsList.isEmpty()) {
			throw new InvalidException("Branch is not exist.", ErrorCodeEnum.INVALID_REQUEST);
		}

		return mapper.buildObjects(branchsList, BranchsDto.class);
	}

}
