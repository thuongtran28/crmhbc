package com.bys.crm.app.facade;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.bys.crm.app.dto.ActivityDto;
import com.bys.crm.app.dto.StatusMessengerEnum;
import com.bys.crm.app.dto.TaskAssignDto;
import com.bys.crm.app.dto.constant.ErrorCodeEnum;
import com.bys.crm.app.exception.InvalidException;
import com.bys.crm.app.mapping.ActivityMapper;
import com.bys.crm.app.mapping.TaskAssignMapper;
import com.bys.crm.app.validation.ActivityValidator;
import com.bys.crm.app.validation.BranchValidator;
import com.bys.crm.app.validation.EmployeeValidator;
import com.bys.crm.domain.PageableResult;
import com.bys.crm.domain.erp.constant.AAStatus;
import com.bys.crm.domain.erp.constant.ADObjectType;
import com.bys.crm.domain.erp.constant.ActivityObjectType;
import com.bys.crm.domain.erp.constant.ActivityType;
import com.bys.crm.domain.erp.model.ARActivitys;
import com.bys.crm.domain.erp.model.ARTaskAssigns;
import com.bys.crm.domain.erp.model.BRBranchs;
import com.bys.crm.domain.erp.model.HREmployees;
import com.bys.crm.domain.erp.model.HRGroups;
import com.bys.crm.domain.erp.repository.ARActivitysRepository;
import com.bys.crm.domain.erp.repository.ARTaskAssignsRepository;
import com.bys.crm.domain.erp.repository.HREmployeesRepository;
import com.bys.crm.domain.erp.service.KeyGenerationService;
import com.bys.crm.util.FileUtil;
import com.bys.crm.util.ListUtil;
import com.bys.crm.util.StringUtil;

@Service
public class ActivityFacade {
	@Autowired
	private ActivityMapper activityMapper;

	@Autowired
	private HREmployeesRepository employeesRepository;

	@Autowired
	private ARActivitysRepository activitysRepository;

	@Autowired
	private ActivityValidator validator;

	@Autowired
	private EmployeeValidator employeeValidator;

	@Autowired
	private NotificationFacade notificationFacade;

	@Autowired
	private BranchValidator branchValidator;

	@Autowired
	protected KeyGenerationService keyGenerationService;

	@Autowired
	private TaskAssignMapper taskAssignMapper;

	@Autowired
	private ARTaskAssignsRepository taskAssignsRepository;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ActivityFacade.class);

	// Create activity
	@Transactional
	public ActivityDto createActivity(ActivityDto dto, Integer employeeId) {
		// Validate employee id
		HREmployees employee = employeeValidator.validateEmployeeId(employeeId);

		// Validate branch id
		branchValidator.validateBranchId(dto.getBranch() == null ? null : dto.getBranch().getId());

		// Validation input data
		validator.validate(dto);

		// Convert data from dto to entity
		ARActivitys entity = activityMapper.buildEntity(dto);

		entity.setCreatedDate(DateTime.now());
		entity.setUpdatedDate(DateTime.now());
		entity.setCreatedUser(employee.getName());
		entity.setUpdatedUser(employee.getName());
		entity.setCreatedUserId(employee.getId());
		entity.setUpdatedUserId(employee.getId());

		// Save data into DB
		activitysRepository.save(entity);

		// Build notification
		entity.getTaskAssigns().forEach(taskAssign -> {
			notificationFacade.buildNotification(taskAssign.getEmployee(), entity.getBranch(), entity.getActivityType(),
					entity.getId(), entity.getName(), entity.getStartDate(), entity.getEndDate(), null,
					taskAssign.getEmployeeGroup());
		});

		return activityMapper.buildDto(entity);
	}

	// Get activity by id
	public ActivityDto getActivityById(Integer employeeId, Long activityId) {
		// Validate employee id
		employeeValidator.validateEmployeeId(employeeId);

		ARActivitys activity = activitysRepository.findByIdAndStatus(activityId, AAStatus.Alive.name());

		if (activity == null) {
			throw new InvalidException("Activity is not exist.", ErrorCodeEnum.INVALID_REQUEST);
		}

		return activityMapper.buildDto(activity);
	}

	// Edit activity
	@Transactional
	public ActivityDto editActivity(ActivityDto dto, Integer employeeId) {
		// Validate employee id
		HREmployees employee = employeeValidator.validateEmployeeId(employeeId);

		// Validate branch id
		BRBranchs branch = branchValidator.validateBranchId(dto.getBranch() == null ? null : dto.getBranch().getId());

		// Validation Input data
		validator.validate(dto);

		// Validation id
		if (dto.getId() == null) {
			throw new InvalidException("Id is required.", ErrorCodeEnum.INVALID_REQUEST);
		}

		// Get activity
		ARActivitys activity = activitysRepository.findByIdAndStatus(dto.getId(), AAStatus.Alive.name());

		// Check Activity is exist or not
		if (activity == null) {
			throw new InvalidException("Activity is not exist", ErrorCodeEnum.INVALID_REQUEST);
		}

		List<ARTaskAssigns> oldTaskAssigns = activity.getTaskAssigns();

		// Build entity
		activity.setName(dto.getName());
		activity.setAssignedTo(dto.getAssignedTo());
		activity.setStartDate(dto.getStartDate());
		activity.setEndDate(dto.getEndDate());
		activity.setActivityStatus(dto.getActivityStatus());
		activity.setEventType(dto.getEventType());
		activity.setAddress(dto.getAddress());
		activity.setActivityType(dto.getActivityType());
		activity.setActivityObjectTypeId(dto.getActivityObjectTypeId());
		activity.setActivityObjectTypeName(dto.getActivityObjectTypeName());
		activity.setDescription(dto.getDescription());
		activity.setActivityObjectType(dto.getActivityObjectType());
//		if (dto.getEmployee() != null && dto.getEmployee().getId() != null) {
//			HREmployees assignedTo = new HREmployees();
//			assignedTo.setId(dto.getEmployee().getId());
//			activity.setEmployee(assignedTo);
//		} else {
//			activity.setEmployee(null);
//		}
		activity.setUpdatedDate(DateTime.now());
		activity.setUpdatedUser(employee.getName());
//		if (dto.getEmployeeGroup() != null && dto.getEmployeeGroup().getId() != null) {
//			HRGroups group = new HRGroups();
//			group.setId(dto.getEmployeeGroup().getId());
//			activity.setEmployeeGroup(group);
//		} else {
//			activity.setEmployeeGroup(null);
//		}
		activity.setUpdatedUserId(employee.getId());
		activity.setBranch(branch);
		activity.setActivityWorkType(dto.getActivityWorkType());

		List<ARTaskAssigns> taskAssigns = new ArrayList<>();
		ARTaskAssigns taskAssign = null;
		Long newId = null;
		AtomicLong maxId = keyGenerationService.findMaxId(taskAssignsRepository);

		if (dto.getTaskAssigns() != null && !dto.getTaskAssigns().isEmpty()) {
			for (TaskAssignDto taskAssignDto : dto.getTaskAssigns()) {
				taskAssign = taskAssignMapper.buildEntity(taskAssignDto);
				newId = maxId.incrementAndGet();
				maxId = new AtomicLong(newId);

				taskAssign.setId(newId);
				taskAssign.setActivity(activity);
				taskAssigns.add(taskAssign);
			}
		}
		activity.setTaskAssigns(taskAssigns);

		// Delete old task assign
		taskAssignsRepository.deleteInBatch(oldTaskAssigns);

		// Save data into DB
		activitysRepository.save(activity);

		// Update notification
		activity.getTaskAssigns().forEach(taskAsg -> {
			notificationFacade.updateNotification(activity.getActivityType(), activity.getId(), activity.getName(), taskAsg.getEmployee(),
					taskAsg.getEmployeeGroup(), activity.getBranch(), activity.getStartDate(), activity.getEndDate(), activity.getAddress());
		});

		return activityMapper.buildDto(activity);
	}

	// Delete activity
	@Transactional
	public String deleteActivity(Long activityId, Integer employeeId) {
		// Validate employee id
		HREmployees employee = employeeValidator.validateEmployeeId(employeeId);

		// Check activity is exist or not
		ARActivitys activity = activitysRepository.findByIdAndStatus(activityId, AAStatus.Alive.name());
		if (activity == null) {
			throw new InvalidException("Activity is not exist", ErrorCodeEnum.INVALID_REQUEST);
		}

		// Update status to delete
		activity.setStatus(AAStatus.Delete.name());
		activity.setUpdatedDate(DateTime.now());
		activity.setUpdatedUser(employee.getName());
		activity.setUpdatedUserId(employee.getId());

		// Save data into DB
		activitysRepository.save(activity);

		// Delete notification
		notificationFacade.deleteNotification(activity.getActivityType(), activityId);

		return StatusMessengerEnum.Successful.name();
	}

	// Delete activity list
	@Transactional
	public String deleteActivityList(List<Long> idList, Integer employeeId) {
		// Validate employee id
		HREmployees employee = employeeValidator.validateEmployeeId(employeeId);

		// Validation input data
		if (idList == null || idList.isEmpty()) {
			throw new InvalidException("Id is required.", ErrorCodeEnum.INVALID_REQUEST);
		}

		ARActivitys activityEntity = null;

		// Loop id list
		for (Long id : idList) {
			// Get activity
			activityEntity = activitysRepository.findByIdAndStatus(id, AAStatus.Alive.name());

			// Check activity is exist or not
			if (activityEntity == null) {
				throw new InvalidException("Activity is not exist", ErrorCodeEnum.INVALID_REQUEST);
			}

			// Update status to delete
			activityEntity.setStatus(AAStatus.Delete.name());
			activityEntity.setUpdatedDate(DateTime.now());
			activityEntity.setUpdatedUser(employee.getName());
			activityEntity.setUpdatedUserId(employee.getId());

			// Save data into DB
			activitysRepository.save(activityEntity);

			// Delete notification
			notificationFacade.deleteNotification(activityEntity.getActivityType(), id);
		}

		// return success message
		return StatusMessengerEnum.Successful.name();
	}

	// Search activity
	public PageableResult<ActivityDto> searchActivity(Integer employeeId, String searchKey, Integer pageNumber,
			Integer pageSize, String sortBy, String direct) {
		// Validate employee id
		HREmployees employee = employeeValidator.validateEmployeeId(employeeId);

		if (StringUtils.isNotBlank(searchKey)) {
			try {
				searchKey = "%".concat(StringUtils.trim(URLDecoder.decode(searchKey, "UTF-8"))).concat("%");
			} catch (UnsupportedEncodingException e) {
				LOGGER.error("Decoder keySearch fail" + e);
			}
		} else {
			searchKey = "%";
		}

		// Get page request list with page number and page size.
		Pageable activityPageRequest = new PageRequest(pageNumber, pageSize,
				StringUtil.isEmpty(sortBy) ? new Sort(Direction.ASC, "name")
						: new Sort(Direction.fromStringOrNull(direct), sortBy));
		// Search activity by name, phone, email, website with searchKey
		Page<ARActivitys> activityList = activitysRepository.findByNameLikeOrAddressLikeOrEmployeeNameLike(searchKey,
				searchKey, searchKey, AAStatus.Alive.name(), ListUtil.convertToArrayId(employee.getBranchs()), activityPageRequest);

		// Convert the list of entity to the list of dto
		List<ActivityDto> dtos = null;
		if (activityList != null && activityList.getContent() != null && !activityList.getContent().isEmpty()) {
			dtos = activityList.getContent().stream().map(activity -> activityMapper.buildDto(activity))
					.collect(Collectors.toList());
		}

		// Return PageableResult
		return new PageableResult<>(pageNumber, activityList.getTotalPages(), activityList.getTotalElements(), dtos);
	}

	// Import activity from excel file
	@Transactional
	public String importActivityFromExcel(Integer employeeId, MultipartFile file) {
		// Validate employee id
		HREmployees employee = employeeValidator.validateEmployeeId(employeeId);

		// Convert data from excel file to activity dto
		List<ActivityDto> activityDtos = FileUtil.convertExcelToObject(FileUtil.getUploadPath(file).toString(),
				ActivityDto.class, ADObjectType.Activity.name());

		BRBranchs branch = null;
		ARActivitys entity = null;
		List<HREmployees> employees = null;
		for (ActivityDto dto : activityDtos) {
			// Validation input data
			validator.validate(dto);

			// Validate branch id
			branch = branchValidator.validateBranchId(dto.getBranch() == null ? null : dto.getBranch().getId());

			if (StringUtil.isNotEmpty(dto.getEmployeeNo())) {
				// Get employees by employee number
				employees = employeesRepository.findByStatusAndEmployeeNumber(AAStatus.Alive.name(),
						dto.getEmployeeNo());

				// Validation employee number
				if (employees.isEmpty()) {
					throw new InvalidException("Employee number is not exist.", ErrorCodeEnum.DATA_NOT_EXIST);
				}

//				dto.setEmployee(new EmployeeSummaryDto(employees.get(0).getId()));
//			} else {
//				dto.setEmployee(null);
//			}
			}

			entity = activityMapper.buildEntity(dto);
			entity.setCreatedDate(DateTime.now());
			entity.setCreatedUser(employee.getName());
			entity.setUpdatedDate(DateTime.now());
			entity.setUpdatedUser(employee.getName());
			entity.setCreatedUserId(employee.getId());
			entity.setUpdatedUserId(employee.getId());
			entity.setBranch(branch);

			// save data into DB
			activitysRepository.save(entity);

			// Build notification
//			entity.getTaskAssigns().forEach(taskAssign -> {
//				notificationFacade.buildNotification(taskAssign.getEmployee(), entity.getBranch(), entity.getActivityType(),
//						entity.getId(), entity.getName(), entity.getStartDate(), entity.getEndDate(), null,
//						taskAssign.getEmployeeGroup());
//			});
		}

		return StatusMessengerEnum.Successful.name();
	}

	// Get activity by Employee
	public List<ActivityDto> getActivityByEmployee(Integer employeeId, String activityType) {
		// Validate employee id
		HREmployees employee = employeeValidator.validateEmployeeId(employeeId);

		if (ActivityType.fromValue(activityType) == null) {
			throw new InvalidException("Valid ActivityObjectType values is " + ActivityObjectType.supportValues(),
					ErrorCodeEnum.INVALID_ACTIVITY_TYPE);
		}

		// Get activities
		List<ARActivitys> activityList = activitysRepository
				.findByActivityTypeAndTaskAssignsEmployeeIdAndStartDateGreaterThanEqualAndStatusAndBranchIdInOrderByStartDate(
						ActivityType.fromValue(activityType).name(), employeeId, DateTime.now(), AAStatus.Alive.name(), ListUtil.convertToArrayId(employee.getBranchs()));

		return activityMapper.buildDtos(activityList);
	}

	// Get activity by Object Type
	public PageableResult<ActivityDto> getActivityByObjectType(Integer employeeId, String objectType, Long objectId,
			Integer pageNumber, Integer pageSize, String sortBy, String direct) {
		// Validate employee id
		HREmployees employee = employeeValidator.validateEmployeeId(employeeId);

		// Get page request list with page number and page size.
		Pageable activityPageRequest = new PageRequest(pageNumber, pageSize,
				StringUtil.isEmpty(sortBy) ? new Sort(Direction.ASC, "startDate")
						: new Sort(Direction.fromStringOrNull(direct), sortBy));

		// Get activities
		Page<ARActivitys> activityList = activitysRepository.findByActivityObjectTypeAndActivityObjectTypeIdAndStatusAndBranchIdIn(
				objectType, objectId, AAStatus.Alive.name(), ListUtil.convertToArrayId(employee.getBranchs()), activityPageRequest);

		// Convert the list of entity to the list of dto
		List<ActivityDto> dtos = null;
		if (activityList != null && activityList.getContent() != null && !activityList.getContent().isEmpty()) {
			dtos = activityList.getContent().stream().map(activity -> activityMapper.buildDto(activity))
					.collect(Collectors.toList());
		}

		// Return PageableResult
		return new PageableResult<>(pageNumber, activityList.getTotalPages(), activityList.getTotalElements(), dtos);
	}

	// Get activity by Employee and Type
	public PageableResult<ActivityDto> getActivityByEmployeeAndType(Integer employeeId, String activityType,
			Integer pageNumber, Integer pageSize, String sortBy, String direct) {
		// Validate employee id
		HREmployees employee = employeeValidator.validateEmployeeId(employeeId);

		// Get group list
		Set<HRGroups> groups = employee.getGroups();

		// Get group id list
		ArrayList<Integer> groupIdList = new ArrayList<>();
		groups.forEach(group -> {
			groupIdList.add(group.getId());
		});
		Integer[] groupIdArray = groupIdList.isEmpty() ? null : groupIdList.toArray(new Integer[0]);

		// Get page request list with page number and page size.
		Pageable activityPageRequest = new PageRequest(pageNumber, pageSize,
				StringUtil.isEmpty(sortBy) ? new Sort(Direction.ASC, "startDate")
						: new Sort(Direction.fromStringOrNull(direct), sortBy));

		// Get activities
		Page<ARActivitys> activityList = activitysRepository
				.findByActivityTypeAndEmployeeIdAndStartDateGreaterThanEqualAndStatus(
						ActivityType.fromValue(activityType).name(), employeeId, groupIdArray, DateTime.now(),
						AAStatus.Alive.name(), ListUtil.convertToArrayId(employee.getBranchs()), activityPageRequest);

		// Convert the list of entity to the list of dto
		List<ActivityDto> dtos = null;
		if (activityList != null && activityList.getContent() != null && !activityList.getContent().isEmpty()) {
			dtos = activityList.getContent().stream().map(activity -> activityMapper.buildDto(activity))
					.collect(Collectors.toList());
		}

		// Return PageableResult
		return new PageableResult<>(pageNumber, activityList.getTotalPages(), activityList.getTotalElements(), dtos);
	}

	// Filter activity
	public PageableResult<ActivityDto> activityFilter(Integer employeeId, String searchKey, String activityType,
			String activityStatus, Long fromDate, Long toDate, Integer pageNumber, Integer pageSize, String sortBy,
			String direct) {
		// Validate employee id
		HREmployees employee = employeeValidator.validateEmployeeId(employeeId);

		// Validate filter value
		validator.validateFilter(activityType, activityStatus);

		if (StringUtils.isNotBlank(searchKey)) {
			try {
				searchKey = "%".concat(StringUtils.trim(URLDecoder.decode(searchKey, "UTF-8"))).concat("%");
			} catch (UnsupportedEncodingException e) {
				LOGGER.error("Decoder keySearch fail" + e);
			}
		} else {
			searchKey = "%";
		}

		// Get page request list with page number and page size.
		Pageable pageRequest = new PageRequest(pageNumber, pageSize,
				StringUtil.isEmpty(sortBy) ? new Sort(Direction.ASC, "name")
						: new Sort(Direction.fromStringOrNull(direct), sortBy));

		// Search activity with filter
		Page<ARActivitys> activityList = activitysRepository.findByActivityTypeLikeAndActivityStatusLikeAndStatus(
				searchKey, StringUtil.convertSearchKey(activityType), StringUtil.convertSearchKey(activityStatus),
				fromDate == null ? new DateTime(-2211753600000L) : new DateTime(fromDate),
				toDate == null ? new DateTime(4068144000000L) : new DateTime(toDate), AAStatus.Alive.name(), ListUtil.convertToArrayId(employee.getBranchs()),
				pageRequest);

		// Convert the list of entity to the list of dto
		List<ActivityDto> dtos = null;
		if (activityList != null && activityList.getContent() != null && !activityList.getContent().isEmpty()) {
			dtos = activityList.getContent().stream().map(activity -> activityMapper.buildDto(activity))
					.collect(Collectors.toList());
		}

		// Return PageableResult
		return new PageableResult<>(pageNumber, activityList.getTotalPages(), activityList.getTotalElements(), dtos);
	}
}
