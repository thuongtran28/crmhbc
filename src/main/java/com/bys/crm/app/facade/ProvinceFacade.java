package com.bys.crm.app.facade;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bys.crm.app.dto.DistrictDto;
import com.bys.crm.app.dto.ProvinceDto;
import com.bys.crm.app.mapping.GenericMapper;
import com.bys.crm.app.validation.EmployeeValidator;
import com.bys.crm.domain.erp.model.GELocations;
import com.bys.crm.domain.erp.service.GELocationsService;

@Service
public class ProvinceFacade {

	@Autowired
	private GenericMapper mapper;

	@Autowired
	private GELocationsService service;

	@Autowired
	private EmployeeValidator employeeValidator;

	public List<ProvinceDto> getProvinceList(Integer employeeId) {
		// Validate employee id
		employeeValidator.validateEmployeeId(employeeId);

		Map<Long, List<GELocations>> districtMap = new HashMap<>();
		List<GELocations> provinces = service.getProvinceList();

		service.getDistrictList().stream().forEach(item -> {
			GELocations parent = item.getParent();
			if (parent != null) {
				List<GELocations> districts = districtMap.get(parent.getId());
				if (districts == null) {
					districts = new ArrayList<>();
					districtMap.put(parent.getId(), districts);
				}
				districts.add(item);
			}

		});

		return provinces.stream().map(item -> {
			ProvinceDto dto = mapper.buildObject(item, ProvinceDto.class);
			List<GELocations> districts = districtMap.get(item.getId());
			if (CollectionUtils.isNotEmpty(districts)) {
				districts.stream().forEach(district -> {
					DistrictDto districtDto = mapper.buildObject(district, DistrictDto.class);
					dto.getDistricts().add(districtDto);
				});
			}

			return dto;
		}).collect(Collectors.toList());
	}
}
