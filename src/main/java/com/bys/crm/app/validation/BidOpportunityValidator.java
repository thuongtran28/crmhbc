package com.bys.crm.app.validation;

 

import org.joda.time.DateTime;
import org.springframework.stereotype.Component;

import com.bys.crm.app.dto.BidOpportunitysDto;
 
import com.bys.crm.app.dto.constant.ErrorCodeEnum;
import com.bys.crm.app.exception.InvalidException;
import com.bys.crm.domain.erp.constant.OpportunityClassify;
import com.bys.crm.domain.erp.constant.OpportunityStep;
 
import com.bys.crm.util.StringUtil;

@Component
public class BidOpportunityValidator {
	public void validate(BidOpportunitysDto  dto) {
		// Validation Input data
		if (dto == null) {
			throw new InvalidException("Input data is required.", ErrorCodeEnum.INVALID_REQUEST);
		}
		if (dto.getOpportunityInfo() == null) {
			throw new InvalidException("Input data is required (OpportunityInfo is not null).", ErrorCodeEnum.INVALID_REQUEST);
		}
//		if (StringUtil.isEmpty(dto.getProjectName())) {
//			throw new InvalidException("Valid Project Name values is not null",
//					ErrorCodeEnum.INVALID_REQUEST);
//		}
	 
		//Tên gói thầu
		if (StringUtil.isEmpty(dto.getName())) {
			throw new InvalidException("Valid Name values is not null",
					ErrorCodeEnum.INVALID_REQUEST);
		}
//		//địa điểm
//		if (StringUtil.isEmpty(dto.getPlace())) {
//			throw new InvalidException("Valid Place values is not null",
//					ErrorCodeEnum.INVALID_REQUEST);
//		}
		// khu vực
		if (dto.getBidlocation()== null) {
			throw new InvalidException("Valid Bidlocation values is not null",
					ErrorCodeEnum.INVALID_REQUEST);
		}
//		// tên khách hàng
//		if (dto.getCustomer()== null) {
//			throw new InvalidException("Valid Customer values is not null",
//					ErrorCodeEnum.INVALID_REQUEST);
//		}
		// Loại công trình
		if (dto.getConstructionType()== null) {
			throw new InvalidException("Valid ConstructionType values is not null",
					ErrorCodeEnum.INVALID_REQUEST);
		}
		
		 
//		// đơn vị tư vấn 
//		if (dto.getConsultantUnitCustomer()== null) {
//			throw new InvalidException("Valid ConsultantUnitCustomer values is not null",
//					ErrorCodeEnum.INVALID_REQUEST);
//		}
		// Hạn mục thi công chính 
		
		
		if (dto.getConstructionCategorie()== null) {
			throw new InvalidException("Valid ConstructionCategorie values is not null",
					ErrorCodeEnum.INVALID_REQUEST);
		}
		
//		//Chủ trì
//		
//		if (dto.getChairEmployee()== null) {
//			throw new InvalidException("Valid ChairEmployee values is not null",
//					ErrorCodeEnum.INVALID_REQUEST);
//		}
//		
//		// Gia đoạn
//		
//		if (dto.getProgress() != null) {
//			throw new InvalidException("Valid Progress values is not null",
//					ErrorCodeEnum.INVALID_REQUEST);
//		}
//		if (dto.getEvaluation() != null) {
//			throw new InvalidException("Valid Evaluation values is not null",
//					ErrorCodeEnum.INVALID_REQUEST);
//		}
		
		
//		if (dto.getBidSubmissionDate()==null) {
//			throw new InvalidException("Valid Bid Submission Date values is not null",
//					ErrorCodeEnum.INVALID_REQUEST);
//		}
//		if (dto.getEstimatedResultDate()==null) {
//			throw new InvalidException("Valid Estimated Result Date values is not null",
//					ErrorCodeEnum.INVALID_REQUEST);
//		}
		
		
//		//&& new Date(dto.getStartTrackingDate())  >  new Date(dto.getBidSubmissionDate())
		if (dto.getStartTrackingDate()!=null && dto.getBidSubmissionDate()!=null && dto.getEstimatedResultDate()!=null) {
			DateTime startTrackingDate = new DateTime(dto.getStartTrackingDate());
			DateTime bidSubmissionDate = new DateTime(dto.getBidSubmissionDate());
			if (startTrackingDate.isAfter(bidSubmissionDate)) {
				throw new InvalidException("BidSubmissionDate must less than or equal to values is StartTrackingDate" ,
						ErrorCodeEnum.IVALID_DATE);
			}
		}
		
		
		if(dto.getBidSubmissionDate()!=null && dto.getEstimatedResultDate()!=null) {
			DateTime bidSubmissionDate = new DateTime(dto.getBidSubmissionDate());
			DateTime estimatedResultDate = new DateTime(dto.getEstimatedResultDate());
			if (bidSubmissionDate.isAfter(estimatedResultDate) ) {
				throw new InvalidException("EstimatedResultDate must less than or equal to values is BidSubmissionDate",
						ErrorCodeEnum.INVALID_REQUEST);
			}
		}
		
		
		if(dto.getEstimatedProjectStartDate() != null && dto.getEstimatedProjectEndDate() != null) {
			DateTime estimatedProjectStartDate = new DateTime(dto.getEstimatedProjectStartDate());
			DateTime estimatedProjectEndDate = new DateTime(dto.getEstimatedProjectEndDate());
			if (estimatedProjectStartDate.isAfter(estimatedProjectEndDate) ) {
				throw new InvalidException("EstimatedProjectEndDate must less than or equal to values is EstimatedProjectStartDate",
						ErrorCodeEnum.INVALID_REQUEST);
			}
		}
		
	 

		 
	}

	public void validateFilter(String classify, String step) {
		if (StringUtil.isNotEmpty(classify) && OpportunityClassify.fromName(classify) == null) {
			throw new InvalidException("Valid OpportunityClassify values is " + OpportunityClassify.supportValues(),
					ErrorCodeEnum.INVALID_TYPE);
		}

		if (StringUtil.isNotEmpty(step) && OpportunityStep.fromName(step) == null) {
			throw new InvalidException("Valid OpportunityStep values is " + OpportunityStep.supportValues(),
					ErrorCodeEnum.INVALID_TYPE);
		}
	}

}
