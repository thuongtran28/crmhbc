package com.bys.crm.app.validation;

import org.springframework.stereotype.Component;

import com.bys.crm.app.dto.UserFieldConfigsDto;
import com.bys.crm.app.dto.constant.ErrorCodeEnum;
import com.bys.crm.app.exception.InvalidException;
import com.bys.crm.domain.erp.constant.UserFieldConfigs;
import com.bys.crm.util.StringUtil;

@Component
public class UserFieldConfigValidator {
	public void validate(UserFieldConfigsDto dto) {
		if (StringUtil.isEmpty(dto.getUserFieldConfigType())) {
			throw new InvalidException("Valid UserFieldConfigType values is " + UserFieldConfigs.supportValues(),
					ErrorCodeEnum.INVALID_TYPE);
		}
		if (StringUtil.isNotEmpty(dto.getUserFieldConfigType())
				&& UserFieldConfigs.fromName(dto.getUserFieldConfigType()) == null) {
			throw new InvalidException("Valid UserFieldConfigType values is " + UserFieldConfigs.supportValues(),
					ErrorCodeEnum.INVALID_TYPE);
		}
	}
	public void validate(String funcType) {
		if (StringUtil.isEmpty(funcType)) {
			throw new InvalidException("Valid funcType values is " + UserFieldConfigs.supportValues(),
					ErrorCodeEnum.INVALID_TYPE);
		}
		if (StringUtil.isNotEmpty(funcType)
				&& UserFieldConfigs.fromName(funcType) == null) {
			throw new InvalidException("Valid funcType values is " + UserFieldConfigs.supportValues(),
					ErrorCodeEnum.INVALID_TYPE);
		}
	}
}
