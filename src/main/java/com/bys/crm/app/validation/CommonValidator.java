package com.bys.crm.app.validation;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bys.crm.domain.erp.repository.GELocationsRepository;
import com.bys.crm.app.dto.ActivityDto;
import com.bys.crm.app.dto.ActivitySummaryDto;
import com.bys.crm.app.dto.EmployeeSummaryDto;
import com.bys.crm.app.dto.GroupSummaryDto;
import com.bys.crm.app.dto.LocationDto;
import com.bys.crm.app.exception.ResourceNotFoundException;
 
import com.bys.crm.domain.erp.model.GELocations;
import com.bys.crm.app.dto.constant.ErrorCodeEnum;
import com.bys.crm.app.exception.InvalidException;
import com.bys.crm.domain.erp.service.CommonService;

@Component
public class CommonValidator {

	@Autowired
	public CommonService commonService;

	@Autowired
	private GELocationsRepository locationsRepository;

	// Check exist phone number
	public void checkExistNumberPhone(Integer branchId, String phone) {
		if (commonService.isExistPhoneNumber(branchId, phone)) {
			throw new InvalidException("Phone number already existed", ErrorCodeEnum.EXIST_PHONE_NUMBER);
		};
	}
	public void checkExistNumberPhoneAndName(Integer branchId, String phone, String name) {
		if (commonService.isExistPhoneNumberAndNameCustomer(branchId, phone, name)) {
			throw new InvalidException("Phone number already existed", ErrorCodeEnum.EXIST_PHONE_NUMBER);
		};
	}

	public void isExistOnlyPhoneNumberContact(Integer branchId, String phone) {
		if (commonService.isExistOnlyPhoneNumberContact(branchId, phone)) {
			throw new InvalidException("Phone number already existed", ErrorCodeEnum.EXIST_PHONE_NUMBER);
		};
	}
	public void checkExistsCustomerGiftType(Boolean isGift, String customerGiftType) {
		if(isGift && customerGiftType == null) {
			throw new InvalidException("Invalid Gift type", ErrorCodeEnum.INVALID_TYPE);
		}
	}
	
	public void checkExistsCustomerEvent(Boolean isInvitation, ActivityDto customerEvent) {
		if((isInvitation &&  (customerEvent != null && customerEvent.getId() == null)) || 
				(isInvitation && customerEvent == null)) {
			throw new InvalidException("Invalid Customer Event", ErrorCodeEnum.INVALID_TYPE);
		}
	}
	
	public void checkValidateCustomerType(String type, DateTime dob, DateTime companyEstablishmentDay) {
		if(type.equals("Personal") ) {  
			// chọn khách hàng cá nhân mà chon ngày thành thập tì 
			if (companyEstablishmentDay != null ) {
				throw new InvalidException("Customer Type is not exists CompanyEstablishmentDay", ErrorCodeEnum.INVALID_TYPE);
			}
		} else {
			if (dob != null ) {
				throw new InvalidException("Customer Type is not exists dob", ErrorCodeEnum.INVALID_TYPE);
			}
		}
	}
	
	public void checkExistsAssignment(EmployeeSummaryDto employee, GroupSummaryDto employeeGroup) {
		if((employee == null && employeeGroup == null )) {
			throw new InvalidException("Invalid Assignment", ErrorCodeEnum.INVALID_TYPE);
		}else if (employee!=null && employee.getId()== null) {
			throw new InvalidException("Invalid Assignment Employee", ErrorCodeEnum.INVALID_TYPE);
		}else if (employeeGroup!=null && employeeGroup.getId()== null) {
			throw new InvalidException("Invalid Assignment Group", ErrorCodeEnum.INVALID_TYPE);
		}else if(employee != null && employeeGroup != null) {
			throw new InvalidException("Invalid Assignment Group Employee", ErrorCodeEnum.INVALID_TYPE);
		}
	}
	public void checkvalidateContactRelationHB(String key) {
		if (commonService.isExistsContactRelationHBC("customer-categories",key)==false) {
			throw new InvalidException("Invalid Contact Relation HBC", ErrorCodeEnum.INVALID_TYPE);
		};
	}
	
	public void validateLocation(LocationDto locationDto) {
		GELocations location = this.locationsRepository.findOne(locationDto.getId());
		if (location == null) {
			throw new ResourceNotFoundException("Location id not found");
		}
		if(location.getParent() != null && locationDto.getParent() != null){
			if (!location.getParent().getId().equals(locationDto.getParent().getId())) {
				throw new InvalidException("Mismatch province and district", ErrorCodeEnum.INVALID_ADDRESS);
			}
		}
	}
}
