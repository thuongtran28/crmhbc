package com.bys.crm.app.validation;

import org.springframework.stereotype.Component;

import com.bys.crm.app.dto.CustomerContactDto;
import com.bys.crm.app.dto.constant.ErrorCodeEnum;
import com.bys.crm.app.exception.InvalidException;
import com.bys.crm.domain.erp.constant.Name;
import com.bys.crm.domain.erp.constant.PotentialSource;
import com.bys.crm.domain.erp.constant.ShareholderType;
import com.bys.crm.util.StringUtil;

@Component
public class ContactValidator {

	public void validate(CustomerContactDto dto) {
		// Validation Input data
		if (dto == null) {
			throw new InvalidException("Input data is required.", ErrorCodeEnum.INVALID_REQUEST);
		}

		if (StringUtil.isNotEmpty(dto.getTitle()) && Name.fromName(dto.getTitle()) == null) {
			throw new InvalidException("Invalid Name values is " + Name.supportValues(),
					ErrorCodeEnum.INVALID_TYPE);
		}
		
		if (StringUtil.isEmpty(dto.getContactRelationHBC())) {
			throw new InvalidException("Invalid ContactRelationHBC values is not null" ,
					ErrorCodeEnum.INVALID_TYPE);
		}
		
//		if (StringUtil.isEmpty(dto.getContactSignificantType())) {
//			throw new InvalidException("Invalid Contact Significant Type values is not null" ,
//					ErrorCodeEnum.INVALID_TYPE);
//		}
		
		if (StringUtil.isNotEmpty(dto.getContactSignificantType()) && ShareholderType.fromName(dto.getContactSignificantType()) == null) {
			throw new InvalidException("Invalid Significant Type values is " + ShareholderType.supportValues(),
					ErrorCodeEnum.INVALID_TYPE);
		}
		
		
		if (StringUtil.isEmpty(dto.getFirstName())) {
			throw new InvalidException("Invalid Frist Name values is not empty",
					ErrorCodeEnum.INVALID_TYPE);
		}
		if (StringUtil.isEmpty(dto.getLastName())) {
			throw new InvalidException("Invalid Last Name values is not empty",
					ErrorCodeEnum.INVALID_TYPE);
		}
		if (StringUtil.isEmpty(dto.getCellularPhone())) {
			throw new InvalidException("Invalid Cellular Phone Number values is not empty",
					ErrorCodeEnum.INVALID_TYPE);
		}
		// KHông có nguồn tiềm năng
//		if (StringUtil.isNotEmpty(dto.getPotentialSource()) && PotentialSource.fromName(dto.getPotentialSource()) == null) {
//			throw new InvalidException("Valid PotentialSource values is " + PotentialSource.supportValues(),
//					ErrorCodeEnum.INVALID_TYPE);
//		}
	}

	public void validateFilter(String potentialSource) {
		if (StringUtil.isNotEmpty(potentialSource) && PotentialSource.fromName(potentialSource) == null) {
			throw new InvalidException("Valid PotentialSource values is " + PotentialSource.supportValues(),
					ErrorCodeEnum.INVALID_TYPE);
		}
	}
}
