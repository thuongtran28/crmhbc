package com.bys.crm.domain.erp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.bys.crm.domain.erp.model.ARTaskAssigns;

@Repository
public interface ARTaskAssignsRepository extends JpaRepository<ARTaskAssigns, Long>, BaseRepository {
	@Query("SELECT max(entity.id) FROM ARTaskAssigns entity")
	Long findMaxId();
}
