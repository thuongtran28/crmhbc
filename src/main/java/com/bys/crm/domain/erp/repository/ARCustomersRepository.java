package com.bys.crm.domain.erp.repository;

import java.util.List;

import org.joda.time.DateTime;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.bys.crm.domain.erp.model.ARCustomers;

@Repository
public interface ARCustomersRepository extends JpaRepository<ARCustomers, Long>, BaseRepository {

	List<ARCustomers> findByCustomerTypeAndBranchIdIn(String customerType, Integer[] branchIds);

	@Query("SELECT max(entity.id) FROM ARCustomers entity")
	Long findMaxId();

	List<ARCustomers> findByEmail1AndTel1AndStatusAndBranchIdInOrderByIdDesc(String email, String phone, String status,
			Integer[] branchIds);

	List<ARCustomers> findByEmail1AndStatusAndBranchIdInOrderByIdDesc(String email, String status, Integer[] branchIds);

	List<ARCustomers> findByStatusAndBranchIdInAndCreatedDateBetweenOrderByCreatedDate(String status,
			Integer[] branchIds, DateTime startDate, DateTime endDate);

	ARCustomers findByIdAndStatus(Long id, String status);

	List<ARCustomers> findByNameAndStatusAndBranchIdIn(String name, String status, Integer[] branchIds);

	List<ARCustomers> findByStatusAndBranchIdIn(String status, Integer[] branchIds, Sort sort);

	Page<ARCustomers> findByStatusAndBranchIdIn(String status, Integer[] branchIds, Pageable requestPage);

	@Query("SELECT entity FROM ARCustomers entity" + " LEFT JOIN entity.employee employee WHERE"
			+ " ( entity.name LIKE ?1" + " OR entity.tel1 LIKE ?2" + " OR entity.email1 LIKE ?3"
			+ " OR entity.website LIKE ?4" + " OR employee.name LIKE ?5 )" + " AND entity.status = ?6")
	Page<ARCustomers> findByNameLikeOrTel1LikeOrEmailLikeOrWebsiteLike(String name, String phone, String email,
			String website, String assignedTo, String status, Pageable requestPage);

	@Query("SELECT entity FROM ARCustomers entity WHERE ('%' = ?1 OR entity.name LIKE ?1)  AND entity.status = ?2")
	Page<ARCustomers> findByNameLikeAndStatus(String name, String status, Pageable requestPage);

	@Query("SELECT entity FROM ARCustomers entity" + " LEFT JOIN entity.employee employee WHERE"
			+ " ( entity.name LIKE ?1" + " OR entity.tel1 LIKE ?1" + " OR entity.email1 LIKE ?1"
			+ " OR entity.website LIKE ?1" + " OR employee.name LIKE ?1 OR entity.customerParentCompany LIKE ?8 )"
			+ " AND ('%' = ?2 OR entity.business = ?2)" + " AND ('%' = ?3 OR entity.group = ?3)"
			+ " AND ((entity.dob BETWEEN ?4 AND ?5) OR (entity.dob BETWEEN ?4 AND ?5) OR (entity.companyEstablishmentDay BETWEEN ?4 AND ?5))"
			+ " AND entity.status = ?6" + " AND ('%' = ?7 OR entity.customerCategories = ?7)")
	Page<ARCustomers> findByBusinessLikeAndGroupLikeAndEvaluateLikeAndStatus(String searchKey, String business,
			String group, DateTime fromDate, DateTime toDate, String status, String customerCategories,
			String customerParentCompany, Pageable requestPage);

	@Query("SELECT entity FROM ARCustomers entity" + " LEFT JOIN entity.employee employee WHERE"
			+ " ( entity.name LIKE ?1" + " OR entity.tel1 LIKE ?1" + " OR entity.email1 LIKE ?1"
			+ " OR entity.website LIKE ?1" + " OR employee.name LIKE ?1 OR entity.customerParentCompany LIKE ?8 )"
			+ " AND ('%' = ?2 OR entity.business = ?2)" + " AND ('%' = ?3 OR entity.group = ?3)"
			+ " AND ((entity.createdDate BETWEEN ?4 AND ?5) )" + " AND entity.status = ?6"
			+ " AND ('%' = ?7 OR entity.customerCategories = ?7)")
	Page<ARCustomers> findByBusinessLikeAndGroupLikeAndEvaluateLikeAndStatusExport(String searchKey, String business,
			String group, DateTime fromDate, DateTime toDate, String status, String customerCategories,
			String customerParentCompany, Pageable requestPage);

	@Query("SELECT entity FROM ARCustomers entity" + " LEFT JOIN entity.employee employee WHERE"
			+ " (entity.name LIKE ?1" + " OR entity.tel1 LIKE ?1" + " OR entity.email1 LIKE ?1)"
			+ " AND ('%' = ?2 OR entity.business = ?2)" + " " + "AND ('%' = ?3 OR entity.group = ?3)"
			+ " AND ('%' = ?4 OR entity.customerCategories = ?4)" + " AND entity.status = ?5")
	Page<ARCustomers> findByBusinessLikeAndGroupLikeAndBussness(String searchkey, String business, String group,
			String customerCategories, String status, Pageable requestPage);

	// %hung%
	// Get customer list by array id
	@Query("SELECT entity FROM ARCustomers entity" + " WHERE entity.id IN ?1")
	List<ARCustomers> findByIds(Long[] id);

	// Search phone
//	@Query("SELECT entity FROM ARCustomers entity"
//			+ " WHERE (entity.phone = ?1" + " OR entity.companyPhone = ?1 " + " OR entity.tel1 = ?1 " + " OR entity.tel2 = ?1)"
//			+ " AND entity.status = ?2" + " AND entity.branch.id = ?3")
//	List<ARCustomers> findByPhone(String phone, String status, Integer branchId);
	@Query("SELECT entity FROM ARCustomers entity" + " WHERE (entity.tel1 = ?1 " + " OR entity.tel2 = ?1)"
			+ " AND entity.status = ?2" + " AND entity.branch.id = ?3")
	List<ARCustomers> findByPhone(String phone, String status, Integer branchId);

	@Query("SELECT entity FROM ARCustomers entity" + " WHERE (entity.tel1 = ?1 " + " OR entity.tel2 = ?1)"
			+ " AND entity.status = ?2" + " AND entity.name = ?3  AND entity.branch.id = ?4")
	List<ARCustomers> findByPhoneAndName(String phone, String status, String name, Integer branchId);
}
