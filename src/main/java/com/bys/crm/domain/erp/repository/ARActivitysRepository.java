package com.bys.crm.domain.erp.repository;

import java.util.List;

import org.joda.time.DateTime;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.bys.crm.domain.erp.model.ARActivitys;

@Repository
public interface ARActivitysRepository extends JpaRepository<ARActivitys, Long>, BaseRepository {
	@Query("SELECT max(entity.id) FROM ARActivitys entity")
	Long findMaxId();

	ARActivitys findByIdAndStatus(Long id, String status);

	List<ARActivitys> findByStatusAndBranchIdIn(String status, Integer[] branchIds, Sort sort);

	Page<ARActivitys> findByStatusAndBranchIdIn(String status, Integer[] branchIds, Pageable pageRequest);

	@Query("SELECT DISTINCT entity FROM ARActivitys entity"
			+ " LEFT JOIN entity.taskAssigns taskAssigns" 
			+ " LEFT JOIN taskAssigns.employee employee" + " WHERE "
			+ "( entity.name LIKE ?1" + " OR entity.address LIKE ?2" + " OR employee.name LIKE ?3 )"
			+ " AND entity.status = ?4" + " AND entity.branch.id IN ?5")
	Page<ARActivitys> findByNameLikeOrAddressLikeOrEmployeeNameLike(String name, String address, String assignedTo,
			String status, Integer[] branchIds, Pageable requestPage);

	List<ARActivitys> findByActivityTypeAndTaskAssignsEmployeeIdAndStartDateGreaterThanEqualAndStatusAndBranchIdInOrderByStartDate(
			String activityType, Integer employeeId, DateTime startDate, String status, Integer[] branchIds);

	Page<ARActivitys> findByActivityObjectTypeAndActivityObjectTypeIdAndStatusAndBranchIdIn(String activityObjectType,
			Long activityObjectTypeId, String status, Integer[] branchIds, Pageable requestPage);

	@Query("SELECT DISTINCT entity FROM ARActivitys entity"
			+ " LEFT JOIN entity.taskAssigns taskAssigns" 
			+ " LEFT JOIN taskAssigns.employee employee" 
			+ " WHERE entity.activityType = ?1"
//			+ " AND ( entity.employee.id = ?2 OR entity.employeeGroup.id IN ?3 )" + " AND entity.startDate >= ?4"
			+ " AND ( taskAssigns.employee.id = ?2 OR taskAssigns.employeeGroup.id IN ?3 )" + " AND entity.startDate >= ?4"
			+ " AND entity.status = ?5" + " AND entity.branch.id IN ?6")
	Page<ARActivitys> findByActivityTypeAndEmployeeIdAndStartDateGreaterThanEqualAndStatus(String activityType,
			Integer employeeId, Integer[] groupId, DateTime startDate, String status, Integer[] branchIds, Pageable requestPage);

	@Query("SELECT DISTINCT entity FROM ARActivitys entity"
			+ " LEFT JOIN entity.taskAssigns taskAssigns" 
			+ " LEFT JOIN taskAssigns.employee employee" + " WHERE "
			+ "( entity.name LIKE ?1" + " OR entity.address LIKE ?1" + " OR employee.name LIKE ?1 )"
			+ " AND ('%' = ?2 OR entity.activityType = ?2)"
			+ " AND ('%' = ?3 OR entity.activityStatus = ?3)"
			+ " AND (entity.startDate BETWEEN ?4 AND ?5)"
			+ " AND entity.status = ?6" + " AND entity.branch.id IN ?7")
	Page<ARActivitys> findByActivityTypeLikeAndActivityStatusLikeAndStatus(String searchKey, String activityType,
			String activityStatus, DateTime fromDate, DateTime toDate, String status, Integer[] branchIds, Pageable requestPage);
}
