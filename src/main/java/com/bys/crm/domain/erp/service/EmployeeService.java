package com.bys.crm.domain.erp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bys.crm.domain.erp.constant.AAStatus;
import com.bys.crm.domain.erp.model.HREmployees;
import com.bys.crm.domain.erp.repository.HREmployeesRepository;


@Service
public class EmployeeService {
	
	@Autowired
	private HREmployeesRepository employeesRepository;
	
	public HREmployees getEmployee(Integer employeeId) {
		return employeesRepository.findByStatusAndId(AAStatus.Alive.name(), employeeId);
	}
}
