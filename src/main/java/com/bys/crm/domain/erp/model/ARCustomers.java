package com.bys.crm.domain.erp.model;

import java.math.BigDecimal;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;
import org.joda.time.DateTime;

import com.bys.crm.domain.DomainEntity;
import com.bys.crm.domain.erp.constant.AAStatus;
import com.bys.crm.util.DateTimeUtil;

@Entity
@Audited
@Table(name = "ARCustomers")
public class ARCustomers extends DomainEntity<Long> {
	@Id
	@Column(name = "ARCustomerID", columnDefinition = "int", nullable = false)
	private Long id;
	
	@Column(name = "ARCustomerName", nullable = false, columnDefinition = "nvarchar(4000)")
	private String name;

	//===New HBC ===
	@Column(name = "ARCustomerNewOldType ", columnDefinition = "varchar(50)")
	private String customerNewOldType;
	
	
	
	public String getCustomerNewOldType() {
		return customerNewOldType;
	}


	public void setCustomerNewOldType(String customerNewOldType) {
		this.customerNewOldType = customerNewOldType;
	}

	@Column(name = "ARCustomerCapacityProvideProdQuality ", columnDefinition = "varchar(255)")
	private String customerCapacityProvideProdQuality;
	
	@Column(name = "ARCustomerCapacitySupplyProgress ", columnDefinition = "varchar(255)")
	private String customerCapacitySupplyProgress;
	
	@Column(name = "ARCustomerCapacityFinancialFinishContract ", columnDefinition = "varchar(255)")
	private String customerCapacityFinancialFinishContract;
	
	@Column(name = "ARCustomerCapacityConstructionQuality ", columnDefinition = "varchar(255)")
	private String customerCapacityConstructionQuality;
	
	
	@Column(name = "ARCustomerCapacityConstructionManagemen ", columnDefinition = "varchar(255)")
	private String customerCapacityConstructionManagemen;
	
	@Column(name = "ARCustomerCapacityEnsureLaborSafety ", columnDefinition = "varchar(255)")
	private String customerCapacityEnsureLaborSafety;
	
	public String getCustomerCapacityProvideProdQuality() {
		return customerCapacityProvideProdQuality;
	}


	public void setCustomerCapacityProvideProdQuality(String customerCapacityProvideProdQuality) {
		this.customerCapacityProvideProdQuality = customerCapacityProvideProdQuality;
	}


	public String getCustomerCapacitySupplyProgress() {
		return customerCapacitySupplyProgress;
	}


	public void setCustomerCapacitySupplyProgress(String customerCapacitySupplyProgress) {
		this.customerCapacitySupplyProgress = customerCapacitySupplyProgress;
	}


	public String getCustomerCapacityFinancialFinishContract() {
		return customerCapacityFinancialFinishContract;
	}


	public void setCustomerCapacityFinancialFinishContract(String customerCapacityFinancialFinishContract) {
		this.customerCapacityFinancialFinishContract = customerCapacityFinancialFinishContract;
	}


	public String getCustomerCapacityConstructionQuality() {
		return customerCapacityConstructionQuality;
	}


	public void setCustomerCapacityConstructionQuality(String customerCapacityConstructionQuality) {
		this.customerCapacityConstructionQuality = customerCapacityConstructionQuality;
	}


	public String getCustomerCapacityConstructionManagemen() {
		return customerCapacityConstructionManagemen;
	}


	public void setCustomerCapacityConstructionManagemen(String customerCapacityConstructionManagemen) {
		this.customerCapacityConstructionManagemen = customerCapacityConstructionManagemen;
	}


	public String getCustomerCapacityEnsureLaborSafety() {
		return customerCapacityEnsureLaborSafety;
	}


	public void setCustomerCapacityEnsureLaborSafety(String customerCapacityEnsureLaborSafety) {
		this.customerCapacityEnsureLaborSafety = customerCapacityEnsureLaborSafety;
	}

	@Column(name = "ARCustomerIsEventInvitation",nullable=true)
	private Boolean  customerIsEventInvitation;
	 
	
	@Column(name = "ARCustomerRepresentativePosition", columnDefinition = "varchar(50)")
	private String customerRepresentativePosition;
	
	@Column(name = "ARCustomerDebtSales")
	private Double customerDebtSales;
	
	
	@Column(name = "ARCustomerDescription", columnDefinition = "varchar(4000)")
	private String customerDescription;
	
	public String getCustomerDescription() {
		return customerDescription;
	}


	public void setCustomerDescription(String customerDescription) {
		this.customerDescription = customerDescription;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "FK_ARActivityID")
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	private ARActivitys activity;
	
	public ARActivitys getActivity() {
		return activity;
	}


	public void setActivity(ARActivitys activity) {
		this.activity = activity;
	}


	public Boolean  getCustomerIsEventInvitation() {
		return customerIsEventInvitation;
	}

 
	public void setCustomerIsEventInvitation(Boolean  customerIsEventInvitation) {
		
		this.customerIsEventInvitation = customerIsEventInvitation;
	}

	 

	public String getCustomerRepresentativePosition() {
		return customerRepresentativePosition;
	}

	public void setCustomerRepresentativePosition(String customerRepresentativePosition) {
		this.customerRepresentativePosition = customerRepresentativePosition;
	}

	public Double getCustomerDebtSales() {
		return customerDebtSales;
	}

	public void setCustomerDebtSales(Double customerDebtSales) {
		this.customerDebtSales = customerDebtSales;
	}

	@Column(name = "ARCustomerCategories", columnDefinition = "varchar(50)")
	private String customerCategories;
	
	@Column(name = "ARCustomerGift",nullable=true)
	private Boolean  customerGift;
	
	@Column(name = "ARCustomerGiftType", columnDefinition = "varchar(50)")
	private String customerGiftType;
	
	 
	
	@Column(name = "ARCustomerLawRepresentative", columnDefinition = "varchar(50)")
	private String customerLawRepresentative;
	
	@Column(name = "ARCustomerParentCompany", columnDefinition = "varchar(50)")
	private String customerParentCompany;

	@Column(name = "ARCustomerPostCode", columnDefinition = "varchar(50)")
	private String customerPostCode;
	
	
	@Column(name = "ARCustomerIsIPO")
	private Boolean  customerIsIPO;
	
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	@Column(name = "ARCustomerWorkWithHBCFromDate")
	private DateTime customerWorkWithHBCFromDate;
	
	 
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "FK_HRDepartmentID")
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	private HRDepartments departments;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "FK_ARCustomerResourceID")
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	private ARCustomerResources customerResources;
	
	public ARCustomerResources getCustomerResources() {
		return customerResources;
	}

	public void setCustomerResources(ARCustomerResources customerResources) {
		this.customerResources = customerResources;
	}

	public Integer getCustomerAbilityToPay() {
		return customerAbilityToPay;
	}

	public void setCustomerAbilityToPay(Integer customerAbilityToPay) {
		this.customerAbilityToPay = customerAbilityToPay;
	}

	@Column(name = "ARCustomerTotalContracts")
	private Integer customerTotalContracts;
	
	@Column(name = "ARCustomerPaymentStatus", columnDefinition = "varchar(50)")
	private String customerPaymentStatus;
	
	@Column(name = "ARCustomerSalesLastYear")
	private Integer customerSalesLastYear;
	
	
	
	@Column(name = "ARCustomerAbilityToPay")
	private Integer customerAbilityToPay;
	
	
	@Column(name = "ARCustomerAbilityToPayQuickly")
	private Integer customerAbilityToPayQuickly;
	
	@Column(name = "ARCustomerInventoryTurnaround")
	private Integer customerInventoryTurnaround;
	
	@Column(name = "ARCustomerReceivableTurnaround")
	private Integer customerReceivableTurnaround;
	
	@Column(name = "ARCustomerDebtTotalRatio")
	private Double customerDebtTotalRatio;
	
	
	@Column(name = "ARCustomerROE")
	private Double customerROE;
	
	@Column(name = "ARCustomerSalesGrowthRate")
	private Double customerSalesGrowthRate;
	
	@Column(name = "ARCustomerProfitGrowthRate")
	private Double customerProfitGrowthRate;
	
	
	public String getCustomerCategories() {
		return customerCategories;
	}

	public void setCustomerCategories(String customerCategories) {
		this.customerCategories = customerCategories;
	}

	public Boolean  getCustomerGift() {
		return customerGift;
	}

	public void setCustomerGift(Boolean  customerGift) {
		this.customerGift = customerGift;
	}

	public String getCustomerGiftType() {
		return customerGiftType;
	}

	public void setCustomerGiftType(String customerGiftType) {
		this.customerGiftType = customerGiftType;
	}

 
	public String getCustomerLawRepresentative() {
		return customerLawRepresentative;
	}

	public void setCustomerLawRepresentative(String customerLawRepresentative) {
		this.customerLawRepresentative = customerLawRepresentative;
	}

	public String getCustomerParentCompany() {
		return customerParentCompany;
	}

	public void setCustomerParentCompany(String customerParentCompany) {
		this.customerParentCompany = customerParentCompany;
	}

	public String getCustomerPostCode() {
		return customerPostCode;
	}

	public void setCustomerPostCode(String customerPostCode) {
		this.customerPostCode = customerPostCode;
	}

	public Boolean  getCustomerIsIPO() {
		return customerIsIPO;
	}

	public void setCustomerIsIPO(Boolean  customerIsIPO) {
		this.customerIsIPO = customerIsIPO;
	}

	public DateTime getCustomerWorkWithHBCFromDate() {
		return customerWorkWithHBCFromDate;
	}

	public void setCustomerWorkWithHBCFromDate(DateTime customerWorkWithHBCFromDate) {
		this.customerWorkWithHBCFromDate = customerWorkWithHBCFromDate;
	}

	public HRDepartments getDepartments() {
		return departments;
	}

	public void setDepartments(HRDepartments departments) {
		this.departments = departments;
	}

	public Integer getCustomerTotalContracts() {
		return customerTotalContracts;
	}

	public void setCustomerTotalContracts(Integer customerTotalContracts) {
		this.customerTotalContracts = customerTotalContracts;
	}

	public String getCustomerPaymentStatus() {
		return customerPaymentStatus;
	}

	public void setCustomerPaymentStatus(String customerPaymentStatus) {
		this.customerPaymentStatus = customerPaymentStatus;
	}

	public Integer getCustomerSalesLastYear() {
		return customerSalesLastYear;
	}

	public void setCustomerSalesLastYear(Integer customerSalesLastYear) {
		this.customerSalesLastYear = customerSalesLastYear;
	}

	public Integer getCustomerAbilityToPayQuickly() {
		return customerAbilityToPayQuickly;
	}

	public void setCustomerAbilityToPayQuickly(Integer customerAbilityToPayQuickly) {
		this.customerAbilityToPayQuickly = customerAbilityToPayQuickly;
	}

	public Integer getCustomerInventoryTurnaround() {
		return customerInventoryTurnaround;
	}

	public void setCustomerInventoryTurnaround(Integer customerInventoryTurnaround) {
		this.customerInventoryTurnaround = customerInventoryTurnaround;
	}

	public Integer getCustomerReceivableTurnaround() {
		return customerReceivableTurnaround;
	}

	public void setCustomerReceivableTurnaround(Integer customerReceivableTurnaround) {
		this.customerReceivableTurnaround = customerReceivableTurnaround;
	}

	public Double getCustomerDebtTotalRatio() {
		return customerDebtTotalRatio;
	}

	public void setCustomerDebtTotalRatio(Double customerDebtTotalRatio) {
		this.customerDebtTotalRatio = customerDebtTotalRatio;
	}

	public Double getCustomerROE() {
		return customerROE;
	}

	public void setCustomerROE(Double customerROE) {
		this.customerROE = customerROE;
	}

	public Double getCustomerSalesGrowthRate() {
		return customerSalesGrowthRate;
	}

	public void setCustomerSalesGrowthRate(Double customerSalesGrowthRate) {
		this.customerSalesGrowthRate = customerSalesGrowthRate;
	}

	public Double getCustomerProfitGrowthRate() {
		return customerProfitGrowthRate;
	}

	public void setCustomerProfitGrowthRate(Double customerProfitGrowthRate) {
		this.customerProfitGrowthRate = customerProfitGrowthRate;
	}

	public Integer getCustomerTypeAccountConfigID() {
		return customerTypeAccountConfigID;
	}

	//=============================
	@Column(name = "ARCustomerOfficeAddress", nullable = false, columnDefinition = "nvarchar(50)")
	private String customerOfficeAddress;
	
	public String getCustomerOfficeAddress() {
		return customerOfficeAddress;
	}


	public void setCustomerOfficeAddress(String customerOfficeAddress) {
		this.customerOfficeAddress = customerOfficeAddress;
	}

	@Column(name = "ARCustomerNo", nullable = false, columnDefinition = "nvarchar(50)")
	private String customerNo;
	
	@Column(name = "ARCustomerActiveCheck")
	private boolean customerActiveCheck;

	@Column(name = "FK_ARCustomerTypeAccountConfigID")
	private Integer customerTypeAccountConfigID;
	

	public Integer getCustomertypeaccountconfigid() {
		return customerTypeAccountConfigID;
	}

	public void setCustomerTypeAccountConfigID(Integer customerTypeAccountConfigID) {
		this.customerTypeAccountConfigID = 0;
	}

	public boolean getCustomerActiveCheck() {
		return customerActiveCheck;
	}

	public void setCustomerActiveCheck(boolean customerActiveCheck) {
		this.customerActiveCheck = true;
	}

	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	@Column(name = "ARCustomerContactBirthday")
	private DateTime dob;

	@Column(name = "ARCustomerContactAddressLine1", columnDefinition = "nvarchar(2000)")
	private String address;

	@Column(name = "AAStatus", columnDefinition = "varchar(10)")
	private String status;

	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	@Column(name = "AACreatedDate")
	private DateTime createdDate;

	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	@Column(name = "AAUpdatedDate")
	private DateTime updatedDate;

	@Column(name = "AAUpdatedUser", columnDefinition = "nvarchar(50)")
	private String updatedUser;

	@Column(name = "AACreatedUser", columnDefinition = "nvarchar(50)")
	private String createdUser;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "FK_GELocationID")
	@NotAudited
	private GELocations location;

	@Column(name = "ARCustomerContactEmail1", columnDefinition = "nvarchar(1000)")
	private String email1;

	@Column(name = "ARCustomerTypeCombo", columnDefinition = "nvarchar(50)")
	private String customerType;

	@Column(name = "ARCustomerTaxNumber", columnDefinition = "varchar(50)")
	private String taxNumber;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "FK_BRBranchID")
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	private BRBranchs branch;

	@Column(name = "ARCustomerBusiness", columnDefinition = "nvarchar(2000)")
	private String business;

	@Column(name = "ARCustomerClassify", columnDefinition = "nvarchar(2000)")
	private String classify;

	@Column(name = "ARCustomerContactEmail2", columnDefinition = "nvarchar(2000)")
	private String email2;

	@Column(name = "ARCustomerContactFax", columnDefinition = "nvarchar(50)")
	private String fax;

	@Column(name = "ARCustomerRevenueDueYear", columnDefinition = "decimal(18, 5)")
	private BigDecimal revenueDueYear;

	@Column(name = "ARCustomerStockCode", columnDefinition = "varchar(500)")
	private String stockCode;

	@Column(name = "ARCustomerContactPhone", columnDefinition = "nvarchar(50)")
	private String tel1;

	@Column(name = "ARCustomerContactCellPhone", columnDefinition = "nvarchar(50)")
	private String tel2;

	@Column(name = "ARCustomerWebsite", columnDefinition = "nvarchar(2000)")
	private String website;

	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "FK_HREmployeeID")
	private HREmployees employee;

	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "FK_HRGroupID", columnDefinition = "int")
	private HRGroups employeeGroup;

	 @Column(name = "ARCustomerGroup", columnDefinition = "nvarchar(500)")
	 private String group;

	@Column(name = "REV", insertable = false, updatable = false)
	private Long rev;

	@Column(name = "REVTYPE", insertable = false, updatable = false)
	private Long revType;

	@Column(name = "FK_HREmployeeID", insertable = false, updatable = false)
	private Integer employeeId;

	@Column(name = "FK_HRGroupID", insertable = false, updatable = false)
	private Integer employeeGroupId;

	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	@Column(name = "ARCustomerCompanyEstablishmentDay", columnDefinition = "datetime")
	private DateTime companyEstablishmentDay;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "customer", cascade = CascadeType.ALL)
	private Set<ARCustomerContactGroups> customerContactGroups;

	@Column(name = "CreatedUserID", columnDefinition = "int")
	private Integer createdUserId;

	@Column(name = "UpdatedUserID", columnDefinition = "int")
	private Integer updatedUserId;

	// @Column(name = "ARCustomerChangedUser", columnDefinition = "nvarchar(50)")
	// private String changedUser;

//	@Column(name = "ARCustomerGroupCombo")
//	@NotAudited
//	private String customerGroup;

	public ARCustomers() {
		this.status = AAStatus.Alive.name();
		this.customerActiveCheck = true;
		this.customerTypeAccountConfigID = 0;
	}

	public DateTime getCreatedDateChartKeyByDay(){
		return DateTimeUtil.toDateTimeAtStartOfDay(this.getCreatedDate().getMillis());
	}
	
	public DateTime getCreatedDateChartKeyByMonth(){
		return DateTimeUtil.toDateTimeAtStartOfDay(this.getCreatedDate().getMillis()).withDayOfMonth(1);
	}
	
	public DateTime getCreatedDateChartKeyByYear(){
		return DateTimeUtil.toDateTimeAtStartOfDay(this.getCreatedDate().getMillis()).withDayOfMonth(1).withMonthOfYear(1);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public DateTime getDob() {
		return dob;
	}

	public String getCustomerNo() {
		return customerNo;
	}

	public void setCustomerNo(String customerno) {
		this.customerNo = customerno;
	}

	public void setDob(DateTime dob) {
		this.dob = dob;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public DateTime getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(DateTime createdDate) {
		this.createdDate = createdDate;
	}

	public DateTime getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(DateTime updatedDate) {
		this.updatedDate = updatedDate;
	}

	public GELocations getLocation() {
		return location;
	}

	public void setLocation(GELocations location) {
		this.location = location;
	}

	public String getEmail1() {
		return email1;
	}

	public void setEmail1(String email1) {
		this.email1 = email1;
	}

	public String getCustomerType() {
		return customerType;
	}

	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}

	public String getTaxNumber() {
		return taxNumber;
	}

	public void setTaxNumber(String taxNumber) {
		this.taxNumber = taxNumber;
	}

	public BRBranchs getBranch() {
		return branch;
	}

	public void setBranch(BRBranchs branch) {
		this.branch = branch;
	}

	public String getBusiness() {
		return business;
	}

	public void setBusiness(String business) {
		this.business = business;
	}

	public String getClassify() {
		return classify;
	}

	public void setClassify(String classify) {
		this.classify = classify;
	}

	public String getEmail2() {
		return email2;
	}

	public void setEmail2(String email2) {
		this.email2 = email2;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public BigDecimal getRevenueDueYear() {
		return revenueDueYear;
	}

	public void setRevenueDueYear(BigDecimal revenueDueYear) {
		this.revenueDueYear = revenueDueYear;
	}

	public String getStockCode() {
		return stockCode;
	}

	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}

	public String getTel1() {
		return tel1;
	}

	public void setTel1(String tel1) {
		this.tel1 = tel1;
	}

	public String getTel2() {
		return tel2;
	}

	public void setTel2(String tel2) {
		this.tel2 = tel2;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public HREmployees getEmployee() {
		return employee;
	}

	public void setEmployee(HREmployees employee) {
		this.employee = employee;
	}

	public HRGroups getEmployeeGroup() {
		return employeeGroup;
	}

	public void setEmployeeGroup(HRGroups employeeGroup) {
		this.employeeGroup = employeeGroup;
	}

	public Long getRev() {
		return rev;
	}

	public void setRev(Long rev) {
		this.rev = rev;
	}

	public Long getRevType() {
		return revType;
	}

	public void setRevType(Long revType) {
		this.revType = revType;
	}

	public Integer getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}

	public Integer getEmployeeGroupId() {
		return employeeGroupId;
	}

	public void setEmployeeGroupId(Integer employeeGroupId) {
		this.employeeGroupId = employeeGroupId;
	}

	public DateTime getCompanyEstablishmentDay() {
		return companyEstablishmentDay;
	}

	public void setCompanyEstablishmentDay(DateTime companyEstablishmentDay) {
		this.companyEstablishmentDay = companyEstablishmentDay;
	}

	public Set<ARCustomerContactGroups> getCustomerContactGroups() {
		return customerContactGroups;
	}

	public void setCustomerContactGroups(Set<ARCustomerContactGroups> customerContactGroups) {
		this.customerContactGroups = customerContactGroups;
	}

	public Integer getCreatedUserId() {
		return createdUserId;
	}

	public void setCreatedUserId(Integer createdUserId) {
		this.createdUserId = createdUserId;
	}

	public Integer getUpdatedUserId() {
		return updatedUserId;
	}

	public void setUpdatedUserId(Integer updatedUserId) {
		this.updatedUserId = updatedUserId;
	}

//	public String getCustomerGroup() {
//		return customerGroup;
//	}
//
//	public void setCustomerGroup(String customerGroup) {
//		this.customerGroup = customerGroup;
//	}

	public String getUpdatedUser() {
		return updatedUser;
	}

	public void setUpdatedUser(String updatedUser) {
		this.updatedUser = updatedUser;
	}

	public String getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(String createdUser) {
		this.createdUser = createdUser;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

}
