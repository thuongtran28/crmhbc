package com.bys.crm.domain.erp.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Type;
import org.hibernate.envers.NotAudited;
import org.joda.time.DateTime;

import com.bys.crm.app.dto.ColumnAliasDto;
import com.bys.crm.app.dto.UserFieldConfigsDto;

@Entity
@Table(name = "STUserFieldConfigs")
public class STUserFieldConfigs {
	@Id
	@Column(name = "STUserFieldConfigID", columnDefinition = "int")
	private Long id;
	
	@Column(name = "AAStatus")
	private String status;
	
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	@Column(name = "AACreatedDate")
	private DateTime createdDate;
	
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	@Column(name = "AAUpdatedDate")
	private DateTime updatedDate;
	
	@Column(name = "AACreatedUser")
	private String createdUser;
	
	@Column(name = "AAUpdatedUser")
	private String updatedUser;
	
 
 	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "FK_ADUserID")
	@NotAudited
	private ADUsers user;
	
	@Column(name = "STUserFieldConfigValue" , columnDefinition = "varchar(MAX)")
	private String userFieldConfigValue;
	
	@Column(name = "STUserFieldConfigType")
	private String userFieldConfigType;
	
	@Transient
	private List<ColumnAliasDto> columnAlias;
 
	public List<ColumnAliasDto> getColumnAlias() {
		return columnAlias;
	}

	public void setColumnAlias(List<ColumnAliasDto> columnAlias) {
		this.columnAlias = columnAlias;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public DateTime getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(DateTime createdDate) {
		this.createdDate = createdDate;
	}

	public DateTime getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(DateTime modifiedDate) {
		this.updatedDate = modifiedDate;
	}

	public String getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(String aACreatedUser) {
		createdUser = aACreatedUser;
	}

	public String getUpdatedUser() {
		return updatedUser;
	}

	public void setUpdatedUser(String aAUpdatedUser) {
		updatedUser = aAUpdatedUser;
	}

 
	 

	public ADUsers getUser() {
		return user;
	}

	public void setUser(ADUsers user) {
		this.user = user;
	}

	public String getUserFieldConfigValue() {
		return userFieldConfigValue;
	}

	public void setUserFieldConfigValue(String userFieldConfigValue) {
		this.userFieldConfigValue = userFieldConfigValue;
	}

	public String getUserFieldConfigType() {
		return userFieldConfigType;
	}

	public void setUserFieldConfigType(String userFieldConfigType) {
		this.userFieldConfigType = userFieldConfigType;
	}
	
	
	
}
