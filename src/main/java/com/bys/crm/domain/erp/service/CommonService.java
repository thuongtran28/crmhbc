package com.bys.crm.domain.erp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bys.crm.domain.erp.constant.AAStatus;
import com.bys.crm.domain.erp.constant.ADConfigKeyGroup;
import com.bys.crm.domain.erp.model.ADConfigValues;
import com.bys.crm.domain.erp.model.ARCustomerContacts;
import com.bys.crm.domain.erp.model.ARCustomers;
import com.bys.crm.domain.erp.model.ARProspectCustomers;
import com.bys.crm.domain.erp.repository.ADConfigValuesRepository;
import com.bys.crm.domain.erp.repository.ARCustomerContactRepository;
import com.bys.crm.domain.erp.repository.ARCustomersRepository;
import com.bys.crm.domain.erp.repository.ARProspectCustomersRepository;

@Service
public class CommonService {

	@Autowired
	private ARCustomersRepository customersRepository;

	@Autowired
	private ARCustomerContactRepository contactRepository;
	@Autowired
	private ADConfigValuesRepository configValuesRepository; 

	@Autowired
	private ARProspectCustomersRepository prospectRepository;
	
	public Boolean isExistPhoneNumberAndName(Integer branchId, String phone, String customerName) {
		
		List<ARProspectCustomers> prospectList = prospectRepository.findByPhone(phone, AAStatus.Alive.name(), branchId);

		List<ARCustomers> customerList = customersRepository.findByPhone(phone, AAStatus.Alive.name(), branchId);

		List<ARCustomerContacts> contactList = contactRepository.findByPhone(phone, AAStatus.Alive.name(), branchId);

		return !(prospectList.isEmpty() && customerList.isEmpty() && contactList.isEmpty());
	}
	
	public Boolean isExistPhoneNumberAndNameCustomer(Integer branchId, String phone, String customerName) {
		
		//List<ARProspectCustomers> prospectList = prospectRepository.findByPhone(phone, AAStatus.Alive.name(), branchId);

		List<ARCustomers> customerList = customersRepository.findByPhoneAndName(phone, AAStatus.Alive.name(), customerName, branchId);

		//List<ARCustomerContacts> contactList = contactRepository.findByPhone(phone, AAStatus.Alive.name(), branchId);

		return !(customerList.isEmpty());
	}
	// Check exist phone number
	public Boolean isExistPhoneNumber(Integer branchId, String phone) {
		
		List<ARProspectCustomers> prospectList = prospectRepository.findByPhone(phone, AAStatus.Alive.name(), branchId);

		List<ARCustomers> customerList = customersRepository.findByPhone(phone, AAStatus.Alive.name(), branchId);

		List<ARCustomerContacts> contactList = contactRepository.findByPhone(phone, AAStatus.Alive.name(), branchId);

		return !(prospectList.isEmpty() && customerList.isEmpty() && contactList.isEmpty());
	}
	
public Boolean isExistOnlyPhoneNumberContact(Integer branchId, String phone) {
	 

		List<ARCustomerContacts> contactList = contactRepository.findByPhone(phone, AAStatus.Alive.name(), branchId);

		return !(contactList.isEmpty());
	}
	public Boolean isExistsContactRelationHBC(String listType, String value) {
		List<ADConfigValues> entitie = configValuesRepository.findByStatusAndGroupAndActiveAndValue(AAStatus.Alive.name(),
				ADConfigKeyGroup.fromValue(listType).name(), true, value);
		return !(entitie.isEmpty());
	}
}
