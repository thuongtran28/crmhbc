package com.bys.crm.domain.erp.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

import com.bys.crm.domain.DomainEntity;

@Entity
@Audited
@Table(name = "ARCustomerActivityGroups")
public class ARCustomerActivityGroups extends DomainEntity<Long> {
	@Id
	@Column(name = "ARCustomerActivityGroupID", nullable = false, columnDefinition = "int")
	private Long id;

	@Column(name = "AAStatus", columnDefinition = "varchar(10)")
	private String status;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "FK_ARActivityID", columnDefinition = "int")
	private ARActivitys activitys;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "[FK_ARCustomerID", columnDefinition = "int")
	private ARCustomers customers;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "FK_BRBranchID")
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	private BRBranchs branch;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ARActivityType")
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	private ARActivitys activityTyple;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public ARActivitys getActivitys() {
		return activitys;
	}

	public void setActivitys(ARActivitys activitys) {
		this.activitys = activitys;
	}

	public ARCustomers getCustomers() {
		return customers;
	}

	public void setCustomers(ARCustomers customers) {
		this.customers = customers;
	}

	public BRBranchs getBranch() {
		return branch;
	}

	public void setBranch(BRBranchs branch) {
		this.branch = branch;
	}

	public ARActivitys getActivityTyple() {
		return activityTyple;
	}

	public void setActivityTyple(ARActivitys activityTyple) {
		this.activityTyple = activityTyple;
	}
}
