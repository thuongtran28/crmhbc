package com.bys.crm.domain.erp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.bys.crm.domain.erp.model.ADUserPermissions;

@Repository
public interface ADUserPermissionRepository extends JpaRepository<ADUserPermissions, Long>, BaseRepository{

	@Override
	@Query("SELECT max(entity.id) FROM ADUserPermissions entity")
	Long findMaxId();
	
	List<ADUserPermissions> findByStatus(String status);
	
	List<ADUserPermissions> findByStatusAndUserId(String status, Long userId);
	
	List<ADUserPermissions> findByStatusAndUserIdIn(String status, List<Long>  userIds);
}
