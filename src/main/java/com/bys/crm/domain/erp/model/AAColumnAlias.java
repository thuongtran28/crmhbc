package com.bys.crm.domain.erp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.bys.crm.domain.DomainEntity;

@Entity
@Table(name = "AAColumnAlias")
public class AAColumnAlias extends DomainEntity<Long> {
	@Id
	@Column(name = "AAColumnAliasID")
	private Long id;
	
	@Column(name = "AAStatus")
	private String status;
	
	@Column(name = "AANumberInt")
	private Integer numberInt;
	
	@Column(name = "AANumberString")
	private String numberString;
	
	@Column(name = "AAColumnAliasName")
	private String columnAliasName;
	
	@Column(name = "AAColumnAliasCaption")
	private String columnAliasCaption;
	
	@Column(name = "AATableName")
	private String tableName;
	
	@Column(name = "AAIsHidden")
	private boolean isHidden;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getNumberInt() {
		return numberInt;
	}

	public void setNumberInt(Integer numberInt) {
		this.numberInt = numberInt;
	}

	public String getNumberString() {
		return numberString;
	}

	public void setNumberString(String numberString) {
		this.numberString = numberString;
	}

	public String getColumnAliasName() {
		return columnAliasName;
	}

	public void setColumnAliasName(String columnAliasName) {
		this.columnAliasName = columnAliasName;
	}

	public String getColumnAliasCaption() {
		return columnAliasCaption;
	}

	public void setColumnAliasCaption(String columnAliasCaption) {
		this.columnAliasCaption = columnAliasCaption;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public boolean getHidden() {
		return isHidden;
	}

	public void setHidden(boolean isHidden) {
		this.isHidden = isHidden;
	}
	
}
