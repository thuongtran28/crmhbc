package com.bys.crm.domain.erp.repository;

import java.math.BigDecimal;
import java.util.List;

import org.joda.time.DateTime;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.bys.crm.domain.erp.model.ARBidOpportunitys;
 
 

@Repository
public interface ARBidOpportunitysRepository extends JpaRepository<ARBidOpportunitys, Long>, BaseRepository {
	@Query("SELECT max(entity.id) FROM ARBidOpportunitys entity")
	Long findMaxId();
	
	ARBidOpportunitys findByIdAndStatus(Long id, String status);
	
	@Query("SELECT DISTINCT opportunity FROM ARBidOpportunitys opportunity"
    		+ " LEFT JOIN opportunity.chairEmployee chairEmployee" 
			+ " WHERE (opportunity.name LIKE ?1"
			+ " OR opportunity.projectName LIKE ?2  OR chairEmployee.name LIKE ?3 OR opportunity.opportunityHBCRole LIKE ?4)"
			+ " AND opportunity.status = ?5")
//			+ " ORDER BY opportunity.?6")
	List<ARBidOpportunitys> findByNameLikeOrCustomerNameLikeOrCustomerContactNameLike(String name, String projectName,
			  String chairEmployeeName, String roleName,  String status);
	
//	@Query("SELECT DISTINCT opportunity FROM ARBidOpportunitys opportunity"
//			+ " LEFT JOIN opportunity.opportunityInfo opportunityInfo" 
//			+ " LEFT JOIN opportunity.customer customer" 
//			+ " LEFT JOIN opportunity.employee employee"
//			+ " LEFT JOIN opportunity.bidlocation bidlocation"
//			+ " LEFT JOIN opportunity.chairEmployee chairEmployee" 
//			+ " WHERE(opportunity.name LIKE ?1 OR opportunity.projectName LIKE ?1"
//			+ " OR opportunity.opportunityNo LIKE ?1"
//			+ " OR customer.name LIKE ?1"
//			+ " OR bidlocation.name LIKE ?1"
//			+ " OR chairEmployee.name LIKE ?1 )"
//			+ " AND ('%' = ?2 OR opportunity.customerType = ?2)" 
//			+ " AND ('%' = ?3 OR opportunity.opportunityStage = ?3)"
//		     + " AND (0 = ?4 OR opportunity.chairEmployee.id = ?4)"
//	//		+ " AND (opportunity.createdDate BETWEEN ?5 AND ?6)"
//			+ " AND (opportunity.opportunityInfo.amount BETWEEN ?5 AND ?6)"
//			+ " AND opportunity.status = ?7 AND opportunityInfo.status")
////			+ " ORDER BY ?7")
//	List<ARBidOpportunitys> findByClassifyLikeAndStepLikeAndStatus(String searchKey, String customerType, String opportunityStage, Integer chairEmployeeId,
//			BigDecimal fromAmount,BigDecimal toAmount, 
//			    String status);
	
	@Query("SELECT  opportunity FROM ARBidOpportunitys opportunity"
			+ " LEFT JOIN opportunity.opportunityInfo opportunityInfo" 
			+ " LEFT JOIN opportunity.customer customer" 
			+ " LEFT JOIN opportunity.employee employee"
			+ " LEFT JOIN opportunity.bidlocation bidlocation"
			+ " LEFT JOIN opportunity.chairEmployee chairEmployee" 
			+ " WHERE(opportunity.name LIKE ?1 OR opportunity.projectName LIKE ?1"
			+ " OR opportunity.opportunityNo LIKE ?1"
			+ " OR customer.name LIKE ?1"
			+ " OR bidlocation.name LIKE ?1"
			+ " OR chairEmployee.name LIKE ?1 )"
			+ " AND ('%' = ?2 OR opportunity.customerType = ?2)" 
			+ " AND ('%' = ?3 OR opportunity.opportunityStage = ?3)"
		     + " AND (0 = ?4 OR opportunity.chairEmployee.id = ?4)"
	//		+ " AND (opportunity.createdDate BETWEEN ?5 AND ?6)"
			+ " AND (opportunity.opportunityInfo.amount BETWEEN ?5 AND ?6)"
			+ " AND opportunity.status = ?7 AND opportunityInfo.status = ?7")
//			+ " ORDER BY ?7")
	Page<ARBidOpportunitys> findByClassifyLikeAndStepLikeAndStatus(String searchKey, String customerType, String opportunityStage, Integer chairEmployeeId,
			BigDecimal fromAmount,BigDecimal toAmount, 
			    String status,Pageable requestPage);

}
