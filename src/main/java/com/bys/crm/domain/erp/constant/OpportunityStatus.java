package com.bys.crm.domain.erp.constant;

 

public enum OpportunityStatus {
	CanLapDeNghiDuThau("CanLapDeNghiDuThau"),
	HSDT("HSDT"),
	CanBoSungHSMT("CanBoSungHSMT") ;
	
	private String dBCode;

	private OpportunityStatus(String dBCode) {
		this.dBCode = dBCode;
	}

	public String value() {
		return this.dBCode;
	}

	public static OpportunityStatus fromValue(String value) {
		for (OpportunityStatus status : OpportunityStatus.values()) {
			if (status.value().equalsIgnoreCase(value)) {
				return status;
			}
		}

		return null;
	}
}