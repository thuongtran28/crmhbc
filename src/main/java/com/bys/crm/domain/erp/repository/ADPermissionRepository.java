package com.bys.crm.domain.erp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.bys.crm.domain.erp.model.ADPermissions;

@Repository
public interface ADPermissionRepository extends JpaRepository<ADPermissions, Long>, BaseRepository{

	@Override
	@Query("SELECT max(entity.id) FROM ADPermissions entity")
	Long findMaxId();
	
	List<ADPermissions> findByStatusAndType(String status, String type);
}
