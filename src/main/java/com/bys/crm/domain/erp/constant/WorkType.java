package com.bys.crm.domain.erp.constant;

public enum WorkType {
	Quotation("Báo giá"),
	SaleOrder("Đơn bán hàng/Hợp đồng"),
	CustomerCare("Chăm sóc khách hàng"),
	FBMarket("Ghi nhận phản hồi thị trường"),
	FBProduct("Ghi nhận phản hồi sản phẩm"),
	FBService("Ghi nhận phản hồi dịch vụ"),
	CustomerAccess("Tiếp cận khách hàng"),
	OtherType("Khác");

	private String dBCode;

	private WorkType(String dBCode) {
		this.dBCode = dBCode;
	}

	public String value() {
		return this.dBCode;
	}

	public static WorkType fromValue(String value) {
		for (WorkType status : WorkType.values()) {
			if (status.value().equalsIgnoreCase(value)) {
				return status;
			}
		}

		return null;
	}

	public static WorkType fromName(String name) {
		for (WorkType status : WorkType.values()) {
			if (status.name().equalsIgnoreCase(name)) {
				return status;
			}
		}

		return null;
	}

	public static String supportValues() {
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < values().length; i++) {
			String delimeter = (i == 0)?"":",";
			builder.append(delimeter).append(values()[i].name());
		}
		return builder.toString();
	}
}
