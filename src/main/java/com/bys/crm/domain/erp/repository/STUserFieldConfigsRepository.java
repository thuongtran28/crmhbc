package com.bys.crm.domain.erp.repository;

 

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

 
import com.bys.crm.domain.erp.model.STUserFieldConfigs;

@Repository
public interface STUserFieldConfigsRepository extends JpaRepository< STUserFieldConfigs, Long> , BaseRepository{
	
	@Query("SELECT max(entity.id) FROM STUserFieldConfigs entity")
	Long findMaxId();
	
	@Query("SELECT entity FROM STUserFieldConfigs entity"
			+ " WHERE  entity.status = ?1 AND entity.userFieldConfigType = ?2"
			+ " AND entity.user.id = ?3")
	STUserFieldConfigs findByStatusAndUserFieldConfigTypeAndUserId(String status, String userFieldConfigType, Long userId);
	
}
