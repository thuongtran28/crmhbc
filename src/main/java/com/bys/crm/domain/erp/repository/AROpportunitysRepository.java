package com.bys.crm.domain.erp.repository;

import java.util.List;

import org.joda.time.DateTime;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.bys.crm.domain.erp.model.AROpportunitys;

@Repository
public interface AROpportunitysRepository extends JpaRepository<AROpportunitys, Long>, BaseRepository {
	@Query("SELECT max(entity.id) FROM AROpportunitys entity")
	Long findMaxId();

	AROpportunitys findByIdAndStatus(Long id, String status);

	List<AROpportunitys> findByStatusAndBranchIdIn(String status, Integer[] branchIds, Sort sort);
	
	List<AROpportunitys> findByStatusAndBranchIdInAndCreatedDateBetweenOrderByCreatedDate(String status, Integer[] branchIds, DateTime startDate, DateTime endDate);

	Page<AROpportunitys> findByStatusAndBranchIdIn(String status, Integer[] branchIds, Pageable pageRequest);

	@Query("SELECT DISTINCT opportunity FROM AROpportunitys opportunity"
//			+ " LEFT JOIN opportunity.customerContact contact"
			+ " LEFT JOIN opportunity.opportunityContactGroups opportunityContactGroups"
			+ " LEFT JOIN opportunityContactGroups.contact customerContacts"
			+ " LEFT JOIN opportunity.customer customer" 
			+ " LEFT JOIN opportunity.taskAssigns taskAssigns" 
			+ " LEFT JOIN taskAssigns.employee employee" 
//			+ " LEFT JOIN opportunity.employee employee"
			+ " WHERE (opportunity.name LIKE ?1 OR" + " customer.name LIKE ?2"
//			+ " OR ( contact.firstName + ' ' + contact.lastName) LIKE ?3" 
			+ " OR ( customerContacts.lastName + ' ' + customerContacts.firstName) LIKE ?3"
			+ " OR employee.name LIKE ?4 )"
			+ " AND opportunity.status = ?5" + " AND opportunity.branch.id IN ?6")
//			+ " ORDER BY opportunity.?6")
	List<AROpportunitys> findByNameLikeOrCustomerNameLikeOrCustomerContactNameLike(String name, String customerName,
			String contactName, String assignedTo, String status, Integer[] branchIds);

	List<AROpportunitys> findByNameLikeAndStatusAndBranchIdInOrderByNameAsc(String name, String status, Integer[] branchIds);

	@Query("SELECT DISTINCT opportunity FROM AROpportunitys opportunity"
//			+ " LEFT JOIN opportunity.customerContact contact"
			+ " LEFT JOIN opportunity.opportunityContactGroups opportunityContactGroups"
			+ " LEFT JOIN opportunityContactGroups.contact customerContacts"
			+ " LEFT JOIN opportunity.customer customer" 
			+ " LEFT JOIN opportunity.taskAssigns taskAssigns" 
			+ " LEFT JOIN taskAssigns.employee employee" 
//			+ " LEFT JOIN opportunity.employee employee"
			+ " WHERE (opportunity.name LIKE ?1 OR" + " customer.name LIKE ?1"
//			+ " OR ( contact.firstName + ' ' + contact.lastName) LIKE ?1"
			+ " OR ( customerContacts.lastName + ' ' + customerContacts.firstName) LIKE ?1"
			+ " OR employee.name LIKE ?1 )"
			+ " AND ('%' = ?2 OR opportunity.classify = ?2)" + " AND ('%' = ?3 OR opportunity.step = ?3)"
			+ " AND (opportunity.createdDate BETWEEN ?4 AND ?5)"
			+ " AND opportunity.status = ?6" + " AND opportunity.branch.id IN ?7")
//			+ " ORDER BY ?7")
	List<AROpportunitys> findByClassifyLikeAndStepLikeAndStatus(String searchKey, String classify, String step,
			DateTime fromDate, DateTime toDate, String status, Integer[] branchIds);

	// Get opportunity list by customer id
	Page<AROpportunitys> findByCustomerIdAndStatusAndBranchIdIn(Long customerId,
			String status, Integer[] branchIds, Pageable requestPage);

//	// Get opportunity list by customer contact id
//	Page<AROpportunitys> findByCustomerContactIdAndStatus(Long contactId,
//			String status, Pageable requestPage);
}
