package com.bys.crm.domain.erp.service;

import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bys.crm.domain.erp.constant.AAStatus;
import com.bys.crm.domain.erp.constant.GELocationType;
import com.bys.crm.domain.erp.model.GELocations;
import com.bys.crm.domain.erp.repository.GELocationsRepository;

@Service
public class GELocationsService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(GELocationsService.class);
	
	@Autowired
	private GELocationsRepository repository;

//	@Cacheable("provinceList")
	public List<GELocations> getProvinceList() {
		LOGGER.info("There's no caching yet..getting data from DB..");
		return repository.findByTypeOrderByName(GELocationType.PROVINCE.value()).stream()
				.filter(item -> AAStatus.Alive == item.getStatus()).collect(Collectors.toList());
	}

	public List<GELocations> getDistrictList() {
		return repository.findByTypeOrderByName(GELocationType.DISTRICT.value()).stream()
				.filter(item -> AAStatus.Alive == item.getStatus()).collect(Collectors.toList());
	}
}