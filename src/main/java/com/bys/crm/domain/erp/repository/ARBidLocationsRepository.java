package com.bys.crm.domain.erp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.bys.crm.domain.erp.model.ARBidLocations;
 

@Repository
public interface ARBidLocationsRepository extends JpaRepository< ARBidLocations, Long>, BaseRepository {
	@Query("SELECT max(entity.id) FROM ARBidLocations entity")
	Long findMaxId();
	List<ARBidLocations> findByStatus(String status);
}
