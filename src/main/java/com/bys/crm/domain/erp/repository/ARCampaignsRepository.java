package com.bys.crm.domain.erp.repository;

import java.util.List;

import org.joda.time.DateTime;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.bys.crm.domain.erp.model.ARCampaigns;

@Repository
public interface ARCampaignsRepository extends JpaRepository<ARCampaigns, Long>, BaseRepository {
	@Query("SELECT max(entity.id) FROM ARCampaigns entity")
	Long findMaxId();

	ARCampaigns findByIdAndStatus(Long id, String status);

	List<ARCampaigns> findByStatusAndBranchIdIn(String status, Integer[] branchIds, Sort sort);

	List<ARCampaigns> findByStatusAndBranchIdInAndCreatedDateBetweenOrderByCreatedDate(String status, Integer[] branchIds, DateTime startDate, DateTime endDate);
	
	Page<ARCampaigns> findByStatusAndBranchIdIn(String status, Integer[] branchIds, Pageable pageRequest);

	@Query("SELECT DISTINCT entity FROM ARCampaigns entity" 
			+ " LEFT JOIN entity.taskAssigns taskAssigns" 
			+ " LEFT JOIN taskAssigns.employee employee" 
//			+ " LEFT JOIN entity.employee employee" 
			+ " WHERE "
			+ "( entity.name LIKE ?1" + " OR employee.name LIKE ?2 )"
			+ " AND entity.status = ?3" + " AND entity.branch.id IN ?4")
	Page<ARCampaigns> findByNameLikeOrEmployeeNameLike(String name, String assignedTo, String status,
			Integer[] branchIds, Pageable requestPage);

	List<ARCampaigns> findByNameLikeAndStatusAndBranchIdInOrderByNameAsc(String name, String status, Integer[] branchIds);

	@Query("SELECT DISTINCT entity FROM ARCampaigns entity" 
			+ " LEFT JOIN entity.taskAssigns taskAssigns" 
			+ " LEFT JOIN taskAssigns.employee employee" 
//			+ " LEFT JOIN entity.employee employee" 
			+ " WHERE "
			+ "( entity.name LIKE ?1" + " OR employee.name LIKE ?1 )"
			+ " AND ('%' = ?2 OR entity.type = ?2)"
			+ " AND ('%' = ?3 OR entity.campaignStatus = ?3)"
			+ " AND (entity.startDate BETWEEN ?4 AND ?5)"
			+ " AND entity.status = ?6" + " AND entity.branch.id IN ?7")
	Page<ARCampaigns> findByTypeLikeAndCampaignStatusLikeAndStatus(String searchKey, String type,
			String campaignStatus, DateTime fromDate, DateTime toDate, String status, Integer[] branchIds, Pageable requestPage);
}
