package com.bys.crm.domain.erp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.bys.crm.domain.erp.model.ARBidConstructionCategories;
 

@Repository
public interface ARBidConstructionCategoriesRepository  extends JpaRepository< ARBidConstructionCategories, Long>, BaseRepository {
	@Query("SELECT max(entity.id) FROM ARBidConstructionCategories entity")
	Long findMaxId();
	List<ARBidConstructionCategories> findByStatus(String status);
}
