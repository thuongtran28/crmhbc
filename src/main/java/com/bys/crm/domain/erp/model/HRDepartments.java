package com.bys.crm.domain.erp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.bys.crm.domain.DomainEntity;

@Entity
@Table(name = "HRDepartments")
public class HRDepartments extends DomainEntity<Long>{
	
	@Id
	@Column(name = "HRDepartmentID",  columnDefinition = "int", nullable = false)
	private Long id;

	@Column(name = "AAStatus")
	private String status;
	
	@Column(name = "HRDepartmentNo", nullable = false, columnDefinition = "varchar(50)")
	private String departmentNo;
	
	@Column(name = "HRDepartmentName", columnDefinition = "varchar(50)")
	private String name;
	
	@Column(name = "HRDepartmentDesc", columnDefinition = "varchar(50)")
	private String departmentDesc;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "FK_BRBranchID")
	private BRBranchs branch;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDepartmentNo() {
		return departmentNo;
	}

	public void setDepartmentNo(String departmentNo) {
		this.departmentNo = departmentNo;
	}

	public String getName() {
		return name;
	}

	public void setName(String departmentName) {
		this.name = departmentName;
	}

	public String getDepartmentDesc() {
		return departmentDesc;
	}

	public void setDepartmentDesc(String departmentDesc) {
		this.departmentDesc = departmentDesc;
	}

	public BRBranchs getBranch() {
		return branch;
	}

	public void setBranch(BRBranchs branch) {
		this.branch = branch;
	}
	
}
