package com.bys.crm.domain.erp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.bys.crm.domain.erp.model.ARBidConstructionTypes;
 

@Repository
public interface ARBidConstructionTypesRepository  extends JpaRepository< ARBidConstructionTypes, Long>, BaseRepository {
	@Query("SELECT max(entity.id) FROM ARBidConstructionTypes entity")
	Long findMaxId();
	List<ARBidConstructionTypes> findByStatus(String status);
}
