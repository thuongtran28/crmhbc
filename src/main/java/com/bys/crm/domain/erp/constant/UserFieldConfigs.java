package com.bys.crm.domain.erp.constant;

public enum UserFieldConfigs {
	ARCustomers("ARCustomers"),
	ARCampaigns("ARCampaigns"),
	ARActivitys("ARActivitys"),
	ARCustomerContacts("ARCustomerContacts"),
	AROpportunitys("AROpportunitys");
	
	private String dBCode;

	private UserFieldConfigs(String dBCode) {
		this.dBCode = dBCode;
	}

	public String value() {
		return this.dBCode;
	}

	public static UserFieldConfigs fromValue(String value) {
		for (UserFieldConfigs status : UserFieldConfigs.values()) {
			if (status.value().equalsIgnoreCase(value)) {
				return status;
			}
		}

		return null;
	}
	public static UserFieldConfigs fromName(String name) {
		for (UserFieldConfigs status : UserFieldConfigs.values()) {
			if (status.name().equalsIgnoreCase(name)) {
				return status;
			}
		}

		return null;
	}
	public static String supportValues() {
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < values().length; i++) {
			String delimeter = (i == 0)?"":",";
			builder.append(delimeter).append(values()[i].name());
		}
		return builder.toString();
	}
}
