package com.bys.crm.domain.erp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.bys.crm.domain.DomainEntity;

@Entity
@Table(name = "GEPaymentTerms")
public class GEPaymentTerms extends DomainEntity<Long> {
	@Id
	@Column(name = "GEPaymentTermID")
	private Long id;
	
	@Column(name = "GEPaymentTermName")
	private String name;

	@Column(name = "AAStatus")
	private String status;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
