package com.bys.crm.domain.erp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.joda.time.DateTime;



@Entity
@Table(name = "ARBidConstructionTypes")
public class ARBidConstructionTypes {

	@Id
	@Column(name = "ARBidConstructionTypeID", nullable = false)
	private Long id;
	
	@Column(name = "AAStatus", columnDefinition = "varchar(10)")
	private String status;

	@Column(name = "AACreatedUser", columnDefinition = "nvarchar(50)")
	private String createdUser;

	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	@Column(name = "AACreatedDate")
	private DateTime createdDate;

	@Column(name = "AAUpdatedUser", columnDefinition = "nvarchar(50)")
	private String updatedUser;

	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	@Column(name = "AAUpdatedDate")
	private DateTime updatedDate;
	
	@Column(name = "ARBidConstructionTypeName", nullable = false, columnDefinition = "nvarchar(100)")
	private String name;
	
	@Column(name = "ARBidConstructionTypeNo", nullable = false, columnDefinition = "nvarchar(50)")
	private String typeNo;
	
	@Column(name = "ARBidConstructionTypeDesc", nullable = false, columnDefinition = "nvarchar(1020)")
	private String typeDesc;
	
	@Column(name = "ARBidConstructionTypeNameEng", nullable = false, columnDefinition = "nvarchar(100)")
	private String typeNameEng;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(String createdUser) {
		this.createdUser = createdUser;
	}

	public DateTime getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(DateTime createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdatedUser() {
		return updatedUser;
	}

	public void setUpdatedUser(String updatedUser) {
		this.updatedUser = updatedUser;
	}

	public DateTime getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(DateTime updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTypeNo() {
		return typeNo;
	}

	public void setTypeNo(String typeNo) {
		this.typeNo = typeNo;
	}

	public String getTypeDesc() {
		return typeDesc;
	}

	public void setTypeDesc(String typeDesc) {
		this.typeDesc = typeDesc;
	}

	public String getTypeNameEng() {
		return typeNameEng;
	}

	public void setTypeNameEng(String typeNameEng) {
		this.typeNameEng = typeNameEng;
	}
	
	
	
	
}
