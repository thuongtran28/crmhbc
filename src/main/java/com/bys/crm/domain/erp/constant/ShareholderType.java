package com.bys.crm.domain.erp.constant;

public enum ShareholderType {
	SignificantTypeA("A"),
	SignificantTypeB("B"),
	SignificantTypeC("C");
	
	private String dBCode;

	private ShareholderType(String dBCode) {
		this.dBCode = dBCode;
	}

	public String value() {
		return this.dBCode;
	}

	public static ShareholderType fromValue(String value) {
		for (ShareholderType status : ShareholderType.values()) {
			if (status.value().equalsIgnoreCase(value)) {
				return status;
			}
		}

		return null;
	}

	public static ShareholderType fromName(String name) {
		for (ShareholderType status : ShareholderType.values()) {
			if (status.name().equalsIgnoreCase(name)) {
				return status;
			}
		}

		return null;
	}

	public static String supportValues() {
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < values().length; i++) {
			String delimeter = (i == 0)?"":",";
			builder.append(delimeter).append(values()[i].name());
		}
		return builder.toString();
	}
}
