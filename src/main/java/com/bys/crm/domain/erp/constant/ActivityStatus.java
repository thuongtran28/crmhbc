package com.bys.crm.domain.erp.constant;

public enum ActivityStatus {
	NoStarted("Chưa bắt đầu"),
	IsPlanning_Activity("Lên kế hoạch"),
	Finish_Activity("Hoàn thành"),
	WaitingEntry("Trì hoãn nhập liệu"),
	Proceed_Activity("Đang xử lý"),
	Postponed("Đã hoãn lại");
	
	private String dBCode;

	private ActivityStatus(String dBCode) {
		this.dBCode = dBCode;
	}

	public String value() {
		return this.dBCode;
	}

	public static ActivityStatus fromValue(String value) {
		for (ActivityStatus status : ActivityStatus.values()) {
			if (status.value().equalsIgnoreCase(value)) {
				return status;
			}
		}

		return null;
	}

	public static ActivityStatus fromName(String name) {
		for (ActivityStatus status : ActivityStatus.values()) {
			if (status.name().equalsIgnoreCase(name)) {
				return status;
			}
		}

		return null;
	}

	public static String supportValues() {
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < values().length; i++) {
			String delimeter = (i == 0)?"":",";
			builder.append(delimeter).append(values()[i].name());
		}
		return builder.toString();
	}
}
