package com.bys.crm.domain.erp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.Type;
import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;
import org.joda.time.DateTime;

import com.bys.crm.domain.DomainEntity;
import com.bys.crm.domain.erp.constant.AAStatus;

@Entity
@Audited
public class ADUserPermissions extends DomainEntity<Long>{

	@Id
	@Column(name = "ADUserPermissionID")
	private Long id;
	
	@Column(name = "AAStatus", columnDefinition = "varchar(10)")
	private String status;

	@Column(name = "AACreatedUser", columnDefinition = "nvarchar(50)")
	private String createdUser;

	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	@Column(name = "AACreatedDate")
	private DateTime createdDate;

	@Column(name = "AAUpdatedUser", columnDefinition = "nvarchar(50)")
	private String updatedUser;

	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	@Column(name = "AAUpdatedDate")
	private DateTime updatedDate;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name ="FK_ADUserID", columnDefinition = "int")
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	private ADUsers user;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name ="FK_ADPermissionID", columnDefinition = "int")
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	private ADPermissions permissions;
	
	public ADUserPermissions(){
		this.status = AAStatus.Alive.name();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(String createdUser) {
		this.createdUser = createdUser;
	}

	public DateTime getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(DateTime createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdatedUser() {
		return updatedUser;
	}

	public void setUpdatedUser(String updatedUser) {
		this.updatedUser = updatedUser;
	}

	public DateTime getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(DateTime updatedDate) {
		this.updatedDate = updatedDate;
	}

	public ADUsers getUser() {
		return user;
	}

	public void setUser(ADUsers user) {
		this.user = user;
	}

	public ADPermissions getPermissions() {
		return permissions;
	}

	public void setPermissions(ADPermissions permissions) {
		this.permissions = permissions;
	}	
}
