package com.bys.crm.domain.erp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.bys.crm.domain.erp.model.GEPaymentTerms;

@Repository
public interface GEPaymentTermsRepository extends JpaRepository<GEPaymentTerms, Long>, BaseRepository {

	@Query("SELECT max(entity.id) FROM ARCustomers entity")
	Long findMaxId();

	List<GEPaymentTerms> findByStatus(String status);
}
