package com.bys.crm.domain.erp.model;

import java.math.BigDecimal;
 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;
import org.joda.time.DateTime;

import com.bys.crm.domain.DomainEntity;
import com.bys.crm.domain.erp.constant.AAStatus;

@Entity
@Audited
@Table(name = "ARBidOpportunitys")
public class ARBidOpportunitys  extends DomainEntity<Long> {
	
	@Id
	@Column(name = "ARBidOpportunityID", nullable = false)
	private Long id;

	@Column(name = "AAStatus", columnDefinition = "varchar(10)")
	private String status;

	@Column(name = "AACreatedUser", columnDefinition = "nvarchar(50)")
	private String createdUser;

	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	@Column(name = "AACreatedDate")
	private DateTime createdDate;

	@Column(name = "AAUpdatedUser", columnDefinition = "nvarchar(50)")
	private String updatedUser;

	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	@Column(name = "AAUpdatedDate")
	private DateTime updatedDate;
	
	@Column(name = "ARBidOpportunityProjectName", nullable = false, columnDefinition = "nvarchar(500)")
	private String projectName;
	
	@Column(name = "ARBidOpportunityName", nullable = false, columnDefinition = "nvarchar(500)")
	private String name;
	
	@Column(name = "ARBidOpportunityNo", nullable = false, columnDefinition = "nvarchar(50)")
	private String opportunityNo;
	
	
	@Column(name = "ARBidOpportunityJob",   columnDefinition = "nvarchar(100)")
	private String job;
	
	@Column(name = "ARBidOpportunityPlace",  columnDefinition = "nvarchar(500)")
	private String place;
	
	@Column(name = "ARBidOpportunityQuarter",  columnDefinition = "nvarchar(5)")
	private String quarter;
	
	@Column(name = "ARBidOpportunityCustomerType",   columnDefinition = "nvarchar(50)")
	private String customerType;
	
	@Column(name = "ARBidOpportunityConsultantUnit",   columnDefinition = "nvarchar(500)")
	private String consultantUnit;
	
	
	@Column(name = "ARBidOpportunityConsultantAddress",  columnDefinition = "nvarchar(500)")
	private String consultantAddress;
	
	@Column(name = "ARBidOpportunityConsultantPhone",  columnDefinition = "nvarchar(50)")
	private String consultantPhone;
	
	@Column(name = "ARBidOpportunityFloorArea")
	private BigDecimal floorArea;
	
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	@Column(name = "ARBidOpportunityStartTrackingDate")
	private DateTime startTrackingDate;
	
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	@Column(name = "ARBidOpportunityBidSubmissionDate")
	private DateTime bidSubmissionDate;
	
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	@Column(name = "ARBidOpportunityEstimatedResultDate")
	private DateTime estimatedResultDate;
	
	@Column(name = "ARBidOpportunityMagnitude",  columnDefinition = "nvarchar(150)")
	private String magnitude;
	
	@Column(name = "ARBidOpportunityHBCRole",  columnDefinition = "nvarchar(150)")
	private String opportunityHBCRole;
	
	@Column(name = "ARBidOpportunityDocumentLink",  columnDefinition = "nvarchar(MAX)")
	private String documentLink;
	
	@Column(name = "ARBidOpportunityProgress")
	private Integer progress;
	
	@Column(name = "ARBidOpportunityAcceptanceReason",   columnDefinition = "nvarchar(500)")
	private String acceptanceReason;
	
	@Column(name = "ARBidOpportunityUnacceptanceReason",  columnDefinition = "nvarchar(500)")
	private String unacceptanceReason;
	
	@Column(name = "ARBidOpportunityCancelReason",   columnDefinition = "nvarchar(500)")
	private String cancelReason;
	
	@Column(name = "ARBidOpportunityEvaluation",   columnDefinition = "nvarchar(500)")
	private String evaluation;
	
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	@Column(name = "ARBidOpportunityEstimatedProjectStartDate")
	private DateTime estimatedProjectStartDate;
	 
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	@Column(name = "ARBidOpportunityEstimatedProjectEndDate")
	private DateTime estimatedProjectEndDate;
	
	@Column(name = "ARBidOpportunityTotalTime",   columnDefinition = "nvarchar(50)")
	private String totalTime;
	
	@Column(name = "ARBidOpportunityStageStatus",   columnDefinition = "nvarchar(50)")
	private String opportunityStageStatus;
	
	@Column(name = "ARBidOpportunityStage",   columnDefinition = "nvarchar(50)")
	private String opportunityStage;
	
	
	
	@Column(name = "ARBidOpportunityHSMTStatus",   columnDefinition = "nvarchar(50)")
	private String opportunityHSMTStatus;
	
	
	public String getOpportunityHSMTStatus() {
		return opportunityHSMTStatus;
	}




	public void setOpportunityHSMTStatus(String opportunityHSMTStatus) {
		this.opportunityHSMTStatus = opportunityHSMTStatus;
	}




	public String getOpportunityStageStatus() {
		return opportunityStageStatus;
	}




	public void setOpportunityStageStatus(String opportunityStageStatus) {
		this.opportunityStageStatus = opportunityStageStatus;
	}




	public String getOpportunityStage() {
		return opportunityStage;
	}




	public void setOpportunityStage(String opportunityStage) {
		this.opportunityStage = opportunityStage;
	}


	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "FK_ARCustomerID")
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	private ARCustomers customer;
	
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "FK_ARCustomerContactID")
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	private ARCustomerContacts customerContact;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "FK_ARBidConstructionTypeID")
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	private ARBidConstructionTypes constructionType;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "FK_ARBidConstructionCategoryID")
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	private ARBidConstructionCategories constructionCategorie;
	
	
	 
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "FK_ARBidLocationID")
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	private ARBidLocations bidlocation;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "FK_ChairEmployeeID")
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	private HREmployees chairEmployee;
	
	@OneToOne(fetch = FetchType.LAZY, optional = false )
	@JoinColumn(name = "FK_AROpportunityID", nullable = false)
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	private AROpportunitys opportunityInfo;
	//FK_ARBidConstructionCategoryID
	//
	
	public ARBidOpportunitys() {
		this.status = AAStatus.Alive.name();
	}
 
	
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "FK_HREmployeeID")
	private HREmployees employee;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "FK_ARConsultantUnitCustomerID")
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	private ARCustomers consultantUnitCustomer;


	public ARCustomers getConsultantUnitCustomer() {
		return consultantUnitCustomer;
	}




	public void setConsultantUnitCustomer(ARCustomers consultantUnitCustome) {
		this.consultantUnitCustomer = consultantUnitCustome;
	}




	public Long getId() {
		return id;
	}




	public void setId(Long id) {
		this.id = id;
	}




	public String getStatus() {
		return status;
	}




	public void setStatus(String status) {
		this.status = status;
	}




	public String getCreatedUser() {
		return createdUser;
	}




	public void setCreatedUser(String createdUser) {
		this.createdUser = createdUser;
	}




	public DateTime getCreatedDate() {
		return createdDate;
	}




	public void setCreatedDate(DateTime createdDate) {
		this.createdDate = createdDate;
	}




	public String getUpdatedUser() {
		return updatedUser;
	}




	public void setUpdatedUser(String updatedUser) {
		this.updatedUser = updatedUser;
	}




	public DateTime getUpdatedDate() {
		return updatedDate;
	}




	public void setUpdatedDate(DateTime updatedDate) {
		this.updatedDate = updatedDate;
	}




	public String getProjectName() {
		return projectName;
	}




	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}




	public String getName() {
		return name;
	}




	public void setName(String name) {
		this.name = name;
	}




	public String getOpportunityNo() {
		return opportunityNo;
	}




	public void setOpportunityNo(String opportunityNo) {
		this.opportunityNo = opportunityNo;
	}




	public String getJob() {
		return job;
	}




	public void setJob(String job) {
		this.job = job;
	}




	public String getPlace() {
		return place;
	}




	public void setPlace(String place) {
		this.place = place;
	}




	public String getQuarter() {
		return quarter;
	}




	public void setQuarter(String quarter) {
		this.quarter = quarter;
	}




	public String getCustomerType() {
		return customerType;
	}




	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}




	public String getConsultantUnit() {
		return consultantUnit;
	}




	public void setConsultantUnit(String consultantUnit) {
		this.consultantUnit = consultantUnit;
	}




	public String getConsultantAddress() {
		return consultantAddress;
	}




	public void setConsultantAddress(String consultantAddress) {
		this.consultantAddress = consultantAddress;
	}




	public String getConsultantPhone() {
		return consultantPhone;
	}




	public void setConsultantPhone(String consultantPhone) {
		this.consultantPhone = consultantPhone;
	}




	public BigDecimal getFloorArea() {
		return floorArea;
	}




	public void setFloorArea(BigDecimal floorArea) {
		this.floorArea = floorArea;
	}




	public DateTime getStartTrackingDate() {
		return startTrackingDate;
	}




	public void setStartTrackingDate(DateTime startTrackingDate) {
		this.startTrackingDate = startTrackingDate;
	}




	public DateTime getBidSubmissionDate() {
		return bidSubmissionDate;
	}




	public void setBidSubmissionDate(DateTime bidSubmissionDate) {
		this.bidSubmissionDate = bidSubmissionDate;
	}




	public DateTime getEstimatedResultDate() {
		return estimatedResultDate;
	}




	public void setEstimatedResultDate(DateTime estimatedResultDate) {
		this.estimatedResultDate = estimatedResultDate;
	}




	public String getMagnitude() {
		return magnitude;
	}




	public void setMagnitude(String magnitude) {
		this.magnitude = magnitude;
	}




	public String getOpportunityHBCRole() {
		return opportunityHBCRole;
	}




	public void setOpportunityHBCRole(String opportunityHBCRole) {
		this.opportunityHBCRole = opportunityHBCRole;
	}




	public String getDocumentLink() {
		return documentLink;
	}




	public void setDocumentLink(String documentLink) {
		this.documentLink = documentLink;
	}




	public Integer getProgress() {
		return progress;
	}




	public void setProgress(Integer progress) {
		this.progress = progress;
	}




	public String getAcceptanceReason() {
		return acceptanceReason;
	}




	public void setAcceptanceReason(String acceptanceReason) {
		this.acceptanceReason = acceptanceReason;
	}




	public String getUnacceptanceReason() {
		return unacceptanceReason;
	}




	public void setUnacceptanceReason(String unacceptanceReason) {
		this.unacceptanceReason = unacceptanceReason;
	}




	public String getCancelReason() {
		return cancelReason;
	}




	public void setCancelReason(String cancelReason) {
		this.cancelReason = cancelReason;
	}




	public String getEvaluation() {
		return evaluation;
	}




	public void setEvaluation(String evaluation) {
		this.evaluation = evaluation;
	}




	public DateTime getEstimatedProjectStartDate() {
		return estimatedProjectStartDate;
	}




	public void setEstimatedProjectStartDate(DateTime estimatedProjectStartDate) {
		this.estimatedProjectStartDate = estimatedProjectStartDate;
	}




	public DateTime getEstimatedProjectEndDate() {
		return estimatedProjectEndDate;
	}




	public void setEstimatedProjectEndDate(DateTime estimatedProjectEndDate) {
		this.estimatedProjectEndDate = estimatedProjectEndDate;
	}




	public String getTotalTime() {
		return totalTime;
	}




	public void setTotalTime(String totalTime) {
		this.totalTime = totalTime;
	}




	public ARCustomers getCustomer() {
		return customer;
	}




	public void setCustomer(ARCustomers customer) {
		this.customer = customer;
	}




	public ARCustomerContacts getCustomerContact() {
		return customerContact;
	}




	public void setCustomerContact(ARCustomerContacts customerContact) {
		this.customerContact = customerContact;
	}




	public ARBidConstructionTypes getConstructionType() {
		return constructionType;
	}




	public void setConstructionType(ARBidConstructionTypes constructionType) {
		this.constructionType = constructionType;
	}




	public ARBidConstructionCategories getConstructionCategorie() {
		return constructionCategorie;
	}




	public void setConstructionCategorie(ARBidConstructionCategories constructionCategorie) {
		this.constructionCategorie = constructionCategorie;
	}




	public ARBidLocations getBidlocation() {
		return bidlocation;
	}




	public void setBidlocation(ARBidLocations bidlocation) {
		this.bidlocation = bidlocation;
	}




	public HREmployees getChairEmployee() {
		return chairEmployee;
	}




	public void setChairEmployee(HREmployees chairEmployee) {
		this.chairEmployee = chairEmployee;
	}




	public AROpportunitys getOpportunityInfo() {
		return opportunityInfo;
	}




	public void setOpportunityInfo(AROpportunitys opportunityInfo) {
		this.opportunityInfo = opportunityInfo;
	}




	public HREmployees getEmployee() {
		return employee;
	}




	public void setEmployee(HREmployees employee) {
		this.employee = employee;
	}
	
	
	
	
	
}
