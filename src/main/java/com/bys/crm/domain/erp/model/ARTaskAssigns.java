package com.bys.crm.domain.erp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;
import org.joda.time.DateTime;

import com.bys.crm.domain.DomainEntity;

@Entity
@Audited
@Table(name = "ARTaskAssigns")
public class ARTaskAssigns extends DomainEntity<Long> {
	@Id
	@Column(name = "ARTaskAssignID", nullable = false, columnDefinition = "int")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "FK_BRBranchID", columnDefinition = "int")
	@NotAudited
	private BRBranchs branch;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "FK_AROpportunityID", columnDefinition = "int")
	private AROpportunitys opportunity;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "FK_ARCampaignID", columnDefinition = "int")
	private ARCampaigns campaign;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "FK_ARActivityID", columnDefinition = "int")
	private ARActivitys activity;

	@Column(name = "ARTaskAssignType", columnDefinition = "varchar(50)")
	private String taskAssignType;

	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "FK_HREmployeeID", columnDefinition = "int")
	private HREmployees employee;

	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "FK_HRGroupID", columnDefinition = "int")
	private HRGroups employeeGroup;

	@Column(name = "AACreatedUser", columnDefinition = "nvarchar(50)")
	private String createdUser;

	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	@Column(name = "AACreatedDate", columnDefinition = "datetime")
	private DateTime createdDate;

	@Column(name = "AAUpdatedUser", columnDefinition = "nvarchar(50)")
	private String updatedUser;

	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	@Column(name = "AAUpdatedDate", columnDefinition = "datetime")
	private DateTime updatedDate;

	@Column(name = "AAStatus", columnDefinition = "varchar(10)")
	private String status;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public BRBranchs getBranch() {
		return branch;
	}

	public void setBranch(BRBranchs branch) {
		this.branch = branch;
	}

	public AROpportunitys getOpportunity() {
		return opportunity;
	}

	public void setOpportunity(AROpportunitys opportunity) {
		this.opportunity = opportunity;
	}

	public ARCampaigns getCampaign() {
		return campaign;
	}

	public void setCampaign(ARCampaigns campaign) {
		this.campaign = campaign;
	}

	public ARActivitys getActivity() {
		return activity;
	}

	public void setActivity(ARActivitys activity) {
		this.activity = activity;
	}

	public String getTaskAssignType() {
		return taskAssignType;
	}

	public void setTaskAssignType(String taskAssignType) {
		this.taskAssignType = taskAssignType;
	}

	public HREmployees getEmployee() {
		return employee;
	}

	public void setEmployee(HREmployees employee) {
		this.employee = employee;
	}

	public HRGroups getEmployeeGroup() {
		return employeeGroup;
	}

	public void setEmployeeGroup(HRGroups employeeGroup) {
		this.employeeGroup = employeeGroup;
	}

	public String getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(String createdUser) {
		this.createdUser = createdUser;
	}

	public DateTime getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(DateTime createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdatedUser() {
		return updatedUser;
	}

	public void setUpdatedUser(String updatedUser) {
		this.updatedUser = updatedUser;
	}

	public DateTime getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(DateTime updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
