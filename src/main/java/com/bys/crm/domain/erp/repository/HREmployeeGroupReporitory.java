package com.bys.crm.domain.erp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.bys.crm.domain.erp.model.HREmployeeGroups;

@Repository
public interface HREmployeeGroupReporitory  extends JpaRepository< HREmployeeGroups, Long>, BaseRepository{
	
	@Query("SELECT max(entity.id) FROM HREmployeeGroups entity")
	Long findMaxId();
	
	List<HREmployeeGroups> findByStatus(String status);
	
	List<HREmployeeGroups> findByStatusAndEmployeeId(String status, Integer employeeId);
	
	List<HREmployeeGroups> findByStatusAndEmployeeIdIn(String status, List<Integer> employeeIds);
}
