package com.bys.crm.domain.erp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

 
import com.bys.crm.domain.erp.model.HRDepartments;

@Repository
public interface HRDepartmentsRepository extends JpaRepository<HRDepartments, Long>, BaseRepository{
	@Query("SELECT max(entity.id) FROM HRDepartments entity")
	Long findMaxId();
	
	List<HRDepartments> findByStatusOrderByNameAsc(String status);
}
