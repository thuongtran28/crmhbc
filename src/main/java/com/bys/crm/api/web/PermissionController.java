package com.bys.crm.api.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bys.crm.app.dto.ResponseDto;
import com.bys.crm.app.facade.PermissionFacade;

@RestController
public class PermissionController extends BaseController{

	@Autowired
	private PermissionFacade facade;
	
	@RequestMapping(method = RequestMethod.GET, value = RestURL.GET_PERMISSIONS)
	public ResponseDto getPermissions(@PathVariable(value = "employeeId") int employeeId) {
		return new ResponseDto(facade.getPermissions());
	}
}
