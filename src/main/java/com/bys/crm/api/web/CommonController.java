package com.bys.crm.api.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bys.crm.app.dto.ResponseDto;
import com.bys.crm.app.facade.CommonFacade;
import com.bys.crm.app.facade.ProvinceFacade;

@RestController
public class CommonController extends BaseController {
	@Autowired
	private CommonFacade facade;

	@Autowired
	private ProvinceFacade provinceFacade;

	@RequestMapping(method = RequestMethod.GET, value = RestURL.SEARCH_PHONE_NUMBER)
	public ResponseDto searchPhoneNumber(@PathVariable(value = "employeeId") int employeeId,
			@PathVariable(value = "phoneNumber") String phoneNumber) {
		return new ResponseDto(facade.searchPhoneNumber(employeeId, phoneNumber));
	}

	@RequestMapping(method = RequestMethod.GET, value = RestURL.CHECT_EXIST_PHONE_NUMBER)
	public ResponseDto checkExistPhoneNumber(@PathVariable(value = "employeeId") int employeeId,
			@PathVariable(value = "branchId") int branchId,
			@PathVariable(value = "phoneNumber") String phoneNumber) {
		return new ResponseDto(facade.isExistPhoneNumber(employeeId, branchId, phoneNumber));
	}

	@RequestMapping(method = RequestMethod.GET, value = RestURL.GET_PAYMENT_TERMS)
	public ResponseDto getPaymentTerms(@PathVariable(value = "employeeId") int employeeId) {
		return new ResponseDto(facade.getPaymentTerms(employeeId));
	}
	
	@RequestMapping(method = RequestMethod.GET, value = RestURL.GET_PROVINCE_LIST)
	public ResponseDto getProvinceList(@PathVariable(value = "employeeId") int employeeId) {
		return new ResponseDto(provinceFacade.getProvinceList(employeeId));
	}
	
	@RequestMapping(method = RequestMethod.GET, value = RestURL.GET_BIDLOCATION_LIST)
	public ResponseDto getBidLocations(@PathVariable(value = "employeeId") int employeeId) {
		return new ResponseDto(facade.getBidLocations(employeeId));
	}
	
	@RequestMapping(method = RequestMethod.GET, value = RestURL.GET_BIDCONSTRUCTIONTYPE_LIST)
	public ResponseDto getBidConstructionTypes(@PathVariable(value = "employeeId") int employeeId) {
		return new ResponseDto(facade.getBidConstructionTypes(employeeId));
	}
	
	@RequestMapping(method = RequestMethod.GET, value = RestURL.GET_BIDCONSTRUCTIONCATEGORIE_LIST)
	public ResponseDto getBidConstructionCategories(@PathVariable(value = "employeeId") int employeeId) {
		return new ResponseDto(facade.getBidConstructionCategories(employeeId));
	}
	
	
}
