package com.bys.crm.api.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bys.crm.app.dto.ResponseDto;
import com.bys.crm.app.facade.DepartmentsFacade;

@RestController
public class DepartmentsController extends BaseController{ 
	
	@Autowired
	private DepartmentsFacade facade;

	@RequestMapping(method = RequestMethod.GET, value = RestURL.GET_DEPARTMENTS_LIST)
	public ResponseDto getEmployeesList(@PathVariable(value = "employeeId") int employeeId) {
		return new ResponseDto(facade.getDepartmentsList(employeeId));
	}
}
