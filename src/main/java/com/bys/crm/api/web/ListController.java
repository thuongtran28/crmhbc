package com.bys.crm.api.web;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

 
import com.bys.crm.app.dto.ResponseDto;
import com.bys.crm.app.dto.UserFieldConfigsDto;
import com.bys.crm.app.facade.ListFacade;
import com.bys.crm.app.facade.UserFieldConfigsFacade;

@RestController
public class ListController extends BaseController {
	@Autowired
	private ListFacade facade;
	
	
	@Autowired
	private UserFieldConfigsFacade userFieldFacade;

	@RequestMapping(method = RequestMethod.GET, value = RestURL.GET_COMMON_LIST)
	public ResponseDto getCommonList(@PathVariable(value = "listType") String listType) {
		return new ResponseDto(facade.getCommonList(listType));
	}
	
	@RequestMapping(method = RequestMethod.GET, value = RestURL.GET_USER_FIELD_CONFIG_OF_FUNCTION)
	public ResponseDto getUserFieldById(@PathVariable(value = "employeeId") int employeeId,
			@PathVariable(value = "funcType") String funcType) {
		return new ResponseDto(userFieldFacade.getUserFieldByIdOfFuncType(employeeId, funcType));
	}

	@RequestMapping(method = RequestMethod.POST, value = RestURL.APPLY_USER_FIELD_CONFIG_OF_FUNCTION)
	public ResponseDto applySettingFieldOfFunction(@PathVariable(value = "employeeId") int employeeId,
			@RequestBody @Valid UserFieldConfigsDto dto) {
		return new ResponseDto(userFieldFacade.ApplySettingFieldOfFunction(dto, employeeId));
	}
	
}
